#ifndef __MEMM_CONFIGURATION_H__
#define __MEMM_CONFIGURATION_H__

#include <iostream>
#include <fstream>
#include <cstring>
#include <unordered_map>
#include <map>
#include <vector>
#include <string>

#include "Parameter.hpp"
#include "MEMM_loi.h"

#define DEFAULT_CONFIGURATION_FILE "Parameters.xml"

using namespace std;

enum MEMM_parameters_t {
  MEMM_TYPE,
  MEMM_PARENTS_FILE_NAME,
  MEMM_PARENTS_FILE_NUMBER_OF_LOCUS,
  MEMM_PARENTS_FILE_CLASS_COV,
  MEMM_PARENTS_FILE_QT_COV,
  MEMM_PARENTS_FILE_WEIGHT_VAR,
  MEMM_OFFSPRING_FILE_NAME,
  MEMM_OFFSPRING_FILE_NUMBERS_OF_LOCUS,
  MEMM_AF_FILE_MODE,
  MEMM_AF_FILE_NAME,
  MEMM_DIST_FILE_MODE,
  MEMM_DIST_FILE_NAME,
  MEMM_LOCUS_ERROR,
  MEMM_LOCUS_ERROR_FILE,
  MEMM_SEED,
  MEMM_BURNIN,
  MEMM_ITE,
  MEMM_THIN,
  MEMM_GAMA_FILE_NAME,
  MEMM_IND_FEC_FILE_NAME,
  MEMM_DISP_FILE_NAME
};

/**
 * \class Configuration
 * \brief Abstract Configuration class
 *
 * Abstract Configuration class for MEMM INPUT and OUTPUT
 */
class Configuration {

  public : 
    Configuration();
    Configuration(const char * filein);
    virtual ~Configuration();
    bool isOK();
    virtual char * getValue(MEMM_parameters_t valueName)=0;
    virtual bool setValue(MEMM_parameters_t valueName, char * value)=0;
    virtual unordered_map<string, Parameter *> loadParameters()=0;
    //virtual bool setValue(MEMM_parameters_t valueName, unsigned int * value)=0;
    virtual string getDistributionType(string name)=0;
    virtual map<string,string> getDistributionParameters(string lawname)=0;

    vector<string> getPriorsName();
    map<string,vector<string>> getDistributionLawsNameAndParametersName();

  protected :
    bool isOK_;
    char * configurationFileName_;

};

#endif
