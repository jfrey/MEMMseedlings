/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef MEMM_UTIL_H
#define MEMM_UTIL_H
#include <vector>

typedef struct PerePot{
  int pere_pot;
  double pere_prob;
} PerePot;

double gammln(double xx);

/*long double loglik1 (int nm, int ns, int npar,
                     const std::vector<double> & poids,
                     const std::vector<std::vector<double> > & distmp,
                     const std::vector<int> & meres, const std::vector<int> & merdesc, const std::vector <double > & probmig,
                     const std::vector <double > & probself, const std::vector <int > & nbperes, const std::vector<std::vector<PerePot> > & probperes,
                     const std::vector<double> & fec, double delta, double b, double mig, double self, long double & a, long double & ab);
*/

#endif
