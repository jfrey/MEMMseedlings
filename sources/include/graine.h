/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef GRAINE_H
#define GRAINE_H

/*!
 * \file graine.hpp
 * \brief Graine (seed) class
 * \author Etienne Klein
 * \author Jean-Francois REY
 * \version 1.5
 * \date 21 April 2015
 */

#include "individu.h"
#include <string>
#include <iostream>
#include <vector>


/*! \iclass graine
 * \brief graine class
 * 
 * This class define a seed. It inherites from individu with the mother name.
 */
class graine : public individu
{
    public:

        /*! \brief default constructor
         */
        graine() {};

        /*! \brief Constructor
         * \param name : individu name
         * \param file_in : in stream file containing seed data
         * \param nloc : number of locus to load
         */
        graine(const std::string & name, std::ifstream & file_in, int nloc);

        /*! \brief Destructor
         */
        virtual ~graine();

        /*! \brief print seed informations
         */
        virtual void afficher() const;

        /*! \brief get seed mother
         * \return mother name as string
         */
        std::string getMere() const {return _merdesc;}

//        double mendel (const individu & mere , const individu & pere, int nl, std::vector<int> na,  std::vector < std::map<int,int> > sizeall, const std::vector < std::vector<double> > & freqall) const;
//        double mendel(const individu & mere , const individu & pere, int nl, std::vector<int> na,  std::vector < std::map<int,int> > sizeall, const std::vector < std::vector<double> > & freqall,const std::vector < std::vector < std::vector <double> > > & materror ) const;

    protected:
    private:
        std::string _merdesc; //!< mother name

};

#endif // GRAINE_H
