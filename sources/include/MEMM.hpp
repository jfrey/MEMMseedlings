/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __MEMM_HPP__
#define __MEMM_HPP__

/*!
 * \file MEMM.hpp
 * \brief MEMM abstract class
 * \author Jean-Francois REY
 * \version 1.1
 * \date 11 May 2016
 *
 * The MEMM abstract class define common variables, tools and models.
 */


#include <iostream>
#include <unordered_map>

#include <boost/random.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/lagged_fibonacci.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>

#include "Configuration.hpp"
#include "ConfigTXT.hpp"
#include "ConfigXML.hpp"
#include "Parameter.hpp"
#include "parent.h"
#include "graine.h"
#include "MEMM_util.h"
#include "MEMM_loi.h"
#include "MEMM_logNormal.h"
#include "MEMM_gamma.h"
#include "MEMM_zigamma.h"
#include "MEMM_beta.h"
#include <time.h>

using namespace std;

/*! \class MEMM
 * \brief MEMM main Abstract class
 *
 * This class define the main Mixed Effect Mating Model variables and tools.
 */
class MEMM {

  public :
    
    /*! \brief Constrcutor
     *
     * MEMM class constructor
     */
    MEMM();

    /*! \brief Destructor
     *
     * MEMM class destructor
     */
    virtual ~MEMM();

    /*! \brief initialize variables from parameters
     * \param p : a Configuration pointer
     *
     * Load parameters and initialize variables and model.
     */
    virtual void init(Configuration * p);

    /*! \brief run mcmc
     *  Run mcmc algorithm with nbIteration and thin 
     */
    void run();

    /*! \brief run burn in
     * Run mcmc with burn in iteration
     */
    void burnin();

    /*! \brief mcmc algorithm
     * \param nbIteration : number of iteration
     * \param thinStep : number of step for output (0 print nothings)
     *
     * Run mcmc algorithm. 
     */
    virtual void mcmc(int nbIteration, int thinStep) = 0;
 
    /*! \brief dynamic mcmc algorithm (for developpment)
     * \param nbIteration : number of iteration
     * \param thinStep : number of step for output (0 print nothings)
     *
     * Run mcmc algorithm using dynamic structurs and variables only for developpement. 
     */
    virtual void mcmc_dyn(int nbIteration, int thinStep) = 0;

  protected : 
    bool dynamic_;  //!< True use dynamic mode 

    MEMM_loi * pLoi;  //!< distribution for individual fecundities
    int NPar; //!< number of parents
    int Ns;   //!< number of seeds
    vector<vector<double>> DistMP;  //!< Distances between individu
    vector<double> Poids;   //!< pollen donor weight 


    //Parameters
    unordered_map<string, Parameter *> allParameters;

    // allParameters contains all parameters use below to access parameters directly from variable name.
    double Scale; //!< scale parameter initial, dimension of distance (a)
    double mScale; //!< min value of scale parameter, dimension of distance (a)
    double MScale; //!< max value of scale parameter, dimension of distance (a)
    double Shape; //!< shape parameter initial for dispersal kernel (b)
    double mShape; //!< min value of shape parameter for dispersal kernel (b)
    double MShape; //!< max value of shape parameter for dispersal kernel (b)
    double Mig;   //!< initial value of probabilities to mother to be fertilized by pollen grain from uncensored father form outside the area
    double mMig;  //!< min value of probabilities to mother to be fertilized by pollen grain from uncensored father form outside the area
    double MMig;  //!< max value of probabilities to mother to be fertilized by pollen grain from uncensored father form outside the area
    double Self;  //!< initial probabilites to mother to be self fertilized
    double mSelf; //!< min value probabilites to mother to be self fertilized
    double MSelf; //!< max value probabilites to mother to be self fertilized
    double GamA;  //!< initial LogNormal fecundities exp(variance) = dobs/de
    double mGamA; //!< min value LogNormal fecundities exp(variance) = dobs/de
    double MGamA; //!< max value LogNormal fecundities exp(variance) = dobs/de

    double Pi0, mPi0, MPi0; //!< initial, min and max values for the zero-inflated parameter

    
    int Nburn;  //!< Burn in number
    int Nstep;  //!< number of iteration
    int Nthin;  //!< thinning step

    ofstream * ParamFec;  //!< output stream step | logLik of all fecundities | GamaA
    ofstream * IndivFec;  //!< output stream individual fecundities step | Vec_Fec[i]  
    ofstream * ParamDisp; //!< output stream dispersal parameters step | LogLik | scale | shape | Mig | Self | delta | ab

    boost::normal_distribution<> dist_norm;
    boost::uniform_real<> unif;
    boost::lagged_fibonacci19937 generator;
    boost::variate_generator<boost::lagged_fibonacci19937&, boost::normal_distribution<> > gauss;
    boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> > accept;

    // Variables use at initialisations only
    vector<parent*> allParents; //!< All parents from file
    map<string,int> fatherID; //!< father name - ID
    int Nl; //!< number of locus
    int Nql;  //!< number of class covaiates
    int Nqt;  //!< number of quantitative covariates
    int Nw; //!< variables weight
    vector<individu*> allSeeds; //!< All seeds from file
    vector< std::vector<double> > AllFreq;  //!< [locus][alleles] frequencies  
    vector< std::map<int,int> > SizeAll;  //!< [locus] <alleles-ID>
    bool useMatError;
    vector< vector < vector <double> > > MatError;  //!< [locus][alleles][alleles] <proba de lien lu/vrai>
    vector<int> Na; //!< <locus-ID> number of allele


    /*! \brief load parent file
     * \param p : a Configuration pointer
     * 
     * Will load parents information into AllParents.
     */
    virtual void loadParentFile(Configuration * p);
    
    /*! \brief load seed file
     * \param p : a Configuration pointer
     * 
     * Will load seeds information into AllSeeds.
     */
    virtual void loadSeeds(Configuration * p);

    /*! \brief Calcul distances between parents
     * \param p : a Configuration pointer
     * 
     * Calcul distances between parents (DispMP).
     */
    virtual void calculDistances(Configuration *p);

    /*! \brief load Allelic Frequencies
     * \param p : a Configuration pointer
     * 
     * Will load Allelic frequencies from file or calcul it.
     */
    virtual void loadAllelicFreq(Configuration * p);

    /*! \brief load locus error matrix
     * \param p : a parameterq pointer
     *
     * Will create a matrix of error for all locus (MatError)
     */
    virtual void loadMatrixError(Configuration * p, vector< vector<int> > & AllSize);

    /*! \brief Calcul Trnasition Matrix
     * \param p : a Configuration pointer
     * 
     * Calcul transition matrix.
     */
    virtual void calculTransitionMatrix(Configuration *p);

    /*! \brief load prior
     * \param p : a Configuration pointer
     * 
     * Will load and initialize prior variables.
     */
    virtual void loadParameters(Configuration *p);

    /*! \brief load paramerters
     * \param p : a Configuration pointer
     * 
     * Will load parameters
     */
    virtual void loadOptions(Configuration *p);

    /*! \brief Calcul Pollen donor weight
     * 
     * Calcul pollen donor weight.
     */
    virtual void calculWeightPollenDonor();


    /*! \brief load a law distribution
     * \param config : a Configuration pointer
     * \param name : distribution name to load
     * \return a pointer on MEMM_loi
     */
    MEMM_loi * loadDistribution(Configuration * config, string name);
};

#endif
