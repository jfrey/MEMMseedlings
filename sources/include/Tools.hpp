/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __TOOLS_GUI_MEMM__
#define __TOOLS_GUI_MEMM__

/*! \file Tools.hpp
 * \brief Tools
 * \author Jean-Francois Rey
 * \date 27 April 2016
 */

#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>

/*! \class Tools
 * \brief A set of tools
 */
class Tools {

    public :
        /*! \brief Convert a point to a comma
         * \param str : a string
         * \return a string with last '.' converted to ","
         */
        static std::string convertPointToComma(std::string str);

        /*! \brief Convert a comma to a point
         * \param str : a string
         * \return a string with last ',' converted to '.'.
         */
        static std::string convertCommaToPoint(std::string str);

        template <typename T>
        static std::string to_string_with_precision(const T a_value, const int n = 3);
};

template <typename T> 
std::string Tools::to_string_with_precision(const T a_value, const int n) 
{ 
    std::ostringstream out; 
    out << std::setprecision(n) << a_value; 
    return out.str(); 
}
#endif
