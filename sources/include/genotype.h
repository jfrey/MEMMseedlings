/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef GENOTYPE_H
#define GENOTYPE_H

/*!
 * \file genotype.hpp
 * \brief Genotype class
 * \author Etienne Klein
 * \author Jean-Francois Rey
 * \version 1.5
 * \date 21 April 2015
 */

#include "locus.h"
#include <fstream>
#include <vector>
#include <iostream>
#include <string>
using namespace std;

/*! \class genotype
 * \brief genotype class
 *
 * This class contains genotype data as locus elements.
 */
class genotype
{
    public:
        /*! \brief Constructor
         * \param nloc : number of locus
         * \param file_in : in stream file
         */
        genotype(int nloc, std::ifstream & file_in);

        /*! \brief Constructor
         * \param nloc : number of locus
         */
        genotype(int nloc);

        /*! \brief Default Constructor
         */
        genotype() {};

        /*! Destructor
         */
        ~genotype();

        /*! \brief get locus
         * \return a vector of locus pointer
         */
        std::vector<locus*> getLocus() const {return this->_genonuc;}

        /*! \brief get number of locus
         * \return the number of locus
         */
        long unsigned int getNbLocus() const {return this->_genonuc.size();}

        /*! \brief get a locus
         * \param k : locus index in genotype
         * \return a Locus pointer
         */
        locus* getLocus(int k) const {return this->_genonuc[k];}

        /*! \brief Calcul mendel genetic distance
         * \param mere : a genotype
         * \param pere : a genotype
         * \param nl : number of locus
         * \param na : vector of number of allele by locus
         * \param sizeall : for each locus contain <allele id,allele index>
         * \param freqall : for each locus contain alleles frequencies <locus,allele index>
         * \param materror : for each locus contain alleles genotype error. default value empty.
         *
         * Will calcul mendel genetic distance between this genotype and two others genotype (usually mother and father).
         */
        double mendel(const genotype & mere, const genotype & pere, int nl, const std::vector<int> & na, const std::vector<std::map<int,int> > & sizeall, const std::vector<std::vector<double> > & freqall, const std::vector < std::vector < std::vector <double> > > & materror = std::vector<std::vector<std::vector<double>>>()) const;

        /*! \brief print information
         */
        void afficher() const;
    protected:
    private:
        std::vector<locus*> _genonuc; //!< vector of locus
};

#endif // GENOTYPE_H
