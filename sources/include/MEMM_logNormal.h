/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef MEMM_LOGNORMAL_H
#define MEMM_LOGNORMAL_H
#include <fstream>
#include <vector>
#include <boost/random/variate_generator.hpp>
#include <boost/random/lagged_fibonacci.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/lognormal_distribution.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/math/distributions/gamma.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include "MEMM_loi.h"

/** 
 * \brief Cette classe implemente la loi log-normal.
 */
class MEMM_logNormal : public MEMM_loi
{
 private:
  double _sigma;
  double _mu;
  boost::variate_generator<boost::lagged_fibonacci19937&, boost::normal_distribution<> >& _gauss;
 public:
  /** Constructeur
      \param [in], gauss une loi normal utilisee pour les tirages.
  */
  MEMM_logNormal(boost::variate_generator<boost::lagged_fibonacci19937&, boost::normal_distribution<> >& gauss);
  /** faire un tirage
      \param [out] valeur du tirage.
  */
  virtual void tirage(double& v);
  
   /** Calcul la vraisemblance de l'echantillon ech avec le parametre de loi param
   * \param [in] npar, taille de l'echantillon
   * \param [in] ech, echantillon
   * \param [in] parametre de loi
   * \return la vraisemblance
   * Parametrage de la loi normale en A.
   * A=dobs/de = SIGMA2 + 1 = exp(sigma2);
   * La gaussienne suit une loi d'Esperance = -sigma2/2; de variance sigma2
   */
  virtual double logLik(int npar,const std::vector<double> & ech,double& param);
  /** Modifier le parametre d'indice indiceParam.
      \param [in] valeur a prendre en compte.
   */
  virtual void setDParam(unsigned int indiceParam, double & value);
  /** Destructeur
   */
  virtual ~MEMM_logNormal();
};

enum MEMM_logNormalCst{
  MEMM_LOGNORMAL_SIGMA=1,
  MEMM_LOGNORMAL_MU=2
};
#endif // MEMM_LOGNORMAL_H
