/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __MEMM_PARAMETERS_HPP__
#define __MEMM_PARAMETERS_HPP__

/*!
 * \file Parameter.hpp
 * \brief Parameter class
 * \author Jean-François Rey
 * \version 1.0
 * \date 20 April 2016
 */

#include <string>
#include <vector>
#include <limits>
#include <iostream>

using namespace std;

//! type def for pointer of  acception functions
typedef double (*accept_ratio)(double,double);

/*! \class Parameter
 * \brief Parameter class define parameter object
 *
 * The class define a parameter of prior distribution.
 *
 */
class Parameter {

  private:
    string name_;   //!< Parameter name (ID)
    int nb_of_value_; //!< Number of value for this parameter (vector)
    double * initial_value_;  //!< Pointer on initial value
    double * min_value_;  //!< Pointer on minimal value
    double * max_value_;  //!< Pointer on maximal value
    double * current_value_;  //!< Pointer on current value


  public:
    /*! \brief Parameter Constructor */
    Parameter(string name,double init, double min, double max, string prior, int size=1);
    /*! \brief Parameter Destructor */
    ~Parameter();

    double (*accept_ratio_)(double,double); //!< pointer on acceptation function

    /*! \brief get function acceptation for a prior type
     * \param prior : prior name
     * \return a pointer on acceptation ratio function
     */
    accept_ratio getAcceptRatio(string prior);
    
    /*! \brief ostream operator surdefinition */
    friend ostream& operator<< (ostream &out, const Parameter &param);

    double * getAllInitial(); //!< get pointer on initial value(s)
    double * getAllMin();     //!< get pointer on minimal value(s)
    double * getAllMax();     //!< get pointer on maximal value(s)
    double * getAllCurrent(); //!< get pointer on current value(s)

    string getName(); //!< get parameter name (ID)

    /*! \brief get an initial value
     * \param ind : value indice
     * \return a double
     */
    double getInitial(int ind=0);
    
    /*! \brief get a minimal value
    * \param ind : value indice
    * \return a double
    */
    double getMin(int ind=0);

    /*! \brief get a maximal value
    * \param ind : value indice
    * \return a double
    */
    double getMax(int ind=0);

    /*! \brief get current value value
    * \param ind : value indice
    * \return a double
    */
    double getCurrent(int ind=0);
  
    /*! \brief set an initial value
     * \param value : new value
     * \param ind : indice of the value
     */
     void setInitial(double value, int ind=0);
     /*! \brief set a minimal value
     * \param value : new value
     * \param ind : indice of the value
     */
     void setMin(double value, int ind=0);
     /*! \brief set a maximal value
     * \param value : new value
     * \param ind : indice of the value
     */
     void setMax(double value, int ind=0);
     /*! \brief set a current value
     * \param value : new value
     * \param ind : indice of the value
     */void setCurrent(double value, int ind=0);


};

#define NAME getName()            //!< shortcut to parameter name
#define INIT getAllInitial()[0]   //!< shortcut to first inital value parameter
#define MIN getAllMin()[0]        //!< shortcut to first minimal value paramter
#define MAX getAllMax()[0]        //!< shortcut to first maximal value parameter
#define CURRENT getAllCurrent()[0]  //!< shortcut to first current value paramter

/******************** Acceptation functions **************************/

#define PRIOR_NAME {"powerminusone","rectangularstep"}
const vector<string> prior_name(PRIOR_NAME);

/*! \brief power Minus One acceptation ratio
 * \param before : previous value
 * \param after : new value
 * \return ratio
 */
static inline double powerMinusOneRatio(double before, double after)
{
    return 1;
}

/*! \brief power Rectangular Step acceptation ratio
 * \param before : previous value
 * \param after : new value
 * \return ratio
 */
static inline double rectangularStepRatio(double before, double after)
{
  // Do not divide by Zero please
  return after/before;
}


#endif
