/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef INDIVIDU_H
#define INDIVIDU_H

/*!
 * \file individu.hpp
 * \brief individu class
 * \author Jean-François REY
 * \version 1.5
 * \date 21 April 2015
 *
 * The Individu class define trees or seeds variables. Mainly genotype informations.
 */

#include <string>
#include <iostream>
#include <iomanip>
#include "genotype.h"

/*! \class individu
 * \brief Individu main class
 * 
 * This class define individu (tree or seed) attribut
 */
class individu
{
    protected:
        std::string _nom; //!< individu name
        genotype _geno;   //!< individu genotype

    public:
        /*! \brief Constructor
         * \param name : individu name
         * \param geno : individu genotype
         *
         * Individu constructor with name and genotype.
         */
        individu(const std::string & name, const genotype & geno);

        /*! \brief Constructor
         * \param name : individu name
         *
         * Individu constrcutor with name.
         */
        individu(const std::string & name);

        /*! \brief Default Constructor
         *
         * Individu default constructor
         */
        individu() {};

        /*! \brief Destructor
         *
         * Individu destructor
         */
        virtual ~individu() {}

        /*! \brief get Individu name
         * \return individu name as string
         */
        const std::string & getName() const {return this->_nom;}

        /*! \brief get Individu genotype
         * \return genotype
         */
        genotype getGeno() const {return this->_geno;}

        /*! \brief get individu genotype Locus 
         * \param k : locus indice
         * \return a pointer on the locus at indice k of the individu genotype.
         */
        locus * getGeno(int k) const {return (this->_geno).getLocus(k);}

        /*! \brief print individu informations
         */
        virtual void afficher() const;


        /*! \brief Calcul mendel genetic distance between this individu and two others.
         * \param mere : an Individu
         * \param pere : an Individu
         * \param nl : number of locus in genotype to compare
         * \param na : number of alleles for each locus to compare
         * \param sizeall : for each locus contain <allele id,allele index>
         * \param freqall : for each locus contain alleles frequencies <locus,allele index>
         * \param materror : for each locus contain alleles genotype error. default value empty.
         *
         * Will calcul mendel genetic distance between this individu and two others individus (usually mother and father).
         */
        double mendel (const individu & mere , const individu & pere, int nl, std::vector<int> na,  std::vector < std::map<int,int> > & sizeall, const std::vector < std::vector<double> > & freqall, const std::vector < std::vector < std::vector <double> > > & materror = std::vector < std::vector < std::vector <double> > >()) const;


};

#endif // INDIVIDU_H
