/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __MEMM_SEEDLINGS_HPP__
#define __MEMM_SEEDLINGS_HPP__

#include "MEMM.hpp"
#include "Parameter.hpp"

struct CouplePot { int mere_pot; int pere_pot; double couple_prob;};

/*! \class MEMM_seedlings
 * \brief MEMM_seedlings class
 *
 * Mother and Father are unknow.
 *
 */
class MEMM_seedlings : public MEMM {

  public :

    /*! \brief Constructor
     */
    MEMM_seedlings();

    /*! \brief destructor
     */
    virtual ~MEMM_seedlings();

    virtual void mcmc(int nbIteration, int thinStep);
    virtual void mcmc_dyn(int nbIteration, int thinStep);

  
  protected :
    vector<vector<double>> DistSM;  //!< Distances between Seed Mother
    vector<int> NbMeres;  //!< number of mothers for a seed i
    vector<int> NbCouples;  //!< number of couple (mother-father) for a seed i
    vector<double> ProbMig;   //!< proba of outside site fertilization
    vector<vector< PerePot >> ProbMeres;  //!< potential mothers for a seed i
    vector<vector< CouplePot >> ProbCouples;  //!< potential couples for a seed i

    void loadSeeds(Configuration * p);
    void calculDistances(Configuration *p);
    void calculTransitionMatrix(Configuration *p);
    void loadParameters(Configuration * p);
    void loadOptions(Configuration *p);

    /*! \brief Calcul model log likelihood
     */
    long double logLik();
  
    

    double Scales;  //!< scales parameter initial, dimension of distance (as);
    double mScales; //!< min scales value
    double MScales; //!< max scales value
    double Shapes;  //!< shapes parameter initial value, dispersal kernel (bs)
    double mShapes; //!< min shapes value
    double MShapes; //!< max shapes value
    
    double Migs;  //!< migs initial value;
    double mMigs; //!< min migs value
    double MMigs; //!< max migs value
    double GamAs; //!< GamAs initial value
    double mGamAs;//!< min GamAs value
    double MGamAs;//!< max GamAs value
    double as; //!< mean dispersal distance (deltas)
    double abs; //!< exponential power dispersal kernel (1/as)^bs
    double ap; //!<  mean dispersal distance (delta)
    double abp; //!< exponential power dispersal kernel (1/a)^b

    //Début modif
    double Pi0s, mPi0s, MPi0s; // initial, min and max values for the zero-inflated parameter (seed)
    // Fin modif

    vector<double> Vec_Fecs;  //!< mother fecundity
    vector<double> Vec_Fecp;  //!< male fecunditiy
    
    MEMM_loi * pLois; //!< distribution for individual fecundities

 private :
    
};

#endif

