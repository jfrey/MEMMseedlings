/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __OPTIONS_PARAM_WIDGET__
#define __OPTIONS_PARAM_WIDGET__

/** \file OptionsParamWidget
 * \brief Options editor
 * \author Jean-Francois Rey
 * \version 1.0
 * \date 11 May 2016
 */

#include <string>
#include <iostream>
#include <QApplication>
#include <QWidget>
#include <QFormLayout>
#include <QLabel>
#include <QSpinBox>

#include "ConfigXML.hpp"

/**
 * \class OptionsParamWidget
 * \brief a widget containnning options editors
 */
class OptionsParamWidget : public QWidget
{
  Q_OBJECT
  public :

    /** \breif Constructor
     */
    OptionsParamWidget();

    /** \breif Constructor
     * \param p : a ConfigXML pointer
     */
    OptionsParamWidget(ConfigXML * p);

    /** \brief destructor
     */
    ~OptionsParamWidget();

  public slots :

    /*! \brief Change Seed value
    * \param v : value
    */
    void ChangeSeed(int v);


    /*! \brief Change BurnIn value
    * \param v : value
    */
    void ChangeBurnIn(int v);

    /*! \brief Change Number of Iteration
    * \param v : value
    */
    void ChangeIte(int v);

    /*! \brief Change Thin value
    * \param v : value
    */
    void ChangeThin(int v);

    /*!
    * \brief Set "controler-modele"
    * \param p : a ConfigXML object
    */
    void setParam(ConfigXML * p);

    /*!
    * \brief Initialize Widget
    *
    * Initialize widget with ConfigXML value.
    */
    void initValue();

  private :
    ConfigXML * param; ///< A ConfigXML pointer object.

    QFormLayout * vLayout; ///< widget main layout
    QSpinBox * seedSpinBox; ///< Seed value display
    QSpinBox * burnInSB;    ///< BurnIn value
    QSpinBox * iteSB;   ///< number of iteration
    QSpinBox * thinSB;  ///< Thin/step value



};

#endif
