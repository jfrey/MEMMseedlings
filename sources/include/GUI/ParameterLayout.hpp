/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __PARAMETER_LAYOUT__
#define __PARAMETER_LAYOUT__

/**
 * \file ParameterLayout.hpp
 * \brief a layout containning a parameter variable editor
 * \author Jean-Francois Rey
 * \version 1.0
 */

#include <string>
#include <iostream>
#include <QApplication>
#include <QWidget>
#include <QHBoxLayout>
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QMessageBox>
#include <QValidator>
#include <QRegExpValidator>
#include <QRegExp>

#include "ConfigXML.hpp"
#include "Tools.hpp"

//Q_DISABLE_COPY

using namespace std;

/**
 * \class ParameterLayout
 * \brief a layout containning parameters editor
 */
class ParameterLayout : public QHBoxLayout
{
  Q_OBJECT

  private :
    ConfigXML * p_; ///< a pointer on ConfigXML object
    string ID_; ///< Parameter ID
    QLabel * init_label_; ///< initial value label
    QLabel * size_label_; ///< size label
    QLabel * min_label_;  ///< minimal value label
    QLabel * max_label_;  ///< maximal value label
    QLabel * prior_label_;  ///< prior value label
    QDoubleSpinBox * init_; ///< initial value spinbox
    QDoubleSpinBox * min_;  ///< minimal value spinbox
    QDoubleSpinBox * max_;  ///< maximal value spinbox
    QSpinBox * size_; ///< size spinbox
    QLineEdit * name_;  ///< parameter name
    QComboBox * prior_; ///< prior combo box selector
    QPushButton * rm_button_; ///< delete button

    const QRegExp reg_valid_ = QRegExp("[a-zA-Z0-9_]{1,20}");

  public :
    /**
     * \brief constructor
     */
    ParameterLayout(ConfigXML * p, string param_name = "");

    /**
     * \brief Destructor
     */
    ~ParameterLayout();

  public slots : 
    /**
     * \brief slot when value changed
     */
    void valueChanged();

    /**
     * \brief delete this parameter layout
     */
    void delParameterLayout();
  signals:

    /**
     * \biref signal to remove a layout
     */
    void removeMe(QLayout * qo);

    /**
     * \brief signal when value changed
     */
    void valueAsChanged(QLayout * qo);

};

#endif
