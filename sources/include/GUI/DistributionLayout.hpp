/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __DISTRIBUTION_LAYOUT__
#define __DISTRIBUTION_LAYOUT__

/*!
 * \file DistributionLayout.hpp
 * \brief Layout for a distribution parameters
 * \author Jean-Francois REY
 * \version 1.à
 * \date 11 May 2016
 */


#include <string>
#include <iostream>
#include <QApplication>
#include <QWidget>
#include <QHBoxLayout>
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QMessageBox>
#include <QValidator>
#include <QRegExpValidator>
#include <QRegExp>

#include "ConfigXML.hpp"
#include "Tools.hpp"

using namespace std;

#define MAX_NB_OF_PARAMETERS 3


/**
 * \class DistributionLayout
 * \brief QHBoxLayout containing Distribution parameters
 */
class DistributionLayout : public QHBoxLayout
{
  Q_OBJECT

  private :
    ConfigXML * p_; ///< a ConfigXML pointer
    string ID_; ///< distribution ID
    QLineEdit * name_;  ///< distribution ID_ editable
    QLabel * laws_label_; ///< laws label
    QComboBox * laws_;  ///< laws combo box selections
    QLabel ** param_label_; ///< liste of parameters label
    QComboBox ** param_combo_;  ///< liste of parameters combo box selections
    unsigned int nb_of_param_;  ///< number of parameter for the actual selected law for this distribution
    
    QPushButton * rm_button_; ///< delete button

    const QRegExp reg_valid_ = QRegExp("[a-zA-Z0-9_]{1,40}");

    /**
     * \brief set law distribution parameters
     * \param param_name : list of parameter for the law
     * \param list_params : map parameter and parameters name reference in xml
     */
    void setDistributionParameters(vector<string> param_name, map<string,string> list_params );

  public :

    /**
     * \brief Constructor
     * \param p : a ConfigXML pointer
     * \param dist_name : distribution name in p
     */
    DistributionLayout(ConfigXML * p, string dist_name);

    /**
     * \brief destructor
     */
    ~DistributionLayout();
  public slots : 

    /**
     * \brief slot for changed value
     * will update the ConfigXML p and interface if needed
     */
    void valueChanged();

  /**
   * \brief delete a distribution
   * Will delete a distribution from ConfigXML and interface
   */
    void delDistributionLayout();

    /**
     * \brief update interface avfter law changed
     * Will update parameter name and parameter reference in XML Interface.
     */
    void lawComboValueChanged();
    
    /**
     * \brief update law parameters
     */
    void updateDistributionParameters();
  signals:

    /**
     * \brief signal to remove DistributionLayout
     */
    void removeMe(QLayout * qo);


};

#endif
