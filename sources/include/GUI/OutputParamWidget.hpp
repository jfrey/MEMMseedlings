/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __OUTPUT_PARAM_WIDGET__
#define __OUTPUT_PARAM_WIDGET__

/*!
 * \file OutputParamWidget.hpp
 * \brief QWidget containing output arguments
 * \author Jean-Francois REY
 * \version 1.0
 * \date 04 Dec 2013
 *
 * The OutputParamWidget class is a widget contaning argument editor for the output
 */

#include <string>
#include <iostream>
#include <QApplication>
#include <QWidget>
#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QPushButton>
#include <QFileDialog>
#include <QObject>
#include <QComboBox>
#include <QFormLayout>
//#include <QLine>
#include "ConfigXML.hpp"

using namespace std;

/*! \class OutputParamWidget
 * \brief class editor for output arguments
 *
 * The class implement an editor to output argument value.
 */
class OutputParamWidget : public QWidget
{

    Q_OBJECT
    public : 
        /*!
         * \brief Constructor
         *
         * OutputParamWidget constructor
         */
        OutputParamWidget();

        /*!
         * \brief Constructor
         * \param p : a ParamXML object
         *
         * OutputParamWidget constructor.
         * Set with a ConfigXML p object.
         */
        OutputParamWidget(ConfigXML * p);

        /*!
         * \brief Destructor
         *
         * OutputParamWidget destructor
         */
        ~OutputParamWidget();

        /*!
         * \brief Set "controler-modele"
         * \param p : a ConfigXML object
         */
        void setParam(ConfigXML * p);

        /*!
         * \brief Initialize Widget
         *
         * Initialize widget with ConfigXML value.
         */
        void initValue();

    public slots :

        /*!
         * \brief Open a dialog window slot
         *
         * Open a dialog window to choose an output file for Gama
         */
        void ChooseGamaFile();

        /*!
         * \brief Open a dialog window slot
         * 
         * Open a dialog window to choose an output file for IndFecFile
         */
        void ChooseIndFecFile();

        /*!
         * \brief Open a dialog window slot
         *
         * Open a dialog window to choose an output file for Dispersion param.
         */
        void ChooseDispParamFile();

    private : 

        ConfigXML * param; ///< A ConfigXML pointer object.

        QFormLayout * vLayout; ///< widget main layout

        QLineEdit * oLEdit1; ///< LineEdit 1 parameter
        QLineEdit * oLEdit2; ///< LineEdit 2 parameter
        QLineEdit * oLEdit3; ///< LineEdit 3 parameter

};



#endif
