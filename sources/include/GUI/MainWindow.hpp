/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __MAIN_WINDOW_HPP__
#define __MAIN_WINDOW_HPP__

/*!
 * \file MainWindow.hpp
 * \brief Main Window GUI class
 * \author Jean-Francois REY
 * \version 1.0
 * \date 04 Dec 2013
 */

#include <QApplication>
#include <QMainWindow>
#include <QWidget>
#include <QObject>
#include <QMenu>
#include <QMenuBar>
#include <QToolBar>
#include <QAction>
#include <QMessageBox>
#include <QFileDialog>
#include <QFileInfo>
#include "InputParamWidget.hpp"
#include "ParamParamWidget.hpp"
#include "OutputParamWidget.hpp"
#include "OptionsParamWidget.hpp"
#include "ConfigXML.hpp"

/*! \class MainWindow
 * \brief Main Window GUI class
 *
 * The main MEMM window GUI to modify parameters.<br>
 * Menu + ToolBar<br>
 * Include three tabs : <br>
 * - InputParamWidget
 * - ParamParamWidget
 * - OutputParamWidget
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT
    public :

        /*! \brief Constructor
         * \param p : pointer on ConfigXML class
         *
         * MainWindow constructor
         */
        MainWindow(ConfigXML * p);

        /*! \brief Destructor
         */
        ~MainWindow();

        /*! \brief Set ConfigXML class
         * \param p : ConfigXML object pointer
         */
        void setParam(ConfigXML *p);

    public slots :
        /*! \brief Open a dialog window to select a file
         * 
         * Open a dialogue window to select an XML file and load it.
         */
        void actionOpenFile();

        /*! \brief Save parameters in file
         */
        void actionSaveFile();

        /*! \brief Open a dialog window to select a file to save parameters
         */
        void actionSaveFileAs();

        /*! \brief Close main window
         */
        void actionQuit();

    private :
        ConfigXML * param; ///< Config object
        QTabWidget * tabs; ///< Mainwondow tabs
        InputParamWidget * iPW; ///< InputParam tab
        ParamParamWidget * pPW; ///< ParamParam tab
        OutputParamWidget * oPW; ///< OutputParam tab
        OptionsParamWidget * opPW; ///< OptionsParam tab

        /*! \brief set the main widget
         * \param p : ConfigXML object pointer
         *
         * Set and load main widget.
         */
        QWidget * setMainWidget(ConfigXML * p);

        /*! \brief set menu and tool bar
         */
        void createMenuAndToolBar();

        /*! \brief Display value from ConfigXML object
         * \param p : ConfigXML objet pointer
         */
        void initValue(ConfigXML *);


};

#endif
