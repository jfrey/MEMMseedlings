/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/**
 * \file ParamParamWidget.hpp
 * \brief Configuration arguments editor
 * \author Jean-Francois Rey
 * \version 1.1
 * \date 11 May 2016
 */

#ifndef __PARAM_PARAM_WIDGET__
#define __PARAM_PARAM_WIDGET__

#include <string>
#include <iostream>
#include <QApplication>
#include <QWidget>
#include <QGroupBox>
#include <QPushButton>
#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QObject>
#include <QComboBox>
#include <QString>
#include <QGridLayout>
#include <QScrollArea>
//#include <QLine>
#include "ConfigXML.hpp"
#include "Tools.hpp"
#include "ParameterLayout.hpp"
#include "DistributionLayout.hpp"

using namespace std;

/*! \class ParamParamWidget
 * \brief Class editor for parameters arguments
 *
 * The class implement an editor for parameters arguments values.
 */
class ParamParamWidget : public QWidget
{

    Q_OBJECT
    public : 
        /*! \brief Constructor
         *
         * ParamParamWidget constructor.
         */
        ParamParamWidget();

        /*! \brief Constructor
         * \param p : ConfigXML object
         *
         * ParamParamWidget constructor.
         */
        ParamParamWidget(ConfigXML * p);

        /*! \brief Destructor
         *
         * ParamParamWidget Destructor.
         */
        ~ParamParamWidget();

        /*! \brief Set "controler-modele" object
         * \param p : a ConfigXML Object
         */
        void setParam(ConfigXML *p);

        /*! \brief Initialize Widget
         *
         * Initialize widget with ConfigXML value.
         */
        void initValue();

        /**
         * \brief add a new parameter line editor
         * \param param_name : a parameter name from xml config, if empty create an empty line editor
         */
        void addNewParamLine(QString param_name);

        /**
         * \brief add a new distribution line editor
         * \param dist_name : a distribution name from xml config, if empty create an empty editor
         */
        void addNewDistLine(QString dist_name);

    public slots :

      /**
       * \briefi slot delete a parameter line
       */
      void delParam(QLayout *);

    /**
     * \brief slot delete a distribution line
     */
      void delDist(QLayout *);

      /**
       * \brief slot add param line
       */
      void addParamLine();

      /**
       * \brief slot ass distribution line
       */
      void addDistLine();

      /** 
       * \brief slot update parameters
       */
      void updatedParam(QLayout *);

      signals:
      /**
       * \brief signal for update Distribution Layout
       */
      void updateDist();


      
    private : 

        ConfigXML * param;  ///< A ConfigXML object

        QGroupBox * pBoxGroup;  ///< parameters group box
        QGroupBox * dBoxGroup;  ///< distributions group box
        QVBoxLayout * paramPack;  ///< parameters layout
        QVBoxLayout * distPack; ///< distribution layout
        QVBoxLayout * vLayout;  ///< Widget Main layout
        QScrollArea * scroll_param; ///< scroll for parameters
        QScrollArea * scroll_dist;  ///< scroll for distribution

        //void addParamSection();

};



#endif
