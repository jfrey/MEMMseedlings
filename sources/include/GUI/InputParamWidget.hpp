/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/*!
 * \file InputParamWidget.hpp
 * \brief QWidget containing Input parameters arguements
 * \author Jean-Francois REY
 * \version 1.0
 * \date 11 May 2016
 *
 * The InputParamWidget class is a widget containing argument editor for the input parameters.
 */
#ifndef __INPUT_PARAM_WIDGET__
#define __INPUT_PARAM_WIDGET__

#include <string>
#include <iostream>
#include <QApplication>
#include <QWidget>
#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QPushButton>
#include <QFileDialog>
#include <QObject>
#include <QComboBox>
#include <QGroupBox>
#include <QFormLayout>
#include <QCheckBox>
//#include <QLine>
#include "ConfigXML.hpp"
#include <string>
using namespace std;

/*!
 * \class InputParamWidget
 * \brief Class editor for Input parameters argument value.
 *
 * The class implement an editor for input paramters values.
 */
class InputParamWidget : public QWidget
{

    Q_OBJECT
    public : 
        /*! \brief Constructor
         *
         * Constructor
         */
        InputParamWidget();
        
        /*! \brief Constructor
         * \param p : a ConfigXML object
         *
         * Constructor
         */
        InputParamWidget(ConfigXML * p);

        /*! \brief Destructor
         *
         * Destructor
         */
        ~InputParamWidget();

        /*! \brief Set "controler-modele"
         * \param p : a ConfigXML object
         */
        void setParam(ConfigXML *p);

        /*! \brief Initialize Widget
         *
         * Initialize widget with ConfigXML value.
         */
        void initValue();

    public slots :

        /*! \brief Dialog to choose Parent file
         */

        void ChooseFileParent();
        
        /*! \brief Dialog to choose Offspring file
         */
        void ChooseFileOffspring();

        /*! \brief slot for Allelic frequencies checkbox
         */
        void CheckAF(int state);
        /*! \brief Dialog to choose Allelic Frequencies file
         */
        void ChooseFileAF();

        /*! \brief slot for Distance checkbox
         */
        void CheckDF(int state);

        /*! \brief Dialog to choose Distance file
         */
        void ChooseFileDF();
  
        /*! \brief slot for locus error checkbox
         */
        void CheckLE(int state);
        /*! \brief Dialog to choose locus error file
         */
        void ChooseFileLE();

        /*! \brief Change Number of Locus for parent file
         * \param v : value
         */
        void ChangeLocusNumberParent(int v);

        /*! \brief Change Class Covariates value
         * \param v : value
         */
        void ChangeClassCovariatesParent(int v);

        /*! \brief Change Quantities Covariates value
         * \param v : value
         */
        void ChangeQtCovariatesParent(int v);

        /*! \brief Change Weight Var value
         * \param v : value
         * */
        void ChangeWeightVarParent(int v);

        /*! \brief Change number of locus for Offspring file
         * \param v : value
         */
        void ChangeLocusNumberOffspring(int v);

    private : 

        ConfigXML * param;  ///< A ConfigXML object

        QFormLayout * vLayout;  ///< Main widget Layout
        QLineEdit * pLEdit1;    ///< Parents file name
        QSpinBox * nlSpinBox;   ///< Number of Locus editor
        QSpinBox * ccSpinBox;   ///< Class covariates editor
        QSpinBox * qcSpinBox;   ///< Quantitative covariates editor
        QSpinBox * wvSpinBox;   ///< Weighting variables editor
        QLineEdit * oLEdit2;    ///< Offspring file name
        QSpinBox * nlSpinBox2;   ///< Number of Locus editor
        QCheckBox * afCBox3;    ///< check box allelic freq enable
        QLineEdit * afLEdit3;   ///< Allelic Frequencies file name
        QPushButton * cFile3;   ///< choose file button
        QCheckBox * dfCBox4;    ///< check box distance parents enable
        QLineEdit * dfLEdit4;   ///< distance file name
        QPushButton * cFile4;   ///< choose file button
        QCheckBox * leCBox5;    ///< check box locus error enable
        QLineEdit * leLEdit5;   ///< locus error file name
        QPushButton * cFile5;   ///< choose file button
};



#endif

