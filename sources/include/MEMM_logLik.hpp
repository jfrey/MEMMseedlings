/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __MEMM_LOGLIK_HPP__
#define __MEMM_LOGLIK_HPP__

/*!
 * \file MEMM_logLik.hpp
 * \brief MEMM_logLik class
 * \author Jean-Francois REY
 * \version 1.0
 * \date 23 Sept 2014
 *
 * The MEMM_loglik class enables to estimate the pollen dispersal kernel and the variance in male fecundity from spatial and genetic data (microsatellites) concerning adult plants and sampled seeds (see Klein et al. 2008 Molecular Ecology).
 */

#include "MEMM.hpp"

using namespace std;

/*! \class MEMM_logLik
 * \brief MEMM_logLik estimate pollen dispersal kernel and the variance in male fecundity
 *
 * This class enables to estimate the pollen dispersal kernel and the variance in male fecundity from spatial and genetic data (microsatellites) concerning adult plants and sampled seeds (see Klein et al. 2008 Molecular Ecology).
 */
class MEMM_logLik : public MEMM {

  public :

    /*! \brief Constructor
     *
     * MEMM_logLik constructor
     */
    MEMM_logLik();

    /*! \brief Destructor
     *
     * MEMM_logLik class destructor
     */
    ~MEMM_logLik();

    virtual void mcmc(int nbIteration, int thinStep);
    virtual void mcmc_dyn(int nbIteration, int thinStep);

  protected :
    vector<int> MerDesc;  //!< list of mother ID of descendants
    vector<int> Meres;    //!< mother ID to father ID
    map<string,int> IDmere; //!< mother name and id
    int Nm;   //!< number of mothers
    vector<double> ProbSelf;  //!< proba of self fertilization
    vector<double> ProbMig;   //!< proba of outside site fertilization
    vector<vector<PerePot>> ProbPeres;  //!< proba for each seeds to be fertilizated by site father
    vector<int> NbPeres;    //!< number of posible father for a seed

    // Parameters
    double a;   //!< mean dispersal distance (delta)
    double ab;  //!< exponential power dispersal kernel (1/a)^b
    vector<double> Vec_Fec; //!< male fecunditiy

    virtual void loadSeeds(Configuration * p);
    virtual void calculDistances(Configuration * p);
    virtual void calculTransitionMatrix(Configuration *p);
    virtual void loadParameters(Configuration *p);

    /*! \brief Calcul the likelihood of the model
     *
     * Will calcul the log likelihood of the model
     */
    long double compute();
    long double computeDyn();
  private :


};



#endif
