/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __MEMM_CONFIGTXT_HPP__
#define __MEMM_CONFIGTXT_HPP__

/*!
 * \file ConfigTXT.hpp
 * \brief TXT Configuration manager
 * \author Jean-Francois REY
 * \version 1.0
 * \date 04 Dec 2013
 *
 * The Config class manage parameters from arguments and XML file (for MEMM project).
 * Deprecated
 */


#include <iostream>
#include <fstream>
#include <cstring>
#include <limits>

#include "Configuration.hpp"

/*! \class ConfigTXT
 * \brief Configuration manager for txt file
 *
 * This class manage parameters as arguments or as TXT file.
 * Deprecated ! See ConfigXML
 */
class ConfigTXT : public Configuration {
  public :
    ConfigTXT();
    ConfigTXT(char * filein);
    ~ConfigTXT();
    //template<typename Type> Type getValue(MEMM_parameters_t valueName);
    virtual char * getValue(MEMM_parameters_t valueName);
    virtual bool setValue(MEMM_parameters_t valueName, char * value);
    virtual unordered_map <string, Parameter *> loadParameters(){return(* new unordered_map <string, Parameter *>);};
    virtual string getDistributionType(string name){return string(NULL); };
    virtual map<string,string> getDistributionParameters(string lawname){ return(* new map<string,string>);};



  private : 
    ifstream paramFileStream;

    void goToLine(int lineNumber);
};


#endif
