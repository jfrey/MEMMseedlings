/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __MEMM_SEEDLINGS_2KERNELS_HPP__
#define __MEMM_SEEDLINGS_2KERNELS_HPP__

#include "MEMM.hpp"
#include "MEMM_seedlings.hpp"
#include "MEMM_beta.h"
#include "MEMM_zigamma.h"

/*! \class MEMM_seedlings_2kernels
 * \brief MEMM_seedlings_2kernels class
 *
 * Mother and Father are unknow.
 * Two kernels fort SDD and LDD are mixed with variable weights
 *
 */
class MEMM_seedlings_2kernels : public MEMM_seedlings {

  public :

    /*! \brief Constructor
     */
    MEMM_seedlings_2kernels();

    /*! \brief destructor
     */
    virtual ~MEMM_seedlings_2kernels();

    virtual void mcmc(int nbIteration, int thinStep);
    virtual void mcmc_dyn(int nbIteration, int thinStep);

  
  protected :

    void loadParameters(Configuration * p);

    /*! \brief Calcul model log likelihood
     */
    long double logLik();
  
    
private :

    double ScalesLDD;  //!< LDD scales parameter initial, dimension of distance (as);
    double mScalesLDD; //!< min LDD scales value
    double MScalesLDD; //!< max LDD scales value
    double ShapesLDD;  //!< LDD shapes parameter initial value, dispersal kernel (bs)
    double mShapesLDD; //!< min LDD shapes value
    double MShapesLDD; //!< max LDD shapes value

    double asLDD, absLDD; // A VERIFIER
    double KnormLDD, Knorm; // A VERIFIER
  
    
    double AlphaLDD;  //!< Beta-distribution for LDD: alpha parameter initial value
    double mAlphaLDD; //!< Beta-distribution for LDD: min alpha value
    double MAlphaLDD; //!< Beta-distribution for LDD: max alpha value
    double BetaLDD;  //!< Beta-distribution for LDD: beta parameter initial value
    double mBetaLDD; //!< Beta-distribution for LDD: min beta value
    double MBetaLDD; //!< Beta-distribution for LDD: max beta value
    
    vector<double> Vec_FreqLDD;  //!< frequency of long distance dispersal
    
    MEMM_loi * pLoiLDD; //!< distribution for individual frequencies of LDD


};

#endif

