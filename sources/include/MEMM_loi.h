#ifndef MEMM_LOI_H
/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#define MEMM_LOI_H

#include <vector>
#include <map>
#include <string>

using namespace std;

enum MEMM_loiCst{
  MEMM_LOI_GAMA,
  MEMM_LOI_ALPHA,
  MEMM_LOI_BETA,
  MEMM_LOI_ZIPROB

};

// add here law ID
enum MEMM_collecLois{
  MEMM_IDLOI_LN,
  MEMM_IDLOI_GAMMA,
  MEMM_IDLOI_BETA,
  MEMM_IDLOI_ZIGAMMA
};

/** 
 * \brief Le but de cette classe est d'etre surchargee par des lois specifiques.
 */
class MEMM_loi
{
    public:
  /** Constructeur
   */
    int _IDLoi;
    static string LAWS_NAMES[];
    static map<int,vector<string>> LAWS_PARAMETERS;
    static const map<string,vector<string>> laws_name_and_parameters;// LAWS_NAME_AND_PARAMETERS;
    
    MEMM_loi();
  /** faire un tirage
      \param [out] valeur du tirage.
   */
  virtual void tirage(double& v) =0;
  /** Calcul la vraisemblance de l'echantillon ech avec le parametre de loi param
   * \param [in] npar, taille de l'echantillon
   * \param [in] ech, echantillon
   * \param [in] parametre de loi
   * \return la vraisemblance
   */
   virtual double logLik(int npar,const std::vector<double> & ech,double& param,double& param2) {};
  virtual double logLik(int npar,const std::vector<double> & ech,double& param)=0;
  /** Modifier le parametre d'indice indiceParam.
   *   \param [in] indice du parametre
   *   \param [in] valeur a prendre en compte.
   */
  virtual void setDParam(unsigned int indiceParam, double & value) = 0;
  /** Destructeur
   */
  virtual ~MEMM_loi();
};

#endif // Loi
