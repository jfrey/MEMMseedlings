/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __CONFIG_XML_HPP__
#define __CONFIG_XML_HPP__

/*!
 * \file ConfigXML.hpp
 * \brief XML Configuration manager
 * \author Jean-Francois REY
 * \version 1.1
 * \date 11 May 2016
 *
 * The Config class manage parameters from arguments and XML file (for MEMM project).
 * TODO implement arguments managing.
 */
#include <string>
#include <iostream>
#include <typeinfo>
#include <unordered_map>
#include <map>
#include "XMLInterface.hh"
#include "Configuration.hpp"
#include "Parameter.hpp"
#include "Tools.hpp"

using namespace std;

/*! \def DEFAULT_XML_CONFIG_MEMM
 * Constant containing default parameters/settings as XML.
 */
#define DEFAULT_XML_CONFIG_MEMM "<?xml version='1.0' encoding='UTF-8'?>\
<document>\
<MEMM>\
    <input>\
        <!-- file containing information about parents -->\
        <file description='parents'>\
            <filename>AlisierPar.txt</filename>\
            <numbers_of_locus>6</numbers_of_locus>\
            <class_covariates>6</class_covariates>\
            <quantitative_covariables>2</quantitative_covariables>\
            <weighting_variables>1</weighting_variables>\
        </file>\
        <!-- file containing information about offspring -->\
        <file description='offspring'>\
            <filename>AlisierDesc.txt</filename>\
            <numbers_of_locus>6</numbers_of_locus>\
        </file>\
        <!-- mode of computation of AF and file containing AF -->\
        <AF enable='true'>\
            <mode>file</mode>\
            <value>freqall.txt</value>\
        </AF>\
        <Dist enable='true'>\
          <mode>file</mode>\
          <value>distfile.txt</value>\
        </Dist>\
        <LE enable='false'>\
          <filename>LocusError.txt</filename>\
        </LE>\
    </input>\
    <parameters>\
        <!-- individual fecundities distribution -->\
        <individual_fecundities distribution='LN' />\
         <!-- GamA dobs/dep -->\
        <param name='gama' type='double'>\
          <init>2.0000</init>\
          <min>1.0000</min>\
          <max>1000.0000</max>\
          <prior name='powerminusone' type='default'/>\
        </param>\
        <!-- mean dispersal distance delta -->\
        <param name='scale' type='double'>\
          <init>100.0</init>\
          <min>0.0</min>\
          <max>10000.0000</max>\
          <prior name='powerminusone' type='default'/>\
        </param>\
        <!-- shape b -->\
        <param name='shape' type='double'>\
          <init>1.0000</init>\
          <min>0.1</min>\
          <max>10.0000</max>\
          <prior name='powerminusone' type='default'/>\
        </param>\
        <!-- migration rate m -->\
        <param name='mig' type='double'>\
          <init>0.5000</init>\
          <min>0.1000</min>\
          <max>1.0000</max>\
          <prior name='powerminusone' type='default'/>\
        </param>\
        <!-- selfting rate s -->\
        <param name='self' type='double'>\
          <init>0.0500</init>\
          <min>0.0</min>\
          <max>0.1000</max>\
          <prior name='powerminusone' type='default'/>\
        </param>\
        <!-- individual fecundities distribution -->\
        <param name='individual_fecundities' type='distribution'>\
          <law name='gamma'>\
          <law_parameters>\
            <lp type='param' name='gama'>gama</lp>\
            <!--<lp type='double' name='gama'>1.00</lp>-->\
            <!--<lp type='double' name='sigma'>1.00</lp>-->\
            <!--<lp type='double' name='mu'>1.00</lp>-->\
          </law_parameters>\
        </law>\
      </param>\
      <param name='kernel_dispersion' type='model'>\
        <model_parameters>\
          <mp type='param' name='scale'>scale</mp>\
          <mp type='param' name='shape'>shape</mp>\
          </model_parameters>\
      </param>\
    </parameters>\
    <options>\
        <seed>12345</seed>\
        <!-- burn-in iteration -->\
        <burnin>5000</burnin>\
        <ite>50000</ite>\
        <thin>20</thin>\
    </options>\
    <!-- Output file -->\
    <output>\
        <!-- dobs/dep -->\
        <file type='GamA' name='ParamFec.txt' />\
        <!-- Individual fecundities -->\
        <file type='IndFec' name='IndivFec.txt' />\
        <!-- Dispersal parameters -->\
        <file type='Disp' name='ParamDisp.txt' />\
    </output>\
</MEMM>\
</document>"

/*! \defgroup XMLVariables XPath acces XML variables
 * @{
 */
#define MEMM_ROOT "//MEMM"
#define INPUT_PARENTS_FILE_NAME "//MEMM/input/file[@description='parents']/filename"
#define INPUT_PARENTS_FILE_NUMBERS_OF_LOCUS "//MEMM/input/file[@description='parents']/numbers_of_locus"
#define INPUT_PARENTS_FILE_CLASS_COV "//MEMM/input/file[@description='parents']/class_covariates"
#define INPUT_PARENTS_FILE_QT_COV "//MEMM/input/file[@description='parents']/quantitative_covariables"
#define INPUT_PARENTS_FILE_WEIGHT_VAR "//MEMM/input/file[@description='parents']/weighting_variables"
#define INPUT_OFFSPRING_FILE_NAME "//MEMM/input/file[@description='offspring']/filename"
#define INPUT_OFFSPRING_FILE_NUMBERS_OF_LOCUS "//MEMM/input/file[@description='offspring']/numbers_of_locus"
#define INPUT_AF "//MEMM/input/AF"
#define INPUT_AF_FILE_MODE "//MEMM/input/AF/mode"
#define INPUT_AF_FILE_NAME "//MEMM/input/AF/value"
#define INPUT_DIST "//MEMM/input/Dist"
#define INPUT_DIST_FILE_MODE "//MEMM/input/Dist/mode"
#define INPUT_DIST_FILE_NAME "//MEMM/input/Dist/value"
#define INPUT_MEMM_LOCUS_ERROR "//MEMM/input/LE"
#define INPUT_MEMM_LOCUS_ERROR_FILE "//MEMM/input/LE/filename"
#define PARAM_SEED "//MEMM/options/seed"
#define PARAM_BURNIN "//MEMM/options/burnin"
#define PARAM_ITE "//MEMM/options/ite"
#define PARAM_THIN "//MEMM/options/thin"
#define OUTPUT_GAMA_FILE_NAME "//MEMM/output/file[@type='GamA']"
#define OUTPUT_GAMA_FILE_NAME_ATTRIBUT "name"
#define OUTPUT_IND_FEC_FILE_NAME "//MEMM/output/file[@type='IndFec']"
#define OUTPUT_IND_FEC_FILE_NAME_ATTRIBUT "name"
#define OUTPUT_DISP_FILE_NAME "//MEMM/output/file[@type='Disp']"
#define OUTPUT_DISP_FILE_NAME_ATTRIBUT "name"
/*!@}*/


/*! \class ConfigXML
 * \brief Configuration manager for XML format
 *
 * This class manage parameters as arguments or as XML file.
 */
class ConfigXML : public Configuration {

    private :
        XMLInterface * param_xml; ///< XML tools
        string param_filename; ///< paramater file name

    public :
        /*!
         * \brief Constructor
         *
         * ConfigXML class constructor
         */
        ConfigXML();

        /*! \brief Constructor
         * \param filename : parameters XML file to open
         *
         *  ConfigXML class constructor
         */
        ConfigXML(string filename);
        ConfigXML(char * filename);

        /*! \brief Destructor
         *
         *  ConfigXML class destructor
         */
        ~ConfigXML();

        /*! \brief Create and load default parameters as XML format
         *
         * Create new xml document and load DEFAULT_XML_CONFIG_MEMM as default parameters
         */
        void createDefaultXMLConfig();

        /*! \brief set verbose mode
         * \param b : true active verbose
         */
        void setVerbose(bool b);

        /*! \brief Load an xml parameters file
         * \param filename : xml parameters file
         * \return 1 if ok
         *
         */
        bool loadXMLConfig(string filename);

        /*! \brief Print xml file in stdout
         */
        bool printXMLConfig();

        /*! \brief Print help message
         *
         * Print help message for parameters
         */
        void printHelpMessage();

        /*! \brief Add an element and value in xml format
         * \param expr : XPath expression
         * \param name : element name to add
         * \param text : element value, default ""
         * \return 1 if OK otherwise return value < 1
         *
         * Add an element name with value text in a xml node represented as XPath expression.
         *
         */
        bool addElement(string expr, string name, string text="");

        /*! \brief change enable attribut of an element
         * \param name : element (tag) name
         * \param enable : default enable value is true
         * \return true if find the element
         */
        bool enableElement(string name, bool enable=true);

        /*! \brief Get value of a parameter
         * \param expr : XPath expression
         * \param attribut : element attribut name of the value to return
         * \return the value as a string.
         *
         * Get the value of an element or an element attribut value.
         */
        string getValue(string expr, string attribut="");

        /*! \brief Get value of a parameter
         * \param valueName : Predefine parameter identifier from MEMM_parameters_t
         * \return the value as aar *.
         *
         * Get the value of an element or an element attribut value.
         */
        virtual char * getValue(MEMM_parameters_t valueName);

        /*! \brief Set value of a parameter
         * \param expr : XPath expression
         * \param value : parameter value to set
         * \param attribut : parameter attribut to set value, if none ""
         *
         * Set the value of an element or an element attribut value.
         */
        bool setValue(string expr,string value, string attribut="");

        /*! \brief Set value of a parameter MEMM_parameter_t
         * \param valueName : a MEMM_paramter_t variable
         * \param value : parameter value to set
         *
         * Set the value of an element or an element attribut value.
         */
        virtual bool setValue(MEMM_parameters_t valueName, char * value);

        /*! \brief Save parameters
         * \return 1 if OK
         *
         * Save parameters as XML file if already specified.
         */
        bool save();

        /*! \brief Save parameters in file
         * \param filename : file name to save parameters
         * \return 1 if OK
         * 
         * Save parameters as XML file if already specified.
         */
        bool saveFile(string filename);

        /*! \brief Get filename
         * \return filename as string
         *
         * Get the filename of the parameters record if any.
         */
        string getFileName();

        /*! \brief load Parameters 
         * \retunr a map of loaded paramter
         */
        unordered_map <string, Parameter *> loadParameters();

        /*! \brief get all variable parameters names
         * \return a vecotr of name
         */
        vector<string> getParametersName();

        /*! \brief remove a parameter 
         * \param name : parameter name to delete
         */
        bool deleteParameter(string name);

        /*! \brief Add or modifiy a parameter variable
         * \param name : parameter name
         * \param init : initial value
         * \param min : minimal value
         * \param max : maximal value
         * \param size : vector size (covariable)
         * \param prior : prior name
         *
         * Add or modify a parameter variable in XML file. If size > 1 that will create a vector.
         */
        bool setParameter(string name, double init, double min, double max, int size, string prior);

        /*! \brief get a parameter variable initial value
         * \param param_name : the parameter name
         * \return a double
         */
        double getParameterInitValue(string param_name);

        /*! \brief get a parameter variable minimum value
         * \param param_name : the paramter name
         * \return a double
         */
        double getParameterMinValue(string param_name);

        /*! \brief get a parameter variable maximum value
         * \param param_name : the paramter name
         * \return a double
         */
        double getParameterMaxValue(string param_name);

        /*! \brief get a parameter variable size
         * \param param_name : the paramter name
         * \return an interger
         */
        int getParameterSizeValue(string param_name);

        /*! \brief get a parameter variable prior name
         * \param param_name : the paramter name
         * \return a string
         */
        string getParameterPriorValue(string param_name);

        /*! \brief Get Distribution name
         * \return a vector of string 
         */
        vector<string> getDistributionsName();

        /*! \brief get distribution type
         * \param namelaw : name of the distribution law
         * \return law type
         */
        string getDistributionType(string namelaw);

        /*! \brief get Distribution parameters list
         * \param lawname
         * \return a map matching distribution parameter name and a parameter name
         */
        map<string,string> getDistributionParameters(string lawname);

        /*! \brief remove a distribution
         * \param name : a distribution name
         */
        bool deleteDistribution(string name);

        /*! \brief add or modify a distribution 
         * \param name : distribution name
         * \param law : distribution law name
         * \param name_ref : map of parameter name and parameter reference name in the xml
         */
        bool setDistribution(string name, string law, map<string,string> name_ref);

    private :

        /*! \brief load a parameter section
         * \param paramNode : pointer on xml tag param
         * \return a Parameter object
         */
        Parameter * loadParameterD(xmlNodePtr paramNode);
};



#endif


