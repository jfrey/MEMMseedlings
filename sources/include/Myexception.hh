/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/*!
 * \file Myexception.hh
 * \brief Class Myexception definition
 * \author Jean-François REY
 * \version 1.0
 * \date 4 mai 2010
 *
 * Myexception derived from public heritage of exception. 
 */

#ifndef __MYEXCEPTIONS__
#define __MYEXCEPTIONS__

#include <exception>
#include <string>

/*!
 *  \class Myexception
 *  \brief Little exception enhancement in order to give an explicit message when an exception is throwed.
 *
 *  Little exception enhancement in order to give an explicit message when an exception is throwed.
 *	Myexception class derived from public heritage of exception class.
 */
class Myexception: public std::exception
{

private:
    int ex_number;               //!< Error number
    std::string ex_sentence;       //!< Error description
    int ex_rank;               //!< Error rank

public:
    /*! \brief Constructor
     * \param number : exception number
     * \param sentence : exception message
     * \param rank : exception rank
     */
    Myexception(int number=0, const std::string& sentence="", int rank=0) throw()
         :ex_number(number),ex_sentence(sentence),ex_rank(rank)
    {}
 
    /*! \brief Print exception message
     */
     virtual const char* what() const throw()
     {
         return ex_sentence.c_str();
     }
     
     /*! \brief Get rank
      */
     int getRank() const throw()
     {
          return ex_rank;
     }
    
    /*! \brief Destructor
     */
    virtual ~Myexception() throw()
    {}
 
};

#endif
