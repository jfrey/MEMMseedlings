/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef LOCUS_H
#define LOCUS_H

/*! 
 * \file locus.h
 * \brief locus class
 * \author Etienne Klein
 * \author Jean-Francois Rey
 * \version 1.5
 * \date 21 Aril 2015
 */

#include <map>
#include <vector>
#include <string>
#include <iostream>

/*! \class locus
 * \brief locus Class
 */
class locus
{
    public:

        /*! \brief Constructor
         * \param all1 : allele id
         * \param all2 : allele id
         *
         * Will construct a locus with two alleles. No information by default equal to -1.
         */
        locus(int all1=-1, int all2=-1);

        /*! \brief destructor
         */
        ~locus();

        /*! \brief print information
         */
        void afficher() const;

        /*! \brief compare two locus
         */
        bool operator== (const locus & other) const; 

        /*! \brief get an allele
         * \param a : allele index (0 or 1)
         * \return allele id
         */
        int getAllele(int a){ return _allele[a];}

        /*! \brief Calcul mendel genetic distance between this locus and two others.
         * \param mere : a locus
         * \param pere : a locus
         * \param na : number of different allele in this locus
         * \param sizeall : contains <allele id,allele index>
         * \param freqall : contains allele frequancies 
         * materror : alleles genotype error. default value empty.
         *
         * Will calcul mendel genetic distance between this locus and two others locus (usually mother and father).
         */
        double mendel1(const locus & mere , const locus & pere, int na,  std::map<int,int> sizeall, const std::vector<double> & freqall, const std::vector< std::vector <double> > & materror = std::vector<std::vector<double>>()) const;

    protected:
    private:
        int _allele[2]; //!< two alleles
};

#endif // LOCUS_H
