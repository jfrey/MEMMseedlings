#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include "ConfigTXTTest.hpp"
#include "../include/ConfigTXT.hpp"

ConfigTXTTest::ConfigTXTTest(){
        ptxt = new ConfigTXT("./ParametersTest.txt");
    };

ConfigTXTTest::~ConfigTXTTest(){
  delete ptxt;
    };


TEST_F(ConfigTXTTest, ParametersValue)
{
    EXPECT_STREQ("AlisierParTest.txt",ptxt->getValue(MEMM_PARENTS_FILE_NAME));
    EXPECT_STREQ("6",ptxt->getValue(MEMM_PARENTS_FILE_NUMBER_OF_LOCUS));
    EXPECT_STREQ("6",ptxt->getValue(MEMM_PARENTS_FILE_CLASS_COV));
    EXPECT_STREQ("2",ptxt->getValue(MEMM_PARENTS_FILE_QT_COV));
    EXPECT_STREQ("1",ptxt->getValue(MEMM_PARENTS_FILE_WEIGHT_VAR));
    EXPECT_STREQ("AlisierDescTest.txt",ptxt->getValue(MEMM_OFFSPRING_FILE_NAME));
    EXPECT_STREQ("6",ptxt->getValue(MEMM_OFFSPRING_FILE_NUMBERS_OF_LOCUS));
    EXPECT_STREQ("file",ptxt->getValue(MEMM_AF_FILE_MODE));
    EXPECT_STREQ("freqallextTest.txt",ptxt->getValue(MEMM_AF_FILE_NAME));
   // EXPECT_STREQ("",ptxt->getValue(MEMM_LOCUS_ERROR_FILE));
    //EXPECT_STREQ("LN",ptxt->getValue(MEMM_IND_FEC_DIST));
    EXPECT_STREQ("12345",ptxt->getValue(MEMM_SEED));
   /* EXPECT_STREQ("2.",ptxt->getValue(MEMM_GAMA_INIT));
    EXPECT_STREQ("1.",ptxt->getValue(MEMM_GAMA_MIN));
    EXPECT_STREQ("1000.",ptxt->getValue(MEMM_GAMA_MAX));
    EXPECT_STREQ("100.",ptxt->getValue(MEMM_DELTA_INIT));
    EXPECT_STREQ("0.",ptxt->getValue(MEMM_DELTA_MIN));
    EXPECT_STREQ("10000.",ptxt->getValue(MEMM_DELTA_MAX));
    EXPECT_STREQ("1.",ptxt->getValue(MEMM_SHAPE_B_INIT));
    EXPECT_STREQ("0.1",ptxt->getValue(MEMM_SHAPE_B_MIN));
    EXPECT_STREQ("10.",ptxt->getValue(MEMM_SHAPE_B_MAX));
    EXPECT_STREQ("0.5",ptxt->getValue(MEMM_MIG_RATE_M_INIT));
    EXPECT_STREQ("0.1",ptxt->getValue(MEMM_MIG_RATE_M_MIN));
    EXPECT_STREQ("1.",ptxt->getValue(MEMM_MIG_RATE_M_MAX));
    EXPECT_STREQ("0.05",ptxt->getValue(MEMM_SELF_RATE_S_INIT));
    EXPECT_STREQ("0.",ptxt->getValue(MEMM_SELF_RATE_S_MIN));
    EXPECT_STREQ("0.1",ptxt->getValue(MEMM_SELF_RATE_S_MAX));*/
    EXPECT_STREQ("5000",ptxt->getValue(MEMM_BURNIN));
    EXPECT_STREQ("50000",ptxt->getValue(MEMM_ITE));
    EXPECT_STREQ("20",ptxt->getValue(MEMM_THIN));
    EXPECT_STREQ("ParamFecTest.txt",ptxt->getValue(MEMM_GAMA_FILE_NAME));
    EXPECT_STREQ("IndivFecTest.txt",ptxt->getValue(MEMM_IND_FEC_FILE_NAME));
    EXPECT_STREQ("ParamDispTest.txt",ptxt->getValue(MEMM_DISP_FILE_NAME));

}

