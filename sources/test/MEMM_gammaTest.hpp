#ifndef __MEMM_GAMMA_TEST__
#define __MEMM_GAMMA_TEST__

#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include "../include/MEMM_gamma.h"

class MEMM_gammaTest : public ::testing::Test {

    protected:
    MEMM_gammaTest();
    
    virtual ~MEMM_gammaTest();

    virtual void SetUp();
    
    virtual void TearDown(); 

    MEMM_gamma * pLoiGamma;
    boost::uniform_real<> unif;
    boost::lagged_fibonacci19937 generator;
    boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> > accept;

};

#endif

