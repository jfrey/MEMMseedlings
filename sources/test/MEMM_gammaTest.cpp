#include <gtest/gtest.h>
#include "MEMM_gammaTest.hpp"
#include "../include/MEMM_gamma.h"

MEMM_gammaTest::MEMM_gammaTest():unif(0,1.0), generator(static_cast<long unsigned int>(12345)), accept(generator,unif){
};

MEMM_gammaTest::~MEMM_gammaTest(){
};

void MEMM_gammaTest::SetUp() {
    double gamma = 1.0;
    pLoiGamma = new MEMM_gamma(gamma,accept);
}
    
void MEMM_gammaTest::TearDown() {
    delete(pLoiGamma);
}

TEST_F(MEMM_gammaTest, gammaMinAndMax)
{
    int size = 10;
    std::vector<double> vec(size,1.0);
    double A = 1.0;
    EXPECT_EQ(-10,pLoiGamma->logLik(size,vec,A));
    A=1000;
    EXPECT_FLOAT_EQ(-69.150864,pLoiGamma->logLik(size,vec,A));

}

TEST_F(MEMM_gammaTest, gammaZeroInput)
{
    int size = 10;
    std::vector<double> vec(size,1.0);
    double A=0.0;
    EXPECT_TRUE(std::isnan((double)pLoiGamma->logLik(size,vec,A)));
}

