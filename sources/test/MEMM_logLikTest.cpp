#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include "MEMM_logLikTest.hpp"
#include "../include/MEMM_logLik.hpp"

MEMM_logLikTest::MEMM_logLikTest(){
  Configuration *p = new ConfigTXT("./ParametersTest.txt");
  aloglik = new MEMM_logLik();
  aloglik->init(input); 
};

MEMM_logLikTest::~MEMM_logLikTest(){
        delete(aloglik);
    };


TEST_F(MEMM_logLikTest, LogLikParameters)
{
    EXPECT_EQ(100.0,aloglik->getDValue(MEMM_LOGLIK_SCALE));
    EXPECT_EQ(0.0,aloglik->getDValue(MEMM_LOGLIK_MINSCALE));
    EXPECT_EQ(10000.0,aloglik->getDValue(MEMM_LOGLIK_MAXSCALE));
    EXPECT_EQ(1.0,aloglik->getDValue(MEMM_LOGLIK_SHAPE));
    EXPECT_EQ(0.1,aloglik->getDValue(MEMM_LOGLIK_MINSHAPE));
    EXPECT_EQ(10.0,aloglik->getDValue(MEMM_LOGLIK_MAXSHAPE));
    EXPECT_EQ(0.5,aloglik->getDValue(MEMM_LOGLIK_MIG));
    EXPECT_EQ(0.1,aloglik->getDValue(MEMM_LOGLIK_MINMIG));
    EXPECT_EQ(1.0,aloglik->getDValue(MEMM_LOGLIK_MAXMIG));
    EXPECT_EQ(0.05,aloglik->getDValue(MEMM_LOGLIK_SELF));
    EXPECT_EQ(0.0,aloglik->getDValue(MEMM_LOGLIK_MINSELF));
    EXPECT_EQ(0.1,aloglik->getDValue(MEMM_LOGLIK_MAXSELF));

    EXPECT_LT(0.0,aloglik->getDValue(MEMM_LOGLIK_A));
    EXPECT_GT(0,aloglik->getDValue(MEMM_LOGLIK_AB));

    EXPECT_EQ(2.0,aloglik->getDValue(MEMM_LOGLIK_GAMA));
    EXPECT_EQ(1.0,aloglik->getDValue(MEMM_LOGLIK_MINGAMA));
    EXPECT_EQ(1000.0,aloglik->getDValue(MEMM_LOGLIK_MAXGAMA));

    EXPECT_EQ(3,aloglik->getIValue(MEMM_LOGLIK_NPAR));
    EXPECT_EQ(12345,aloglik->getIValue(MEMM_LOGLIK_SEED));

}

