#ifndef __CONFIG_TXT_TEST__
#define __CONFIG_TXT_TEST__

#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include "../include/ConfigTXT.hpp"

class ConfigTXTTest : public ::testing::Test {

    protected:
    ConfigTXTTest();
    
    virtual ~ConfigTXTTest();

    virtual void SetUp() {
        // Code here will be called immediately after the constructor (right
            //     // before each test).
     }
    
    virtual void TearDown() {
    // Code here will be called immediately after each test (right
    // before the destructor).
    }

    ConfigTXT * ptxt;

};

#endif

