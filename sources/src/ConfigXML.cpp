/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/**
 * \file ConfigXML.cpp
 * \brief Configuration manager
 * \author Jean-Francois REY
 * \version 1.1
 * \date 11 May 2016
 */


#include "ConfigXML.hpp"

ConfigXML::ConfigXML()
{
    param_xml = NULL;
    createDefaultXMLConfig();
}

ConfigXML::ConfigXML(string filename)
{
    param_xml = NULL;
    isOK_ = loadXMLConfig(filename);
}

ConfigXML::ConfigXML(char * filename) : Configuration(filename)
{
    param_xml = NULL;
    string st(filename);
    if(isOK_) isOK_ = loadXMLConfig(st);
}

ConfigXML::~ConfigXML()
{
    if(param_xml != NULL) delete param_xml;
}

void ConfigXML::createDefaultXMLConfig()
{
    param_xml = new XMLInterface();
    param_xml->createNewDocument();

    param_xml->loadString(DEFAULT_XML_CONFIG_MEMM);

}

bool ConfigXML::loadXMLConfig(string filename)
{
    //if(filename == NULL || filename.empty() || filename.length == 0) return -1;

    if(param_xml != NULL) delete param_xml;
    param_xml = new XMLInterface();

    try{
        param_xml->loadFile(filename);
    }
    catch(Myexception &me)
    {
        cout << "Error loading xml configuration file : " << filename << endl;
        cout << me.what() << endl;
        return 0;
    };

    param_filename.clear();
    param_filename.assign(filename);
    
    return 1;
}

string ConfigXML::getValue(string name, string attribut)
{
    string result;
    if(param_xml != NULL) result.assign(param_xml->getXPathValue(name,attribut));

    return result;
}

char * ConfigXML::getValue(MEMM_parameters_t valueName)
{

  string st;
  char * temp = NULL;

  switch(valueName) {
    case MEMM_TYPE:
      st = getValue(MEMM_ROOT,"type");
      break;
    case MEMM_PARENTS_FILE_NAME:
      st = getValue(INPUT_PARENTS_FILE_NAME);
      break;

    case MEMM_PARENTS_FILE_NUMBER_OF_LOCUS:
      st = getValue(INPUT_PARENTS_FILE_NUMBERS_OF_LOCUS);
      break;

    case MEMM_PARENTS_FILE_CLASS_COV:
      st = getValue(INPUT_PARENTS_FILE_CLASS_COV);
      break;

    case MEMM_PARENTS_FILE_QT_COV:
      st = getValue(INPUT_PARENTS_FILE_QT_COV);
      break;

    case MEMM_PARENTS_FILE_WEIGHT_VAR:
      st = getValue(INPUT_PARENTS_FILE_WEIGHT_VAR);
      break;

    case MEMM_OFFSPRING_FILE_NAME:
      st = getValue(INPUT_OFFSPRING_FILE_NAME);
      break;

    case MEMM_OFFSPRING_FILE_NUMBERS_OF_LOCUS:
      st = getValue(INPUT_OFFSPRING_FILE_NUMBERS_OF_LOCUS);
      break;

    case MEMM_AF_FILE_MODE:
      st = getValue(INPUT_AF_FILE_MODE);
      break;

    case MEMM_AF_FILE_NAME:
      st = getValue(INPUT_AF_FILE_NAME);
      break;

    case MEMM_DIST_FILE_MODE:
      st = getValue(INPUT_DIST_FILE_MODE);
      break;

    case MEMM_DIST_FILE_NAME:
      st = getValue(INPUT_DIST_FILE_NAME);
      break;

    case MEMM_LOCUS_ERROR:
      st = getValue(INPUT_MEMM_LOCUS_ERROR);
      break;

    case MEMM_LOCUS_ERROR_FILE:
      st = getValue(INPUT_MEMM_LOCUS_ERROR_FILE);
      break; 

    case MEMM_SEED:
      st = getValue(PARAM_SEED);
      break;

    case MEMM_BURNIN:
      st = getValue(PARAM_BURNIN);
      break;

    case MEMM_ITE:
      st = getValue(PARAM_ITE);
      break;

    case MEMM_THIN:
      st = getValue(PARAM_THIN);
      break;

    case MEMM_GAMA_FILE_NAME:
      st = getValue(OUTPUT_GAMA_FILE_NAME,OUTPUT_GAMA_FILE_NAME_ATTRIBUT);
      break;

    case MEMM_IND_FEC_FILE_NAME:
      st = getValue(OUTPUT_IND_FEC_FILE_NAME,OUTPUT_IND_FEC_FILE_NAME_ATTRIBUT);
      break;

    case MEMM_DISP_FILE_NAME:
      st = getValue(OUTPUT_DISP_FILE_NAME,OUTPUT_DISP_FILE_NAME_ATTRIBUT);
      break;


    default:
      cerr<<"Error reading "<<valueName<<" value"<<endl;
      break;
  }

  temp = new char[st.length()+1];
  strcpy(temp,st.c_str());

  return temp;

}

bool ConfigXML::setValue(string name,string value, string attribut)
{
    bool res = 0;
    
    if(param_xml != NULL) res = param_xml->setXPathValue(name,value,attribut);

    return res;

}

bool ConfigXML::setValue(MEMM_parameters_t valueName, char * value)
{

  return true;
}

bool ConfigXML::printXMLConfig()
{
    if(param_xml == NULL) return -1;

    return param_xml->print();
}

bool ConfigXML::save()
{
    return saveFile(param_filename);
}

bool ConfigXML::saveFile(string filename)
{
    bool b;
    if(param_xml != NULL) b = param_xml->save(filename);

    if(b){ param_filename.clear(); param_filename.assign(filename);}

    return b;
}

bool ConfigXML::addElement(string  expr, string name, string text)
{
    if( param_xml != NULL) return param_xml->addXPathElement(expr,name,text);

    return -1;
}

bool ConfigXML::enableElement(string name, bool enable)
{
  xmlNodePtr pos;
  pos = param_xml->find(name);

  param_xml->addAttribut(pos,"enable",(enable)?(string("true")):(string("false")));
  return pos;
}

string ConfigXML::getFileName()
{
    return param_filename;
}

unordered_map <string, Parameter *> ConfigXML::loadParameters()
{
  xmlNodePtr pos;
  list<xmlNodePtr> list_params;
  string type;
  Parameter * param_temp;

  pos = param_xml->find("parameters",param_xml->getRootNode());
  list_params = param_xml->findAll("param",pos);

  unordered_map <string, Parameter *> result(list_params.size());

  for(list<xmlNodePtr>::iterator it=list_params.begin(); it != list_params.end(); ++it)
  {
    type = param_xml->getAttribut(*it,"type");
    if(type == "double" || type == "vector")
    {
      param_temp = loadParameterD(*it);
      result.insert(pair<string,Parameter *>(param_temp->NAME,param_temp));
    }
  }

  list_params.clear();

  return result;
}

vector<string> ConfigXML::getParametersName()
{
  xmlNodePtr pos;
  list<xmlNodePtr> list_params;
  string type;

  pos = param_xml->find("parameters",param_xml->getRootNode());
  list_params = param_xml->findAll("param",pos);

  vector<string> result;

  for(list<xmlNodePtr>::iterator it=list_params.begin(); it != list_params.end(); ++it)
  {
    type = param_xml->getAttribut(*it,"type");
    if(type == "double" || type == "vector")
    {
      result.push_back(param_xml->getAttribut(*it,"name"));
    }
  }

  list_params.clear();

  return result;
}

bool ConfigXML::deleteParameter(string name)
{
  xmlNodePtr pos;

  pos = param_xml->find("parameters",param_xml->getRootNode());
  pos = param_xml->findTagAttribut("param","name",name,pos);
  if(pos) param_xml->deleteNode(pos);

  return pos;

}

bool ConfigXML::setParameter(string name, double init, double min, double max, int size, string prior)
{
  xmlNodePtr params,param,pprior;

  params = param_xml->find("parameters",param_xml->getRootNode());
  param = param_xml->findTagAttribut("param","name",name,params);

  // if it does not exist
  if(!param)
  {
    param = param_xml->addTagNode(string("param"),params);
    param_xml->addAttribut(param,"name",name);
    param_xml->addAttribut(param,"type","double");
        
    param_xml->addTagNode(string("init"),param);
    param_xml->addTagNode(string("min"),param);
    param_xml->addTagNode(string("max"),param);
    pprior = param_xml->addTagNode(string("prior"),param);
    param_xml->addAttribut(pprior,"name","priorname");
    param_xml->addAttribut(pprior,"type","default");

  }
  
  if(size > 1){
    param_xml->addAttribut(param,"type","vector");
    param_xml->addAttribut(param,"size",to_string(size));
  }
  else
  {
    param_xml->addAttribut(param,"type","double");
    param_xml->addAttribut(param,"size","");
  }

  param_xml->addTextToNode(Tools::to_string_with_precision(init,4),param_xml->find("init",param));
  param_xml->addTextToNode(Tools::to_string_with_precision(min,4),param_xml->find("min",param));
  param_xml->addTextToNode(Tools::to_string_with_precision(max,4),param_xml->find("max",param));
  pprior = param_xml->find("prior",param);
  param_xml->addAttribut(pprior,"name",prior);


}

double ConfigXML::getParameterInitValue(string param_name)
{
  xmlNodePtr pos;

  pos = param_xml->find("parameters",param_xml->getRootNode());
  pos = param_xml->findTagAttribut("param","name",param_name,pos);
  pos = param_xml->find("init",pos);

  return stod(Tools::convertPointToComma(param_xml->getText(pos)));
}

double ConfigXML::getParameterMinValue(string param_name)
{
  xmlNodePtr pos;

  pos = param_xml->find("parameters",param_xml->getRootNode());
  pos = param_xml->findTagAttribut("param","name",param_name,pos);
  pos = param_xml->find("min",pos);
  
  return stod(Tools::convertPointToComma(param_xml->getText(pos)));
}

double ConfigXML::getParameterMaxValue(string param_name)
{
  xmlNodePtr pos;

  pos = param_xml->find("parameters",param_xml->getRootNode());
  pos = param_xml->findTagAttribut("param","name",param_name,pos);
  pos = param_xml->find("max",pos);

  return stod(Tools::convertPointToComma(param_xml->getText(pos)));
}

int ConfigXML::getParameterSizeValue(string param_name)
{
  xmlNodePtr pos;
  string res;

  pos = param_xml->find("parameters",param_xml->getRootNode());
  pos = param_xml->findTagAttribut("param","name",param_name,pos);
  res = param_xml->getAttribut(pos,"size");

  return atoi(res.c_str());
}

string ConfigXML::getParameterPriorValue(string param_name)
{
  xmlNodePtr pos;
  string res;

  pos = param_xml->find("parameters",param_xml->getRootNode());
  pos = param_xml->findTagAttribut("param","name",param_name,pos);
  pos = param_xml->find("prior",pos);
  res = param_xml->getAttribut(pos,"name");

  return res;
}

vector<string> ConfigXML::getDistributionsName()
{
  xmlNodePtr pos;
  list<xmlNodePtr> list_params;
  string type;

  pos = param_xml->find("parameters",param_xml->getRootNode());
  list_params = param_xml->findAll("param",pos);

  vector<string> result;

  for(list<xmlNodePtr>::iterator it=list_params.begin(); it != list_params.end(); ++it)
  {
    type = param_xml->getAttribut(*it,"type");
    if(type == "distribution")
    {
      result.push_back(param_xml->getAttribut(*it,"name"));
    }
  }

  list_params.clear();

  return result;

}

string ConfigXML::getDistributionType(string namelaw)
{
  string res ="";
  string type;
  string name;
  xmlNodePtr pos;
  list<xmlNodePtr> list_params;


  pos = param_xml->find("parameters",param_xml->getRootNode());
  list_params = param_xml->findAll("param",pos);

  for(list<xmlNodePtr>::iterator it=list_params.begin(); it != list_params.end(); ++it)
  {
    type = param_xml->getAttribut(*it,"type");
    name = param_xml->getAttribut(*it,"name");
    if(type == "distribution" && name == namelaw)
    {
      res = param_xml->getAttribut((*it)->children,"name");
    }
  }

  list_params.clear();
  return res;
}

map<string,string> ConfigXML::getDistributionParameters(string lawname)
{

  map<string,string> res;
  string type;
  string name;
  xmlNodePtr pos;
  list<xmlNodePtr> list_params;
  list<xmlNodePtr> law_params;

  pos = param_xml->find("parameters",param_xml->getRootNode());
  list_params = param_xml->findAll("param",pos);

  for(list<xmlNodePtr>::iterator it=list_params.begin(); it != list_params.end(); ++it)
  {
    type = param_xml->getAttribut(*it,"type");
    name = param_xml->getAttribut(*it,"name");
    if(type == "distribution" && name == lawname)
    {
     law_params = param_xml->findAll("lp",*it);
     break;
    }
  }
  list_params.clear();

  for(list<xmlNodePtr>::iterator it=law_params.begin(); it != law_params.end(); ++it)
  {
    res.insert(pair<string,string>(param_xml->getAttribut(*it,"name"),param_xml->getText(*it)));
  }

  return res;
}

bool ConfigXML::setDistribution(string name,string law, map<string,string> name_ref)
{

  xmlNodePtr params,param,pos;

  params = param_xml->find("parameters",param_xml->getRootNode());
  param = param_xml->findTagAttribut("param","name",name,params);

  // if it does not exist
  if(!param)
  {
    param = param_xml->addTagNode(string("param"),params);
    param_xml->addAttribut(param,"name",name);
    param_xml->addAttribut(param,"type","distribution");

  }

  pos = param_xml->find("law",param);
  if(pos) param_xml->deleteNode(pos);

  param = param_xml->addTagNode(string("law"),param);
  param_xml->addAttribut(param, "name",law);
  param = param_xml->addTagNode(string("law_parameters"),param);

  for(map<string,string>::iterator it = name_ref.begin(); it != name_ref.end(); ++it)
  {
    //cerr<<it->first<<" "<<it->second<<endl;
    pos = param_xml->addTagNode(string("lp"),param);
    param_xml->addAttribut(pos,"type","param");
    param_xml->addAttribut(pos,"name",it->first);
    param_xml->addTextToNode(it->second,pos);
  }


  return true;
}

bool ConfigXML::deleteDistribution(string name)
{
  xmlNodePtr pos;

  pos = param_xml->find("parameters",param_xml->getRootNode());
  pos = param_xml->findTagAttribut("param","name",name,pos);
  if(pos && param_xml->getAttribut(pos,"type")=="distribution") param_xml->deleteNode(pos);

  return pos;
}

/***************************** PRIVATE ****************************************/
Parameter * ConfigXML::loadParameterD(xmlNodePtr paramNode)
{
  string name;
  double init,min,max;
  int size;
  string prior;

  name = param_xml->getAttribut(paramNode,"name");
  size = atoi(param_xml->getAttribut(paramNode,"size").c_str());
  if(size == 0) size = 1;
  init = atof(param_xml->getText(paramNode,"init").c_str());
  min = atof(param_xml->getText(paramNode,"min").c_str());
  max = atof(param_xml->getText(paramNode,"max").c_str());
  prior = param_xml->getAttribut(param_xml->find(prior,paramNode),"name");

  return(new Parameter(name,init,min,max,prior,size));
}



