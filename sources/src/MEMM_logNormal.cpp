/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "MEMM_logNormal.h"
#include <iostream>
#include <string>

MEMM_logNormal::MEMM_logNormal(boost::variate_generator<boost::lagged_fibonacci19937&, boost::normal_distribution<> >& gauss):
  _sigma(1),_mu(1),_gauss(gauss)
{
    _IDLoi=MEMM_IDLOI_LN;
    std::cout<<"Use LogNormal"<<std::endl;
}




MEMM_logNormal::~MEMM_logNormal()
{
    //dtor
}

void MEMM_logNormal::tirage(double& v){
  v= exp(_sigma*_gauss()+_mu);
}
double MEMM_logNormal::logLik(int npar, const std::vector<double> & fec, double& A) {

  // Parametrage de la loi normale en A.
  // A=dobs/de = SIGMA2 + 1 = exp(sigma2);
  // La gaussienne suit une loi d'Esperance = -sigma2/2; de variance sigma2

  double sigma2=log(A);
  double a1=0.5*log(sigma2);
  double liktemp=0;
  for (int p=0; p<npar; p++) {
    liktemp += - a1 - (log(fec[p]) + sigma2/2 )*(log(fec[p]) + sigma2/2 )/2/sigma2;
  }
  return liktemp;
}
void MEMM_logNormal::setDParam(unsigned int indiceParam, double & value){
  switch (indiceParam)
    {
    case MEMM_LOI_GAMA:
      _sigma=pow(log(value),0.5);
      _mu=-log(value)/2;
      break;
    default:
      printf("Error: MEMM_logNormal indiceParam out of range \n");
    }
}
