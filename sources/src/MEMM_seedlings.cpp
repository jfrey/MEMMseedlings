/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "MEMM_seedlings.hpp"

MEMM_seedlings::MEMM_seedlings()
{
  cout<< "MEMM : Seedlings mode"<<endl;
}

MEMM_seedlings::~MEMM_seedlings()
{
  if(pLois) delete pLois;
}

void MEMM_seedlings::mcmc(int nbIteration, int thinStep)
{

  double lastFecLogLiks, nextFecLogLiks;
  double lastFecLogLikp, nextFecLogLikp;
  double previousValue, nextValue;
  double previousValue2;
  long double previousLogLik, nextLogLik;

  int xpourCent = nbIteration/20;
  if(!xpourCent) xpourCent=1;

  lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp,GamA);
  lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs,GamAs);
  previousLogLik = logLik();

  //output start value
  if(thinStep){
    *ParamFec << "0 " << lastFecLogLiks << " " << GamAs << " " << lastFecLogLikp << " " << GamA << endl;
    ParamFec->flush();
    *IndivFec << "0 " ;
    for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecs[p] << " ";}
    for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecp[p] << " ";}
    *IndivFec << endl;
    IndivFec->flush();
    *ParamDisp << "0 " << previousLogLik
      << " " << Scales
      << " " << Shapes
      << " " << Migs
      << " " << Scale
      << " " << Shape
      << " " << Mig
      << " " << Self
      <<endl;
    ParamDisp->flush();

  }

  if(nbIteration) cout << "it=1..."<< std::endl;
  for(int ite=1; ite<=nbIteration; ite++)
  {
    if (ite % xpourCent == 0) {cout << "it="<<ite << "..."<< std::endl;}

    //GamAs- fecundities distribution
    nextValue = pow(GamAs, exp(0.2*gauss()));
    if( nextValue>mGamAs && nextValue<MGamAs )
    {
      lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs,GamAs);
      nextFecLogLiks = pLois->logLik(NPar, Vec_Fecs, nextValue);
      if( accept() < exp(nextFecLogLiks-lastFecLogLiks) )
      {
        GamAs = nextValue;
        lastFecLogLiks = nextFecLogLiks;
        pLois->setDParam(MEMM_LOI_GAMA,GamAs);
      }
    }
    if(thinStep)*ParamFec << ite << " " << lastFecLogLiks << " " << GamAs << " ";

    //GamA (GamAp)- fecundities distribution
    nextValue = pow(GamA, exp(0.2*gauss()));
    if( nextValue>mGamA && nextValue<MGamA )
    {
      lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp,GamA);
      nextFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, nextValue);
      if( accept() < exp(nextFecLogLikp-lastFecLogLikp) )
      {
        GamA = nextValue;
        lastFecLogLikp = nextFecLogLikp;
        pLoi->setDParam(MEMM_LOI_GAMA,GamA);
      }
    }
    if(thinStep)*ParamFec << lastFecLogLikp << " " << GamA << endl;

    for(int p=0 ; p<NPar ; p++)
    {
      previousValue = Vec_Fecs[p];
      previousValue2 = Vec_Fecp[p];
      pLois->tirage(Vec_Fecs[p]);
      pLoi->tirage(Vec_Fecp[p]);
      nextLogLik = logLik();
      if(accept()<exp(nextLogLik-previousLogLik)) previousLogLik = nextLogLik;
      else
      {
        Vec_Fecs[p] = previousValue;
        Vec_Fecp[p] = previousValue2;
      }
    }

    if(thinStep && (ite%thinStep == 0))
    {
      *IndivFec << ite << " " ;
      for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecs[p] << " ";}
      for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecp[p] << " ";}
      *IndivFec << endl;
      IndivFec->flush();
    }

    // change Scales
    previousValue = Scales;
    Scales = previousValue*exp(0.2*gauss());
    if( Scales>mScales && Scales<MScales)
    {
      as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
      abs=-pow(as,-Shapes);
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else
      {
        Scales = previousValue;
        as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
        abs=-pow(as,-Shapes);
      }
    }
    else Scales = previousValue;

    //change Shapes
    previousValue = Shapes;
    Shapes = previousValue*exp(0.2*gauss());
    if( Shapes>mShapes && Shapes<MShapes)
    {
      as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
      abs=-pow(as,-Shapes);
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else{
        Shapes = previousValue;
        as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
        abs=-pow(as,-Shapes);
      }
    }
    else Shapes = previousValue;


    // change Mig
    previousValue = Migs;
    Migs = previousValue*exp(0.2*gauss());
    if( Migs>mMigs && Migs<MMigs)
    {
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else Migs = previousValue;
    }
    else Migs = previousValue;

    // change Scale
    previousValue = Scale;
    Scale = previousValue*exp(0.2*gauss());
    if( Scale>mScale && Scale<MScale)
    {
      ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
      abp=-pow(ap,-Shape);
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else
      {
        Scale = previousValue;
        ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        abp=-pow(ap,-Shape);
      }
    }
    else Scale = previousValue;

    // change Shape
    previousValue = Shape;
    Shape = previousValue*exp(0.2*gauss());
    if( Shape>mShape && Shape<MShape)
    {
      ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
      abp=-pow(ap,-Shape);
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else{
        Shape = previousValue;
        ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        abp=-pow(ap,-Shape);
      }
    }   
    else Shape = previousValue;

    // change Mig
    previousValue = Mig;
    Mig = previousValue*exp(0.2*gauss());
    if( Mig>mMig && Mig<MMig)
    {
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else Mig = previousValue;
    }
    else Mig = previousValue;

    // change Self
    previousValue = Self;
    Self = previousValue*exp(0.2*gauss());
    if( Self>mSelf && Self<MSelf)
    {
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else Self = previousValue;
    }
    else Self = previousValue;

    *ParamDisp << ite<<" " << previousLogLik
      << " " << Scales
      << " " << Shapes
      << " " << Migs
      << " " << Scale
      << " " << Shape
      << " " << Mig
      << " " << Self
      <<endl;
    ParamDisp->flush();

  }
}

void MEMM_seedlings::mcmc_dyn(int nbIteration, int thinStep)
{
  cerr<<"Dynamic seedlings not implemented yet"<<endl;
  exit(0);
}



/***************************** PROTECTED ********************************/

long double MEMM_seedlings::logLik()
{

  long double liktemp = 0;
  long double pip = 0;
  long double pip2 = 0;
  int pbm = 0;

  vector < vector<long double> > matp (NPar, vector <long double> (NPar, 0.) );
  vector < vector<long double> > mats (Ns, vector <long double> (NPar, 0.) );
  vector <long double> totp (NPar);
  vector <long double> tots (Ns);

  for (int m=0; m<NPar; m++)
  {
    totp[m]=0;
    for (int p=0; p<m; p++)
    {
      matp[m][p]=exp(abp*pow(DistMP[m][p],Shape))*Vec_Fecp[p];
      if (isnan(matp[m][p])){matp[m][p]=0; pbm=1;}
      totp[m]+=matp[m][p];
    }
    for (int p=m+1; p<NPar; p++)
    {
      matp[m][p]=exp(abp*pow(DistMP[m][p],Shape))*Vec_Fecp[p];
      if (isnan(matp[m][p])){matp[m][p]=0; pbm=1;}
      totp[m]+=matp[m][p];
    }
    matp[m][m]=Self*totp[m]/(1-Mig-Self);
  }

  for (int s=0; s<Ns; s++)
  {
    tots[s]=0;
    for (int m=0; m<NPar; m++)
    {
      mats[s][m]=exp(abs*pow(DistSM[s][m],Shapes))*Vec_Fecs[m];
      if (isnan(mats[s][m])){mats[s][m]=0; pbm=1;}
      tots[s]+=mats[s][m];
    }
  }

  if (pbm==1) { cerr << " Warning: Overflow. One component of the dispersal matrix was not defined. " << endl;}

  for (int s=0; s<Ns; s++)
  {
    if( ProbMig[s]>0 && tots[s]>0)
    {
      pip=0;pip2=0;
      for (int m=0; m<NbMeres[s]; m++)
      {
        pip += (ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot];
      }
      for (int m=0; m<NbCouples[s]; m++)
      {
        pip2 += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[ProbCouples[s][m].mere_pot][ProbCouples[s][m].pere_pot]/totp[ProbCouples[s][m].mere_pot];
      }
      liktemp += log(ProbMig[s]*Migs+(1-Migs)/tots[s]*(pip*Mig+(1-Mig-Self)*pip2));
    }
  }

return liktemp;

}


/*************** INIT ******************/
void MEMM_seedlings::loadSeeds(Configuration * p)
{
  int Nsl;
  string name;
  char * temp;

  if(p == NULL)return;

  cout<<endl<<"# Loading seeds... as parents"<<endl;
  allSeeds.clear();

  temp = p->getValue(MEMM_OFFSPRING_FILE_NAME);
  ifstream seeds(temp);
  if(temp) delete []temp;
  temp = p->getValue(MEMM_OFFSPRING_FILE_NUMBERS_OF_LOCUS);
  Nsl = atoi(temp);
  if(temp) delete []temp;

  Ns = 0;
  if(seeds.good())
  {
    while(seeds >> name)
    {
      cout<<name<<"...";
      allSeeds.push_back(new parent(name,seeds,0,0,Nsl));
      Ns++;
    }
    cout<<endl<<Ns<<" seeds loaded."<<endl;
    seeds.close();
  }
  else
  {
  cerr<<"#ERROR ! Can not read seeds file !"<<endl<<"Continue..."<<endl;
  }
}

void MEMM_seedlings::calculDistances(Configuration *p)
{
  MEMM::calculDistances(p);

  DistSM.resize(Ns,vector<double>(NPar,0.));
  
  cout<< "Calcul distance seeds - mothers"<<endl;
  for(int s=0; s<Ns; s++)
    for(int m=0; m<NPar; m++)
      DistSM[s][m]=dynamic_cast<parent*>(allSeeds[s])->dist(*allParents[m]);


}

void MEMM_seedlings::calculTransitionMatrix(Configuration *p)
{

  NbMeres.clear();
  NbMeres.resize(Ns);
  NbCouples.clear();
  NbCouples.resize(Ns);
  ProbMig.clear();
  ProbMig.resize(Ns);
  ProbMeres.resize(Ns);
  ProbCouples.resize(Ns);

  individu nul("nul", genotype(Nl));
  double probtemp;
  PerePot perepottemp;
  CouplePot couplepottemp;

    cout << endl << "Computing the mendelian likelihoods..."  << endl;
  for (int s=0; s<Ns; s++)
  {
      cout << s << "...";
      cout.flush();
    NbMeres[s]=0;

    ProbMig[s]=dynamic_cast<parent*>(allSeeds[s])->mendel(nul,nul,Nl,Na,SizeAll,AllFreq);
    if (ProbMig[s]==0) {cout << "Seedling # " << s << " (" << dynamic_cast<parent*>(allSeeds[s])->getName() << ") carries an unknown allele " << endl;}

    for (int m=0; m<NPar; m++)
    {
        if(!useMatError)
            probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[m]),nul,Nl,Na,SizeAll,AllFreq);
        else
            probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[m]),nul,Nl,Na,SizeAll,AllFreq,MatError);

      if (probtemp>0)
      {
        NbMeres[s]++;
        perepottemp.pere_pot=m;
        perepottemp.pere_prob=probtemp;
        ProbMeres[s].push_back(perepottemp);
      }
    }

    for (int m=0; m<NbMeres[s]; m++)
    {
      for (int p=0; p<NbMeres[s]; p++)
      {
        
          if(!useMatError)
              probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),(*allParents[ProbMeres[s][p].pere_pot]),Nl,Na,SizeAll,AllFreq);
          else
              probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),(*allParents[ProbMeres[s][p].pere_pot]),Nl,Na,SizeAll,AllFreq,MatError);
          
        if (probtemp>0)
        {
          NbCouples[s]++;
          couplepottemp.mere_pot=ProbMeres[s][m].pere_pot;
          couplepottemp.pere_pot=ProbMeres[s][p].pere_pot;
          couplepottemp.couple_prob=probtemp;
          ProbCouples[s].push_back(couplepottemp);
        }
      }
    }
  }
  cout << endl << "Computation of the mendelian likelihoods : OK. " <<endl << endl;

}

void MEMM_seedlings::loadParameters(Configuration *p)
{
  MEMM::loadParameters(p);

  Parameter * param_temp;

  cout<< "Load more Parameters..."<<endl;

  // gama
  param_temp = allParameters["gamas"];
  if(param_temp)
  {
    GamAs = param_temp->INIT;
    mGamAs = param_temp->MIN;
    MGamAs = param_temp->MAX;
    cout << "GamAs : "<< GamAs << " [" <<mGamAs <<"|"<< MGamAs << "]"<<endl;
  }

  // Pi0s
  param_temp = allParameters["Pi0s"];
  if(param_temp)
  {
    Pi0s = param_temp->INIT;
    mPi0s = param_temp->MIN;
    MPi0s = param_temp->MAX;
    cout << "Pi0s : "<< Pi0s << " [" <<mPi0s <<"|"<< MPi0s << "]"<<endl;
  }

  // Scales
  param_temp = allParameters["scales"];
  if(param_temp)
  {
    Scales = param_temp->INIT;
    mScales = param_temp->MIN;
    MScales = param_temp->MAX;
    cout << "Scales : "<< Scales << " [" <<mScales <<"|"<< MScales << "]"<<endl;
  }
   
  // Shapes
  param_temp = allParameters["shapes"];
  if(param_temp)
  {
    Shapes = param_temp->INIT;
    mShapes = param_temp->MIN;
    MShapes = param_temp->MAX;
    cout << "Shapes : "<< Shapes << " [" <<mShapes <<"|"<< MShapes << "]"<<endl;
  }

  // Migs
  param_temp = allParameters["migs"];
  if(param_temp)
  {
    Migs = param_temp->INIT;
    mMigs = param_temp->MIN;
    MMigs = param_temp->MAX;
    cout << "Migs : "<< Migs << " [" <<mMigs <<"|"<< MMigs << "]"<<endl;
  }

  as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
  cout <<"Deltas : "<<as<<endl;
  abs=-pow(as,-Shapes);
  ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
  cout <<"Deltap : "<<ap<<endl;
  abp=-pow(ap,-Shape);

  Vec_Fecs.resize(NPar, 1.0 );
  Vec_Fecp.resize(NPar, 1.0 );

  pLois = loadDistribution(p, "individual_fecundities_seeds");

  cout<<"end loading Parameters"<<endl;

}

void MEMM_seedlings::loadOptions(Configuration *p)
{
  MEMM::loadOptions(p);
}


