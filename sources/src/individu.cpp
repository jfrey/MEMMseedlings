/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "individu.h"

/*!
 * \file individu.h
 * \brief Individu class implementation
 * \author Jean-François
 * \version 1.5
 * \date 21 April 2015
 */

individu::individu(const std::string & name, const genotype & geno) :
    _nom(name), _geno(geno)
    {};

individu::individu(const std::string & name) :
    _nom(name)
    {};

void individu::afficher() const {
    std::cout << _nom << std::endl;
    _geno.afficher();

}
/*
double individu::mendel (const individu & mere , const individu & pere, int nl, std::vector<int> na,  std::vector < std::map<int,int> > sizeall, const std::vector < std::vector<double> > & freqall) const
{
  double temp=1;
  for (int l=0; l<nl; l++)
    temp*=((this->_geno).getLocus(l))->mendel1(*mere.getGeno(l),*pere.getGeno(l),na[l],sizeall[l],freqall[l]);

  return(temp);
}*/

double individu::mendel(const individu & mere , const individu & pere, int nl, std::vector<int> na,  std::vector < std::map<int,int> > & sizeall, const std::vector < std::vector<double> > & freqall,const std::vector < std::vector < std::vector <double> > > & materror ) const 
{
  double temp=1;
  /*if(nl == this->_geno.getNbLocus())
  for (int l=0; l<nl; l++)
    temp*=((this->_geno).getLocus(l)).mendel1(mere.getGeno(l),pere.getGeno(l),na[l],sizeall[l],freqall[l],materror[l]);
  else cerr<<"ERROR : locus number differs "<<nl<<" vs "<<this->_geno.getNbLocus()<<endl;
  */

  temp = this->_geno.mendel(mere.getGeno(),pere.getGeno(),nl,na,sizeall,freqall,materror);
                                                    
  return(temp);
}

