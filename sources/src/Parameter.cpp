/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/*!
 * \file Parameter.cpp
 * \brief Parameter class
 * \author Jean-François Rey
 * \version 1.0
 * \date 20 April 2016
 */

#include "Parameter.hpp"

Parameter::Parameter(string name, double init, double min, double max, string prior, int size):name_(name), nb_of_value_(size){
  
  initial_value_ = new double[nb_of_value_];
  min_value_ = new double[nb_of_value_];
  max_value_ = new double[nb_of_value_];
  current_value_ = new double[nb_of_value_];

  for(int i=0; i<nb_of_value_; ++i)
  {
    initial_value_[i] = init;
    min_value_[i] = min;
    max_value_[i] = max;
    current_value_[i] = init;
  }

  accept_ratio_ = getAcceptRatio(prior);
} 

Parameter::~Parameter()
{
  if(initial_value_) delete[] initial_value_;
  initial_value_ = NULL;
  if(min_value_) delete[] min_value_;
  min_value_ = NULL;
  if(max_value_) delete[] max_value_;
  max_value_ = NULL;
  if(current_value_) delete[] current_value_;
  current_value_ = NULL;

};

accept_ratio Parameter::getAcceptRatio(string prior)
{
  if(prior == "powerminusone") return & powerMinusOneRatio;
  if(prior == "rectangularstep") return & rectangularStepRatio;

  return( & powerMinusOneRatio);
}

ostream& operator<< (ostream &out, const Parameter &param)
{
  out << endl << "# Parameter : " << param.name_ << endl;
  out << " Prior : " << (void *) param.accept_ratio_ << endl;
  if(param.nb_of_value_ > 1) out << " Vector size : " << param.nb_of_value_ << endl;
  for(int i=0; i<param.nb_of_value_; ++i)
    out << "\t" << param.current_value_[i] << ", " << param.initial_value_[i] << " [" << param.min_value_[i] << "|" << param.max_value_[i] << "]" << endl;

  return out;
}

double * Parameter::getAllInitial(){ return initial_value_; }
double * Parameter::getAllMin(){ return min_value_; }
double * Parameter::getAllMax(){ return max_value_; };
double * Parameter::getAllCurrent(){ return current_value_; }

string Parameter::getName(){ return name_; }

double Parameter::getInitial(int ind)
{
  if(ind < nb_of_value_) return initial_value_[ind];
  return numeric_limits<double>::infinity();

}
double Parameter::getMin(int ind)
{
  if(ind < nb_of_value_) return min_value_[ind];
  return numeric_limits<double>::infinity();

}

double Parameter::getMax(int ind)
{
  if(ind < nb_of_value_) return max_value_[ind];
  return numeric_limits<double>::infinity();

}

double Parameter::getCurrent(int ind)
{
  if(ind < nb_of_value_) return current_value_[ind];
  return numeric_limits<double>::infinity();

}

void Parameter::setInitial(double value, int ind)
{
  if(ind < nb_of_value_) initial_value_[ind] = value;
}
void Parameter::setMin(double value, int ind)
{
  if(ind < nb_of_value_) min_value_[ind] = value;
}

void Parameter::setMax(double value, int ind)
{
  if(ind < nb_of_value_) max_value_[ind] = value;
}

void Parameter::setCurrent(double value, int ind)
{
  if(ind < nb_of_value_) current_value_[ind] = value;
}
