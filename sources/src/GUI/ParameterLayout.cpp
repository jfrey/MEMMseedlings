/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "ParameterLayout.hpp"

ParameterLayout::ParameterLayout(ConfigXML * p, string param_name) : QHBoxLayout()
{
  int ind;

  p_ = p;
  ID_ = string("Parameter_name");
  name_ = new QLineEdit("Parameter_name");
  name_->setValidator(new QRegExpValidator(reg_valid_));
  name_->setMinimumSize(110,0);
  name_->setMaxLength(20);
  init_ = new QDoubleSpinBox();
  init_->setDecimals(4);
  init_->setRange(0,400000000);
  min_ = new QDoubleSpinBox();
  min_->setDecimals(4);
  init_->setRange(0,400000000);
  max_ = new QDoubleSpinBox();
  max_->setDecimals(4);
  max_->setRange(0,400000000);
  size_ = new QSpinBox();
  size_->setValue(1);
  size_->setRange(1,400000000);
  prior_ = new QComboBox();
  vector<string> prior_name = p_->getPriorsName();
  for(vector<string>::iterator it = prior_name.begin(); it != prior_name.end(); ++it)
  {
    prior_->addItem(QString::fromStdString(*it));
  }
  rm_button_ = new QPushButton("X");
  rm_button_->setMinimumSize(25,0);
  rm_button_->setToolTip("Remove Parameter");
  rm_button_->setStyleSheet("QPushButton {color: red;}");
  connect(rm_button_,&QPushButton::clicked,this,&ParameterLayout::delParameterLayout);

  init_label_ = new QLabel("Init");
  size_label_ = new QLabel("Size");
  min_label_ = new QLabel("Min");
  max_label_ = new QLabel("Max");
  prior_label_ = new QLabel("Prior");

  if(param_name == "") 
  {
    init_->setDisabled(true);
    min_->setDisabled(true);
    max_->setDisabled(true);
    size_->setDisabled(true);
    prior_->setDisabled(true);
  }
  else
  {
    ID_ = param_name;
    name_->setText(QString(param_name.c_str()));
    init_->setValue(p_->getParameterInitValue(param_name));
    min_->setValue(p_->getParameterMinValue(param_name));
    max_->setValue(p_->getParameterMaxValue(param_name));
    size_->setValue(p_->getParameterSizeValue(param_name));
    if( (ind = prior_->findText(QString(p_->getParameterPriorValue(param_name).c_str()))) >= 0) prior_->setCurrentIndex(ind);
  }

  addWidget(name_);
  addWidget(size_label_);
  addWidget(size_);
  addWidget(init_label_);
  addWidget(init_);
  addWidget(min_label_);
  addWidget(min_);
  addWidget(max_label_);
  addWidget(max_);
  addWidget(prior_label_);
  addWidget(prior_);
  addWidget(rm_button_);


  connect(name_, &QLineEdit::editingFinished,this, &ParameterLayout::valueChanged);
  connect(name_, &QLineEdit::textChanged,this, &ParameterLayout::valueChanged);
  connect(size_,static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),this, &ParameterLayout::valueChanged);
  connect(init_,static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),this, &ParameterLayout::valueChanged);
  connect(min_, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),this, &ParameterLayout::valueChanged);
  connect(max_, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),this, &ParameterLayout::valueChanged);
  connect(prior_,static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this, &ParameterLayout::valueChanged);
}

ParameterLayout::~ParameterLayout()
{
  this->disconnect();
  delete init_;
  delete init_label_;
  delete min_;
  delete min_label_;
  delete max_;
  delete max_label_;
  delete size_;
  delete size_label_;
  delete name_;
  delete prior_;
  delete prior_label_;
  delete rm_button_;
  if(p_) p_->deleteParameter(ID_);
}

void ParameterLayout::valueChanged()
{
  string name = name_->displayText().toStdString();
  if(name == "Parameter_name" || name == "")
  {
    init_->setDisabled(true);
    min_->setDisabled(true);
    max_->setDisabled(true);
    size_->setDisabled(true);
    prior_->setDisabled(true);
  }
  else
  { 
    init_->setDisabled(false);
    min_->setDisabled(false);
    max_->setDisabled(false);
    size_->setDisabled(false);
    prior_->setDisabled(false);

    if(name != ID_)
    {
      p_->deleteParameter(ID_);  
    }
    p_->setParameter(name_->displayText().toStdString(),
        init_->value(),
        min_->value(),
        max_->value(),
        size_->value(),
        prior_->currentText().toStdString());
    ID_ = name_->displayText().toStdString();
    emit valueAsChanged(this);
  }
}

void ParameterLayout::delParameterLayout()
{
  QString filename;
  QMessageBox msgBox;
  msgBox.setText("Whould you delete this item ?");
  msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
  msgBox.setDefaultButton(QMessageBox::Cancel);
  int ret = msgBox.exec();

  //check button signal
  switch (ret) {
    case QMessageBox::Ok:
      p_->deleteParameter(ID_);
      emit removeMe(this);
      break;
    case QMessageBox::Cancel:
      break;
    default:
      break;
  }
}

