/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/*!
 * \file OutputParamWidget.cpp
 * \brief QWidget containing output arguments
 * \author Jean-Francois REY
 * \version 1.0
 * \date 04 Dec 2013
 *
 * The OutputParamWidget class is a widget contianing argument editor for the output
 */

#include "OutputParamWidget.hpp"

OutputParamWidget::OutputParamWidget() : QWidget()
{
    // Main Layout
    vLayout = new QFormLayout();

    // Gama
    // Line edit and button
    QHBoxLayout * hLayout1;
    QPushButton * cFile1;
    hLayout1 = new QHBoxLayout();
    oLEdit1 = new QLineEdit();
    hLayout1->addWidget(oLEdit1);
    cFile1 = new QPushButton("File",this);
    hLayout1->addWidget(cFile1);
    vLayout->addRow(new QLabel("GamA:"),hLayout1);
    QObject::connect(cFile1,SIGNAL(clicked()),this,SLOT(ChooseGamaFile()));
    
    // Individual fecundities
    // Line edit and button
    QHBoxLayout * hLayout2;
    QPushButton * cFile2;
    hLayout2 = new QHBoxLayout();
    oLEdit2 = new QLineEdit();
    hLayout2->addWidget(oLEdit2);
    cFile2 = new QPushButton("File",this);
    hLayout2->addWidget(cFile2);
    vLayout->addRow(new QLabel("Individual fecundities:"),hLayout2);
    QObject::connect(cFile2,SIGNAL(clicked()),this,SLOT(ChooseIndFecFile()));
 
    // Dispersal parameters
    // Line edit and button
    QHBoxLayout * hLayout3;
    QPushButton * cFile3;   
    hLayout3 = new QHBoxLayout();
    oLEdit3 = new QLineEdit();
    hLayout3->addWidget(oLEdit3);
    cFile3 = new QPushButton("File",this);
    hLayout3->addWidget(cFile3);
    vLayout->addRow(new QLabel("Dispersal parameters:"),hLayout3);
    QObject::connect(cFile3,SIGNAL(clicked()),this,SLOT(ChooseDispParamFile()));
  
    setLayout(vLayout);

}

OutputParamWidget::OutputParamWidget(ConfigXML *p) : OutputParamWidget()
{
    param = p;
}

OutputParamWidget::~OutputParamWidget(){}

void OutputParamWidget::setParam(ConfigXML * p)
{
    param = p;
}

void OutputParamWidget::initValue()
{
    // Set edit line with value from param
    oLEdit1->setText(QString((param->getValue(OUTPUT_GAMA_FILE_NAME,OUTPUT_GAMA_FILE_NAME_ATTRIBUT)).c_str()));
    oLEdit2->setText(QString((param->getValue(OUTPUT_IND_FEC_FILE_NAME,OUTPUT_IND_FEC_FILE_NAME_ATTRIBUT)).c_str()));
    oLEdit3->setText(QString((param->getValue(OUTPUT_DISP_FILE_NAME,OUTPUT_DISP_FILE_NAME_ATTRIBUT)).c_str()));
}

void OutputParamWidget::ChooseGamaFile()
{
    QString filename = QFileDialog::getSaveFileName(this,tr("Save File"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        oLEdit1->setText(filename);
        param->setValue(OUTPUT_GAMA_FILE_NAME,filename.toStdString(),OUTPUT_GAMA_FILE_NAME_ATTRIBUT);
    }

}
void OutputParamWidget::ChooseIndFecFile()
{
    QString filename = QFileDialog::getOpenFileName(this,tr("Save File"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        oLEdit2->setText(filename);
        param->setValue(OUTPUT_IND_FEC_FILE_NAME,filename.toStdString(),OUTPUT_IND_FEC_FILE_NAME_ATTRIBUT);
    }

}
void OutputParamWidget::ChooseDispParamFile()
{
    QString filename = QFileDialog::getSaveFileName(this,tr("Save File"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        oLEdit3->setText(filename);
        param->setValue(OUTPUT_DISP_FILE_NAME,filename.toStdString(),OUTPUT_DISP_FILE_NAME_ATTRIBUT);
    }
}

