/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "MainWindow.hpp"

MainWindow::MainWindow(ConfigXML * p) : QMainWindow()
{

    param = p;
    //setFixedSize(300,150);

    setWindowTitle("MEMM configuration");
    setMinimumSize(950,700);
    createMenuAndToolBar();
    setMainWidget(p);
    initValue(param);
}


MainWindow::~MainWindow()
{
}

void MainWindow::createMenuAndToolBar()
{
    // create a menu File
    QMenu * menuFile = menuBar()->addMenu("&File");

    // create an open, save, save as and quit actions
    QAction * actionOpen = new QAction("&Open",this);
    menuFile->addAction(actionOpen);
    QAction * actionSave = new QAction("&Save",this);
    menuFile->addAction(actionSave);
    QAction * actionSaveAs = new QAction("&Save As...",this);
    menuFile->addAction(actionSaveAs);
    QAction * actionQuit = new QAction("&Quit",this);
//    actionQuit->setIcon(QIcon("quit.png"));
    menuFile->addAction(actionQuit);

    // create a toolbar using previous actions
    QToolBar * toolBarOpenFile = addToolBar("&Open");
    toolBarOpenFile->addAction(actionOpen);
    QToolBar * toolBarSaveFile = addToolBar("&Save");
    toolBarSaveFile->addAction(actionSave);
    QToolBar * toolBarSaveFileAs = addToolBar("&Save As");
    toolBarSaveFileAs->addAction(actionSaveAs);
    QToolBar * toolBarQuit = addToolBar("&Quit");
    toolBarQuit->addAction(actionQuit);

    // Connect signals and slots on action
    connect(actionOpen, SIGNAL(triggered()), this, SLOT(actionOpenFile()));
    connect(actionSave, SIGNAL(triggered()), this, SLOT(actionSaveFile()));
    connect(actionSaveAs, SIGNAL(triggered()), this, SLOT(actionSaveFileAs()));
    connect(actionQuit, SIGNAL(triggered()), this, SLOT(actionQuit()));


}

void MainWindow::setParam(ConfigXML * p)
{
    if(p != NULL) param = p;
    initValue(param);
}

QWidget * MainWindow::setMainWidget(ConfigXML * p)
{
    // create the main tab
    tabs = new QTabWidget();

    iPW = new InputParamWidget(p);
    pPW = new ParamParamWidget(p);
    oPW = new OutputParamWidget(p);
    opPW = new OptionsParamWidget(p);

    tabs->addTab(opPW,"Options");
    tabs->addTab(iPW,"Input");
    tabs->addTab(pPW,"Parametres");
    tabs->addTab(oPW,"Output");

    QMainWindow::setCentralWidget(tabs);

    return tabs;

}

void MainWindow::actionOpenFile()
{
    int res = 0;
    QString filename = QFileDialog::getOpenFileName(this,tr("Open text"), "~/", tr("XML files (*.xml)"));
    if(filename != NULL)
    {
        if(param != NULL)res =  param->loadXMLConfig(filename.toStdString());
        if(res)
        {
            initValue(param);
            setWindowTitle(QString("MEMM configuration | "+QFileInfo(filename).fileName()));
        }
    }
}

void MainWindow::actionSaveFile()
{
    if(param->getFileName() != "") param->save();
    else actionSaveFileAs();
}

void MainWindow::actionSaveFileAs()
{
    int res = 0;
    QString filename = QFileDialog::getSaveFileName(this,tr("Open text"), "~/", tr("XML files (*.xml)"));
    if(filename != NULL) 
    {
        res = param->saveFile(filename.toStdString());
        if(res)setWindowTitle(QString("MEMM parameters | "+QFileInfo(filename).fileName()));
    }
}

void MainWindow::actionQuit()
{
    QString filename;
    QMessageBox msgBox;
    msgBox.setText("The document has been modified.");
    msgBox.setInformativeText("Do you want to save your changes?");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int ret = msgBox.exec();

    // check button signal
    switch (ret) {
        case QMessageBox::Save:
            actionSaveFileAs();
            filename = QFileDialog::getSaveFileName(this,tr("Open text"), "~/", tr("XML files (*.xml)"));
            if(filename != NULL) 
            {
                param->saveFile(filename.toStdString());
                close();
            }
            break;
        case QMessageBox::Discard:
            close();
            break;
        case QMessageBox::Cancel:
            break;
        default:
            break;
    }
}

void MainWindow::initValue(ConfigXML * p)
{
    if(p == NULL) return;

    iPW->setParam(p);
    pPW->setParam(p);
    oPW->setParam(p);
    opPW->setParam(p);

    iPW->initValue();
    pPW->initValue();
    oPW->initValue();
    opPW->initValue();

}
