/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/**
 * \file ParamParamWidget.cpp
 * \brief Parameters arguments editor
 * \author Jean-Francois Rey
 * \version 1.1
 * \date 11 May 2016
 */

#include "ParamParamWidget.hpp"

ParamParamWidget::ParamParamWidget() : QWidget()
{

/*  +------------------------
    | ParamParamWidget
    | +----------------------
    | | vLayout
    | | +--------------------
    | | | pBoxGroup
    | | | +------------------
    | | | | pBoxGroupLayout
    | | | | +----------------
    | | | | | scroll_param
    | | | | | +--------------
    | | | | | | param_widget
    | | | | | | +------------
    | | | | | | | paramPack
    | | | | | | | +----------
    | | | | | | | | paramHBox
    | | +--------------------
    | | | dBoxGroup
    etc....
*/  
    // main layout
    vLayout = new QVBoxLayout();

    //* Parameters Box *//
    // Hor layout +  button widget
    QHBoxLayout * paramHBox = new QHBoxLayout();
    QPushButton * addParamButton = new QPushButton(tr("+ add param"));
    addParamButton->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Minimum);
    connect(addParamButton,SIGNAL(clicked()),this,SLOT(addParamLine()));
    paramHBox->addWidget(addParamButton,Qt::AlignCenter);
    paramHBox->setSizeConstraint(QLayout::SetMinAndMaxSize);

    // V layout parameters
    paramPack = new QVBoxLayout();
    paramPack->addLayout(paramHBox,0);
    paramPack->setAlignment(Qt::AlignTop);
    paramPack->setSizeConstraint(QLayout::SetMinAndMaxSize);
    
    // parameters widget
    QWidget * param_widget = new QWidget();
    param_widget->setLayout(paramPack);

    // layout parameters scroll parameters widget
    scroll_param = new QScrollArea();
    scroll_param->setWidget(param_widget);
    //scroll_param->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    // V Box group layout with scroll_param widget
    QVBoxLayout * pBoxGroupLayout = new QVBoxLayout();
    pBoxGroupLayout->addWidget(scroll_param);

    // box group widget with v box group layout
    pBoxGroup = new QGroupBox("Parameters");
    pBoxGroup->setLayout(pBoxGroupLayout);
    

    //* Distribution Box *//
    QHBoxLayout * distHBox = new QHBoxLayout();
    QPushButton * addDistButton = new QPushButton(tr("+ add dist"));
    addDistButton->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Minimum);
    connect(addDistButton,SIGNAL(clicked()),this,SLOT(addDistLine()));
    distHBox->addWidget(addDistButton,Qt::AlignCenter);
    distHBox->setSizeConstraint(QLayout::SetMinAndMaxSize);
  
    // V layout parameters
    distPack = new QVBoxLayout();
    distPack->addLayout(distHBox,0);
    distPack->setAlignment(Qt::AlignTop);
    distPack->setSizeConstraint(QLayout::SetMinAndMaxSize);
    
    // dist widget
    QWidget * dist_widget = new QWidget();
    dist_widget->setLayout(distPack);
    dist_widget->setMinimumWidth(600);

    // layout parameters scroll parameters widget
    scroll_dist = new QScrollArea();
    scroll_dist->setWidget(dist_widget);
    //scroll_dist->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    // V Box group layout with scroll_param widget
    QVBoxLayout * dBoxGroupLayout = new QVBoxLayout();
    dBoxGroupLayout->addWidget(scroll_dist);

    // box group widget with v box group layout
    dBoxGroup = new QGroupBox("Distributions");
    dBoxGroup->setLayout(dBoxGroupLayout);
    
    
    vLayout->addWidget(pBoxGroup);
    vLayout->addWidget(dBoxGroup);

    setLayout(vLayout);
}

ParamParamWidget::ParamParamWidget(ConfigXML *p) : ParamParamWidget()
{
    param = p;
}

ParamParamWidget::~ParamParamWidget(){}

void ParamParamWidget::setParam(ConfigXML * p)
{   
    param = p;
} 

void ParamParamWidget::initValue()
{
  vector<string> list_param = param->getParametersName();
  for(vector<string>::iterator it = list_param.begin(); list_param.end() != it ; ++it)
  {
    addNewParamLine(QString((*it).c_str()));
  }

  vector<string> list_dist = param->getDistributionsName();
  for(vector<string>::iterator it = list_dist.begin(); list_dist.end() != it ; ++it)
  {
    addNewDistLine(QString((*it).c_str()));
  }
}

void ParamParamWidget::addNewParamLine(QString param_name)
{
  ParameterLayout * pl = new ParameterLayout(param,param_name.toStdString());

  paramPack->insertLayout(paramPack->children().size()-1,pl,0);

  QObject::connect(pl,SIGNAL(removeMe(QLayout *)),this,SLOT(delParam(QLayout*)));
  QObject::connect(pl,SIGNAL(valueAsChanged(QLayout *)),this,SLOT(updatedParam(QLayout*)));
}

void ParamParamWidget::addParamLine()
{
  addNewParamLine(QString(""));
}

void ParamParamWidget::addNewDistLine(QString dist_name )
{
  DistributionLayout * dl = new DistributionLayout(param,dist_name.toStdString());

  distPack->insertLayout(distPack->children().size()-1,dl,0);

  QObject::connect(dl,SIGNAL(removeMe(QLayout *)),this,SLOT(delDist(QLayout*)));
  QObject::connect(this,SIGNAL(updateDist()),dl,SLOT(updateDistributionParameters()));

}

void ParamParamWidget::addDistLine()
{
  addNewDistLine(QString(""));
}

void ParamParamWidget::delParam(QLayout * qo)
{
  paramPack->removeItem(qo);
  delete qo;
  paramPack->update();
  updatedParam(qo);
}

void ParamParamWidget::delDist(QLayout * qo)
{
  distPack->removeItem(qo);
  delete qo;
  distPack->update();
}

void ParamParamWidget::updatedParam(QLayout * ql)
{
  emit updateDist();
}
