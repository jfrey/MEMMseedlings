/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include <cstdio>
#include <QApplication>
#include <QtPlugin>
#include <iostream>
#include <getopt.h>
#include "MainWindow.hpp"
#include "ConfigXML.hpp"

using namespace std;

#ifdef _WIN32
	Q_IMPORT_PLUGIN(QWindowsIntegrationPlugin);
#endif
#if defined(__linux__) && defined(_STATIC_)
  Q_IMPORT_PLUGIN(QXcbIntegrationPlugin)
#endif
//Q_IMPORT_PLUGIN(AccessibleFactory);

/**
 * Interface to acces and modify Config
 * there is no controler as in modele MVC (Model - Vue - Controler)
 * It's a simple GUI, that affect directely param (controler is integrated in Vue)
 */

void usage() {
  cout <<
  "------------------------------------------------------------------------------\n" <<
  " MEMM_GUI v 1.5 by BioSP\n" <<
  "------------------------------------------------------------------------------\n" <<
  "USAGE  : MEMM_GUI [options]\n" <<
  "OPTIONS :\n" <<
  "   -h              show this message\n" <<
  "   -v              show version info\n" <<
  "   -p <file_in>    a parameters XML file\n" <<
  "  eg. MEMM_GUI -p parameters.xml \n" <<
  "------------------------------------------------------------------------------\n";
}

int main(int argc,char ** argv)
{

  QApplication app(argc,argv);
  #if defined(__APPLE__) && defined(BUILD_DIST)
    //QDir dir(argv[0]);
    QDir dir(app.applicationDirPath());
    dir.cdUp();
    dir.cdUp();
    dir.cd("Plugins");
    //app.setLibraryPaths(QStringList(dir.absolutePath()));
    //QApplication::setLibraryPaths(QStringList(dir.absolutePath()));
    //QCoreApplication::setLibraryPaths(QStringList(dir.absolutePath()));
    //QCoreApplication::addLibraryPath(dir.absolutePath());
    app.addLibraryPath(dir.absolutePath());
    //cerr<<"plugins " <<QCoreApplication::libraryPaths().join(",").toUtf8().data()<<endl;
    //cerr<<"plugins path " <<dir.absolutePath().toStdString()<<endl;
  #endif


  int opt;
  extern char *optarg;
  ConfigXML * memm_param = NULL;
  char * input = NULL;

  static struct option long_options[] =
  {
    {"version", no_argument, 0, 'v'},
    {"help", no_argument, 0, 'h'},
    {"parameters", required_argument, 0, 'p'},
    {0, 0, 0, 0}
  };
  int option_index = 0;

  while ((opt = getopt_long (argc, argv, "hvp:",long_options, &option_index)) != -1)
  switch(opt)
  {
    case 'h':
      usage();
      exit(0);
    case 'v':
      cerr<<argv[0]<<" version : 1.0"<<endl;
      exit(0);
    case 'p':
      input = optarg;
    break;
    case '?':
      cerr<<"Unknown option "<<opt<<endl;
      return 1;
    default : break;
  }

  if(input) memm_param = new ConfigXML(input);
  else memm_param = new ConfigXML();

  MainWindow mainW(memm_param);
  mainW.show();

  return app.exec();

}

