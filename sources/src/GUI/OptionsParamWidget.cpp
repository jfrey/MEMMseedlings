/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/


#include "OptionsParamWidget.hpp"

OptionsParamWidget::OptionsParamWidget() : QWidget()
{
  vLayout = new QFormLayout();

  seedSpinBox = new QSpinBox();
  seedSpinBox->setMaximum(1000000);
  vLayout->addRow(new QLabel("Seed:"),seedSpinBox);
  QObject::connect(seedSpinBox,SIGNAL(valueChanged(int)),this,SLOT(ChangeSeed(int)));


  burnInSB = new QSpinBox();
  burnInSB->setMaximum(1000000);
  vLayout->addRow(new QLabel("Burnin:"),burnInSB);
  QObject::connect(burnInSB,SIGNAL(valueChanged(int)),this,SLOT(ChangeBurnIn(int)));

  iteSB = new QSpinBox();
  iteSB->setMaximum(1000000);
  vLayout->addRow(new QLabel("Iteration:"),iteSB);
  QObject::connect(iteSB,SIGNAL(valueChanged(int)),this,SLOT(ChangeIte(int)));

  thinSB = new QSpinBox();
  thinSB->setMaximum(50000);
  vLayout->addRow(new QLabel("Thin:"),thinSB);
  QObject::connect(thinSB,SIGNAL(valueChanged(int)),this,SLOT(ChangeThin(int)));

  setLayout(vLayout);

}

OptionsParamWidget::OptionsParamWidget(ConfigXML * p) : OptionsParamWidget()
{
  param = p;

}

OptionsParamWidget::~OptionsParamWidget(){}

void OptionsParamWidget::ChangeSeed(int v)
{
  param->setValue(PARAM_SEED,to_string(v));
}

void OptionsParamWidget::ChangeBurnIn(int v)
{
  param->setValue(PARAM_BURNIN,to_string(v));
}
void OptionsParamWidget::ChangeIte(int v)
{
  param->setValue(PARAM_ITE,to_string(v));
}
void OptionsParamWidget::ChangeThin(int v)
{
  param->setValue(PARAM_THIN,to_string(v));
}

void OptionsParamWidget::setParam(ConfigXML * p)
{
  param = p;
}

void OptionsParamWidget::initValue()
{
  seedSpinBox->setValue(atoi(param->getValue(MEMM_SEED)));
  burnInSB->setValue(atoi(param->getValue(MEMM_BURNIN)));
  iteSB->setValue(atoi(param->getValue(MEMM_ITE)));
  thinSB->setValue(atoi(param->getValue(MEMM_THIN)));
}
