/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "DistributionLayout.hpp"

DistributionLayout::DistributionLayout(ConfigXML * p, string dist_name) : QHBoxLayout()
{
  p_ = p;
 
  if(dist_name != "")
  {
    ID_ = dist_name;
    name_ = new QLineEdit(QString(dist_name.c_str()));
  }
  else
  {
    ID_ = string("Distribution_name");
    name_ = new QLineEdit("Distribution_name");
  }
  name_->setValidator(new QRegExpValidator(reg_valid_));
  name_->setMinimumWidth(150);
  name_->setMaximumWidth(150);
  name_->setMaxLength(40);

  laws_label_ = new QLabel("law");
  laws_label_->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
  laws_label_->setMinimumWidth(20);
  laws_label_->setMaximumWidth(20);

  laws_ = new QComboBox();
  laws_->setMinimumWidth(100);
  laws_->setMaximumWidth(100);
  map<string,vector<string>> list = p_->getDistributionLawsNameAndParametersName();

  for(map<string,vector<string>>::iterator it = list.begin(); it != list.end(); ++it)
  {
    laws_->addItem(QString(it->first.c_str()));
  }
  if(dist_name != "")
  {
    laws_->setCurrentIndex(laws_->findText(QString(p_->getDistributionType(dist_name).c_str())));
  }

  vector<string> list_name_param = p_->getParametersName();
  param_label_ = new QLabel*[MAX_NB_OF_PARAMETERS];
  param_combo_ = new QComboBox*[MAX_NB_OF_PARAMETERS];
  for(int i = 0 ; i<MAX_NB_OF_PARAMETERS; ++i)
  {
    param_label_[i] = new QLabel("Param");
    param_combo_[i] = new QComboBox();
    param_label_[i]->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    param_label_[i]->setMinimumWidth(50);
    param_label_[i]->setMaximumWidth(50);
    param_combo_[i]->setMinimumWidth(100);
    param_combo_[i]->setMaximumWidth(100);
    for(vector<string>::iterator it=list_name_param.begin(); it != list_name_param.end(); ++it)
      param_combo_[i]->addItem(QString((*it).c_str()));

    param_label_[i]->setDisabled(true);
    param_combo_[i]->setDisabled(true);
    connect(param_combo_[i],static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this, &DistributionLayout::valueChanged);
  }

  setDistributionParameters(list[laws_->currentText().toStdString()], p_->getDistributionParameters(dist_name));

  rm_button_ = new QPushButton("X");
  rm_button_->setMinimumSize(25,0);
  rm_button_->setMaximumWidth(25);
  rm_button_->setToolTip("Remove Distribution");
  rm_button_->setStyleSheet("QPushButton {color: red;}");
  connect(rm_button_,&QPushButton::clicked,this,&DistributionLayout::delDistributionLayout);

  if(dist_name == "" ) laws_->setDisabled(true);

  addWidget(name_);
  addWidget(laws_label_);
  addWidget(laws_);
  for(int i = 0 ; i<MAX_NB_OF_PARAMETERS; ++i)
  {
    addWidget(param_label_[i]);
    addWidget(param_combo_[i]);
  }
  addWidget(rm_button_);
  addStretch(1);

  connect(name_, &QLineEdit::editingFinished,this, &DistributionLayout::valueChanged);
  connect(name_, &QLineEdit::textChanged,this, &DistributionLayout::valueChanged);
  connect(laws_,static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this, &DistributionLayout::lawComboValueChanged);
}

DistributionLayout::~DistributionLayout()
{
  this->disconnect();
  delete name_;
  delete laws_label_;
  delete laws_;
  for(int i=0; i<MAX_NB_OF_PARAMETERS ; ++i)
  {
    delete param_label_[i];
    delete param_combo_[i];
  }
  delete rm_button_;
  if(p_) p_->deleteDistribution(ID_);
}

void DistributionLayout::valueChanged()
{
  string name = name_->displayText().toStdString();
  if(name == "Distribution_name" || name == "")
  {
    laws_->setDisabled(true);
    for(unsigned int i=0; i<MAX_NB_OF_PARAMETERS; ++i)
    {
      param_label_[i]->setDisabled(true);
      param_combo_[i]->setDisabled(true);
    }
  }
  else
  { 
    laws_->setDisabled(false);
    for(unsigned int i=0; i<MAX_NB_OF_PARAMETERS; ++i)
    {
      param_label_[i]->setDisabled(false);
      param_combo_[i]->setDisabled(false);
    }

    if(name != ID_)
    {
      p_->deleteDistribution(ID_);  
    }

    map<string,string> name_val;
    for(int i=0; i<nb_of_param_; ++i)
      name_val.insert(pair<string,string>(param_label_[i]->text().toStdString(),param_combo_[i]->currentText().toStdString()));
    
    p_->setDistribution(name_->displayText().toStdString(),
      laws_->currentText().toStdString(),
      name_val);

    ID_ = name_->displayText().toStdString();
  }
}

void DistributionLayout::lawComboValueChanged()
{
  map<string,vector<string>> list = p_->getDistributionLawsNameAndParametersName();
  setDistributionParameters(list[laws_->currentText().toStdString()],map<string,string>());
  valueChanged();
}

void DistributionLayout::delDistributionLayout()
{
  QString filename;
  QMessageBox msgBox;
  msgBox.setText("Whould you delete this item ?");
  msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
  msgBox.setDefaultButton(QMessageBox::Cancel);
  int ret = msgBox.exec();

  //check button signal
  switch (ret) {
    case QMessageBox::Ok:
      p_->deleteDistribution(ID_);
      emit removeMe(this);
      break;
    case QMessageBox::Cancel:
      break;
    default:
      break;
  }
}

void DistributionLayout::updateDistributionParameters()
{
  vector<string> list_name_param = p_->getParametersName();
  QString param_value;
  int ind;
  
  for(int i = 0 ; i<MAX_NB_OF_PARAMETERS; ++i)
  {
    param_value = param_combo_[i]->currentText();
    param_combo_[i]->clear();
    disconnect(param_combo_[i],0,0,0);
    for(vector<string>::iterator it=list_name_param.begin(); it != list_name_param.end(); ++it)
      param_combo_[i]->addItem(QString((*it).c_str()));

    if( (ind = param_combo_[i]->findText(param_value)) >= 0) param_combo_[i]->setCurrentIndex(ind);

    connect(param_combo_[i],static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this, &DistributionLayout::valueChanged);
  }


}

/******************** PRIVATE ***********************/
void DistributionLayout::setDistributionParameters(vector<string> param_name, map<string,string> list_params)
{
  nb_of_param_ = param_name.size();

  int i = 0;
  for(i; i<nb_of_param_; ++i)
  {
      param_label_[i]->setText(QString(param_name[i].c_str()));
      param_label_[i]->setVisible(true);
      param_combo_[i]->setVisible(true);

      if( list_params.size() > i)
      {
        param_label_[i]->setDisabled(false);
        param_combo_[i]->setDisabled(false);
        param_combo_[i]->setCurrentIndex(param_combo_[i]->findText(QString(list_params[param_name[i]].c_str())));
      }
  }
  
  for(i; i<MAX_NB_OF_PARAMETERS; ++i)
  {
      param_label_[i]->hide();
      param_combo_[i]->hide();
  }
}
