/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "InputParamWidget.hpp"


InputParamWidget::InputParamWidget() : QWidget()
{
    vLayout = new QFormLayout();

    /*
     * File and data for parents
     * */
    QHBoxLayout * hLayout1;
    QPushButton * cFile1;

    hLayout1 = new QHBoxLayout();
    pLEdit1 = new QLineEdit();
    pLEdit1->setReadOnly(true);
    hLayout1->addWidget(pLEdit1);
    cFile1 = new QPushButton("File",this);
    hLayout1->addWidget(cFile1);
    QObject::connect(cFile1,SIGNAL(clicked()),this,SLOT(ChooseFileParent()));
    vLayout->addRow(new QLabel("Parents file:"),hLayout1);


    nlSpinBox = new QSpinBox();
    nlSpinBox->resize(10,nlSpinBox->height());
    QObject::connect(nlSpinBox,SIGNAL(valueChanged(int)),this,SLOT(ChangeLocusNumberParent(int)));
    vLayout->addRow(new QLabel("Numbers of locus:"),nlSpinBox);
    
    ccSpinBox = new QSpinBox();
    vLayout->addRow(new QLabel("Class covariates:"),ccSpinBox);
    QObject::connect(ccSpinBox,SIGNAL(valueChanged(int)),this,SLOT(ChangeClassCovariatesParent(int)));

    qcSpinBox = new QSpinBox();
    vLayout->addRow(new QLabel("Quantitative covariates:"),qcSpinBox);
    QObject::connect(qcSpinBox,SIGNAL(valueChanged(int)),this,SLOT(ChangeQtCovariatesParent(int)));

    wvSpinBox = new QSpinBox();
    vLayout->addRow(new QLabel("Weighting variables:"),wvSpinBox);
    QObject::connect(wvSpinBox,SIGNAL(valueChanged(int)),this,SLOT(ChangeWeightVarParent(int)));

    /*
     * File and data for offspring
     */
    QHBoxLayout * hLayout6;
    QPushButton * cFile2;

    hLayout6 = new QHBoxLayout();
    oLEdit2 = new QLineEdit();
    oLEdit2->setReadOnly(true);
    hLayout6->addWidget(oLEdit2);
    cFile2 = new QPushButton("File",this);
    hLayout6->addWidget(cFile2);
    vLayout->addRow(new QLabel("Offspring file:"),hLayout6);
    QObject::connect(cFile2,SIGNAL(clicked()),this,SLOT(ChooseFileOffspring()));


    nlSpinBox2 = new QSpinBox();
    vLayout->addRow(new QLabel("Numbers of locus:"),nlSpinBox2);
    QObject::connect(nlSpinBox2,SIGNAL(valueChanged(int)),this,SLOT(ChangeLocusNumberOffspring(int)));
    
    /*
     * File and mode for allelic frequencies
     */
    QHBoxLayout * hLayoutAF;
    hLayoutAF = new QHBoxLayout();
    afCBox3 = new QCheckBox("enable");
    afCBox3->setChecked(true);
    QObject::connect(afCBox3,SIGNAL(stateChanged(int)), this, SLOT(CheckAF(int)));
    afLEdit3 = new QLineEdit();
    afLEdit3->setReadOnly(true);
    hLayoutAF->addWidget(afCBox3);
    hLayoutAF->addWidget(afLEdit3);
    cFile3 = new QPushButton("File",this);
    hLayoutAF->addWidget(cFile3);
    vLayout->addRow(new QLabel("Allelic frequencies file:"),hLayoutAF);
    QObject::connect(cFile3,SIGNAL(clicked()),this,SLOT(ChooseFileAF()));
    
    /*
     * File and mode for allelic frequencies
     */
    QHBoxLayout * hLayoutdf;
    hLayoutdf = new QHBoxLayout();
    dfCBox4 = new QCheckBox("enable");
    dfCBox4->setChecked(true);
    QObject::connect(dfCBox4,SIGNAL(stateChanged(int)), this, SLOT(CheckDF(int)));
    dfLEdit4 = new QLineEdit();
    dfLEdit4->setReadOnly(true);
    hLayoutdf->addWidget(dfCBox4);
    hLayoutdf->addWidget(dfLEdit4);
    cFile4 = new QPushButton("File",this);
    hLayoutdf->addWidget(cFile4);
    vLayout->addRow(new QLabel("Distance file:"),hLayoutdf);
    QObject::connect(cFile4,SIGNAL(clicked()),this,SLOT(ChooseFileDF()));
  
    /*
     * File and mode for allelic frequencies
     */
    QHBoxLayout * hLayoutle;
    hLayoutle = new QHBoxLayout();
    leCBox5 = new QCheckBox("enable");
    leCBox5->setChecked(true);
    QObject::connect(leCBox5,SIGNAL(stateChanged(int)), this, SLOT(CheckLE(int)));
    leLEdit5 = new QLineEdit();
    leLEdit5->setReadOnly(true);
    hLayoutle->addWidget(leCBox5);
    hLayoutle->addWidget(leLEdit5);
    cFile5 = new QPushButton("File",this);
    hLayoutle->addWidget(cFile5);
    vLayout->addRow(new QLabel("Locus error file:"),hLayoutle);
    QObject::connect(cFile5,SIGNAL(clicked()),this,SLOT(ChooseFileLE()));

    setLayout(vLayout);

}

InputParamWidget::InputParamWidget(ConfigXML *p) : InputParamWidget()
{
    param = p;
}

InputParamWidget::~InputParamWidget(){}

void InputParamWidget::setParam(ConfigXML * p)
{
    param = p;
}

void InputParamWidget::initValue()
{
    pLEdit1->setText(QString((param->getValue(INPUT_PARENTS_FILE_NAME)).c_str()));
    nlSpinBox->setValue(atoi(param->getValue(INPUT_PARENTS_FILE_NUMBERS_OF_LOCUS).c_str()));
    ccSpinBox->setValue(atoi(param->getValue(INPUT_PARENTS_FILE_CLASS_COV).c_str()));
    qcSpinBox->setValue(atoi(param->getValue(INPUT_PARENTS_FILE_QT_COV).c_str()));
    wvSpinBox->setValue(atoi(param->getValue(INPUT_PARENTS_FILE_WEIGHT_VAR).c_str()));
    
    oLEdit2->setText(QString((param->getValue(INPUT_OFFSPRING_FILE_NAME)).c_str()));
    nlSpinBox2->setValue(atoi(param->getValue(INPUT_OFFSPRING_FILE_NUMBERS_OF_LOCUS).c_str()));

    afLEdit3->setText(QString((param->getValue(INPUT_AF_FILE_NAME)).c_str()));
    if(param->getValue(INPUT_AF,"enable") == "false")
    {
      CheckAF(0);
      afCBox3->setChecked(false);
    }
    dfLEdit4->setText(QString((param->getValue(INPUT_DIST_FILE_NAME)).c_str()));
    if(param->getValue(INPUT_DIST,"enable") == "false")
    {
      CheckLE(0); 
      dfCBox4->setChecked(false);
    }
    leLEdit5->setText(QString((param->getValue(INPUT_MEMM_LOCUS_ERROR_FILE)).c_str()));
    if(param->getValue(INPUT_MEMM_LOCUS_ERROR,"enable") == "false")
    {
      CheckLE(0); 
      leCBox5->setChecked(false);
    }
}

void InputParamWidget::ChooseFileParent()
{
    QString filename = QFileDialog::getOpenFileName(this,tr("Open text"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL){
        pLEdit1->setText(filename);
        param->setValue(INPUT_PARENTS_FILE_NAME,filename.toStdString());
    }

}

void InputParamWidget::ChangeLocusNumberParent(int v)
{
    param->setValue(INPUT_PARENTS_FILE_NUMBERS_OF_LOCUS,to_string(v));
}
void InputParamWidget::ChangeClassCovariatesParent(int v)
{
    param->setValue(INPUT_PARENTS_FILE_CLASS_COV,to_string(v));
}
void InputParamWidget::ChangeQtCovariatesParent(int v)
{
    param->setValue(INPUT_PARENTS_FILE_QT_COV,to_string(v));
}
void InputParamWidget::ChangeWeightVarParent(int v)
{
    param->setValue(INPUT_PARENTS_FILE_WEIGHT_VAR,to_string(v));
}

void InputParamWidget::ChooseFileOffspring()
{
    QString filename = QFileDialog::getOpenFileName(this,tr("Open text"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        oLEdit2->setText(filename);
        param->setValue(INPUT_OFFSPRING_FILE_NAME,filename.toStdString());
    }

}
void InputParamWidget::ChangeLocusNumberOffspring(int v)
{
    param->setValue(INPUT_OFFSPRING_FILE_NUMBERS_OF_LOCUS,to_string(v));
}

void InputParamWidget::CheckAF(int state)
{
  if(state)
  {
    afLEdit3->setDisabled(false);
    cFile3->setDisabled(false);
    param->enableElement("AF",true);
  }
  else
  {
    param->enableElement("AF",false);
    afLEdit3->setDisabled(true);
    cFile3->setDisabled(true);
  }
}

void InputParamWidget::ChooseFileAF()
{
    QString filename = QFileDialog::getOpenFileName(this,tr("Open text"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        afLEdit3->setText(filename);
        param->setValue(INPUT_AF_FILE_NAME,filename.toStdString());
    }
}

void InputParamWidget::CheckDF(int state)
{
  if(state)
  {
    dfLEdit4->setDisabled(false);
    cFile4->setDisabled(false);
    param->enableElement("Dist",true);
  }
  else
  {
    param->enableElement("Dist",false);
    dfLEdit4->setDisabled(true);
    cFile4->setDisabled(true);
  }
}

void InputParamWidget::ChooseFileDF()
{
    QString filename = QFileDialog::getOpenFileName(this,tr("Open text"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        dfLEdit4->setText(filename);
        param->setValue(INPUT_DIST_FILE_NAME,filename.toStdString());
    }
}

void InputParamWidget::CheckLE(int state)
{
  if(state)
  {
    leLEdit5->setDisabled(false);
    cFile5->setDisabled(false);
    param->enableElement("LE",true);
  }
  else
  {
    param->enableElement("LE",false);
    leLEdit5->setDisabled(true);
    cFile5->setDisabled(true);
  }
}

void InputParamWidget::ChooseFileLE()
{
    QString filename = QFileDialog::getOpenFileName(this,tr("Open text"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        leLEdit5->setText(filename);
        param->setValue(INPUT_MEMM_LOCUS_ERROR_FILE,filename.toStdString());
    }
}

