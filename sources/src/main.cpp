/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include <iostream>
#include <getopt.h>

#include "Configuration.hpp"
#include "ConfigTXT.hpp"
#include "ConfigXML.hpp"
#include "MEMM.hpp"
#include "MEMM_logLik.hpp"
#include "MEMM_seedlings.hpp"
#include "MEMM_seedlings_2kernels.hpp"

#define CLASSIC 0
#define SEEDLINGS 1
#define SEEDLINGS_2K 2  // A vérifier

#ifndef MEMM_DATA_PATH
#define MEMM_DATA_PATH "."
#endif

using namespace std;

void usage() {
  cout <<
    "------------------------------------------------------------------------------\n" <<
    " MEMM v2.0 under GPL v3 by INRA BioSP Avignon, FRANCE \n" <<
    "------------------------------------------------------------------------------\n" <<
    "USAGE  : MEMM [options]\n" <<
    "OPTIONS :\n" <<
    "   -h              show this message\n" <<
    "   -v              show version info\n" <<
    "   -V              set verbose mode\n" <<
    "   -p <file_in>    a configuration file\n" <<
    "   -s              run seedlings mode\n" <<
    "   -k              run seedlings mode with 2 kernels\n" <<
    "  eg. MEMM -p parameters.xml \n" <<
    "------------------------------------------------------------------------------\n";
}

int main(int argc,char ** argv)
{

  int mode = CLASSIC;
  static int verbose_flag = 0;
  int opt;
  extern char *optarg;
  char * input = NULL;
  char * extension;
  Configuration * configuration = NULL;

  static struct option long_options[] =
  {
    {"verbose", no_argument, 0, 'V'},
    {"version", no_argument, 0, 'v'},
    {"help", no_argument, 0, 'h'},
    {"parameters", required_argument, 0, 'p'},
    {"seedlings", no_argument, 0, 's'},
    {"seedlings_2k", no_argument, 0, 'k'},
    {0, 0, 0, 0}

  };
  int option_index = 0;

  while ((opt = getopt_long (argc, argv, "hvVp:sk",long_options, &option_index)) != -1)
      switch(opt)
      {
        case 'h':
            usage();
            exit(0);
        case 'V':
            verbose_flag = 1;
            cerr<<"set Verbose mode"<<endl;
            break;
        case 'v':
            cerr<<argv[0]<<" version : 2.0"<<endl;
            exit(0);
        case 'p':
            input = optarg;
            break;
        case 's':
            mode = SEEDLINGS;
            break;
        case 'k':
              mode = SEEDLINGS_2K;
              break;
       case '?':
            cerr<<"Unknown option "<<opt<<endl;
            return 1;
        default : break;
      }


  if(input!=NULL)
  {
    extension = strrchr(input,'.');

    if(extension != NULL && strcmp(extension, ".txt") == 0)
    {
      configuration = new ConfigTXT(input);
    }
    else if(extension != NULL && strcmp(extension,".xml") == 0)
    {
      configuration = new ConfigXML(input);
    }
    else
    {
      cerr<<"Can not open "<<input<<" file"<<endl;
    }
    
    if(configuration)
    {
      if(configuration->isOK()) cout<<"Configuration file "<< input<<" loaded"<<endl;
      else{
        cerr<<"Configuration file "<<input<<" can not be loaded"<<endl;
        delete configuration;
        configuration = NULL;
      }
    }
  }

  if(input==NULL || configuration == NULL)
  {
    cout<<"Try Local configuration file "<<DEFAULT_CONFIGURATION_FILE<<endl;
    configuration = new ConfigXML((char*)(string(DEFAULT_CONFIGURATION_FILE)).c_str());
    if(configuration->isOK())
    {
      cout<<"OK"<<endl;
    }
    else
    {
      delete configuration;
      cerr<<"Can not load local configuration file "<<DEFAULT_CONFIGURATION_FILE<<endl;
      cout<<"Try default configuration file "<< MEMM_DATA_PATH<<DEFAULT_CONFIGURATION_FILE<<endl;
      configuration = new ConfigXML((char*)(string(MEMM_DATA_PATH) + string(DEFAULT_CONFIGURATION_FILE)).c_str());
      if(configuration->isOK())
      {
        cout<<"OK"<<endl;
      }
      else
      {
        cerr<<"Can not load default configuration file "<<DEFAULT_CONFIGURATION_FILE<<endl;
        exit(0);
      }

    }
  }

  MEMM * memm;

  switch(mode)
  {
    case CLASSIC :
      memm = new MEMM_logLik();
      break;
    case SEEDLINGS :
      memm = new MEMM_seedlings();
      break;
    case SEEDLINGS_2K :
          memm = new MEMM_seedlings_2kernels();
          break;
    default :
      usage();
      exit(0);
      break;

  }
  memm->init(configuration);
  memm->burnin();
  memm->run();

  delete configuration;
  delete memm;

  return EXIT_SUCCESS;
}

