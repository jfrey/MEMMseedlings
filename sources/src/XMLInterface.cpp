/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/**
 * \file XMLInterface.cpp
 * \brief Class XMLInterface implementation
 * \author Jean-François REY
 * \version 1.5
 * \date 04 dec 2013
 *
 * XMLInterface class is a little API to manipulate xml file using libxml2
 */

#include "XMLInterface.hh"

XMLInterface::XMLInterface()
{
	doc = NULL;
	root_node = NULL;
	verbose = false;
	dtd = NULL;
	xmlKeepBlanksDefault(0);

}


XMLInterface::~XMLInterface()
{
	/*free the document */
    if(doc != NULL ) xmlFreeDoc(doc);

    if(dtd != NULL) xmlFreeDtd(dtd); 
    /*
     *Free the global variables that may
     *have been allocated by the parser.
     */
    xmlCleanupParser();

	xmlMemoryDump();
}

void XMLInterface::setVerbose(bool b){ verbose = b;}

int XMLInterface::checkDTD()
{
    xmlValidCtxtPtr vctxt;
    int ret;

    if( (dtd = xmlGetIntSubset(doc)) == NULL) return -1;
    if( (dtd = xmlParseDTD(NULL,dtd->SystemID)) == NULL) return -1;
   
    // Création du contexte de validation
    if ((vctxt = xmlNewValidCtxt()) == NULL) {
	fprintf(stderr,"Failed to allocate new validation context\n");
        return -1;
    }
    vctxt->userData = (void *) stderr;
    vctxt->error = (xmlValidityErrorFunc) fprintf;
    vctxt->warning = (xmlValidityWarningFunc) fprintf;
    
    ret = xmlValidateDtd(vctxt, doc, dtd);
    // Libération de la mémoire
    xmlFreeValidCtxt(vctxt);
    
    return ret;
}

xmlNodePtr XMLInterface::getRootNode(){ return root_node; }

xmlDocPtr XMLInterface::createNewDocument() throw (Myexception)
{
	/* Create a new XML DOM tree, to which the XML document will be
     * written */
    doc = xmlNewDoc(BAD_CAST XML_DEFAULT_VERSION);
    if (doc == NULL) {
		Myexception(0,"XMLInterface::createNewDocument: Error creating the xml document\n");
        return NULL;
    }
	doc->encoding = new xmlChar[strlen(MY_ENCODING)];
	strcpy((char*)doc->encoding, MY_ENCODING);
	doc->standalone = 1;
	
    /* Create a new XML node, to which the XML document will be
     * appended */
    root_node = xmlNewDocNode(doc, NULL, BAD_CAST "document", NULL);
    if (root_node == NULL) {
		Myexception(0,"XMLInterface::createNewDocument: Error creating document xml node\n");
        return NULL;
    }

	/* Make ELEMENT the root node of the tree */
    xmlDocSetRootElement(doc, root_node);

	return doc;

}

xmlAttrPtr XMLInterface::setFileNameAttribut(std::string filename)
{
	return xmlSetProp(root_node, BAD_CAST "filename" ,BAD_CAST filename.c_str() );
}

xmlNodePtr XMLInterface::addTextToNode(std::string tag,std::string value)
{
	xmlNodePtr node = this->find(tag,root_node);
	xmlNodeSetContent(node, BAD_CAST value.c_str());
	return node;
}

xmlNodePtr XMLInterface::addTextToNode(std::string value,xmlNodePtr node)
{
	xmlNodeSetContent(node, BAD_CAST value.c_str());
	return node;
}

string XMLInterface::getText(xmlNodePtr node, string tag)
{
  string res;
  char * temp;
  xmlNodePtr node_ptr = find(tag,node);
  temp = (char*)xmlNodeGetContent(node_ptr);
  res = string(temp);
  free(temp);

  return(res);
}

string XMLInterface::getText(xmlNodePtr node)
{
  char * temp;
  string res;

  temp = (char*) xmlNodeGetContent(node);
  res = string(temp);
  free(temp);

  return res;
}

xmlNodePtr XMLInterface::addXPathElement(string& xpath_expr, string& elt, string& text)
{
    xmlXPathContextPtr xpathCtx; 
    xmlXPathObjectPtr xpathObj;
    int i,size;
    xmlNodePtr new_node;
    xmlNodeSetPtr nodes;


    /* Create xpath evaluation context */
    xpathCtx = xmlXPathNewContext(doc);
    if(xpathCtx == NULL) {
        cerr << "Error: unable to create new XPath context"<<endl;
        return NULL;
    }

    /* Evaluate xpath expression */
    xpathObj = xmlXPathEvalExpression((const unsigned char*)xpath_expr.c_str(), xpathCtx);
    if(xpathObj == NULL) {
        cerr << "Error: unable to evaluate xpath expression "<<xpath_expr<<endl;
        xmlXPathFreeContext(xpathCtx); 
        return NULL;
    }

    // TODO check if xpath_expr/elt exist deja ou pas
    
        nodes = xpathObj->nodesetval;
     
    size = (nodes) ? nodes->nodeNr : 0;
    for(i = 0; i < size; ++i) {
        if(nodes->nodeTab[i]->type == XML_ELEMENT_NODE) {
        	new_node = xmlNewChild(nodes->nodeTab[i], NULL, BAD_CAST elt.c_str(), BAD_CAST text.c_str());
        }
    }

    /* Cleanup */
    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx); 

    return new_node;


}


string XMLInterface::getAttribut(xmlNodePtr node, string name)
{
	char* result;
	string res;
	if(node != NULL) result = (char *)xmlGetProp(node, BAD_CAST name.c_str());
	if(result == NULL) return string("");
	else{ res = string(result); free(result);return res;}
}

void XMLInterface::addAttribut(xmlNodePtr node, string attribut, string value)
{
  //if(value == "") xmlFreeProp(xmlSetProp(node,BAD_CAST attribut.c_str(), BAD_CAST ""));
  if(value == "") xmlUnsetProp(node,BAD_CAST attribut.c_str());
  else xmlSetProp(node,BAD_CAST attribut.c_str(), BAD_CAST value.c_str());
}

string XMLInterface::getXPathValue(string& xpath_expr, string& attribut)
{
    xmlXPathContextPtr xpathCtx; 
    xmlXPathObjectPtr xpathObj;
    xmlNodeSetPtr nodes;
    string result;
    xmlChar * temp;
    int i;

    /* Create xpath evaluation context */
    xpathCtx = xmlXPathNewContext(doc);
    if(xpathCtx == NULL) {
        cerr << "Error: unable to create new XPath context"<<endl;
        return NULL;
    }

    /* Evaluate xpath expression */
    xpathObj = xmlXPathEvalExpression((const unsigned char*)xpath_expr.c_str(), xpathCtx);
    if(xpathObj == NULL) {
        cerr << "Error: unable to evaluate xpath expression "<<xpath_expr<<endl;
        xmlXPathFreeContext(xpathCtx); 
        return NULL;
    }

    /* Several results
    int size,i;
    xmlNodePtr cur;
    xmlNodeSetPtr nodes;
    nodes = xpathObj->nodesetval;
     
    size = (nodes) ? nodes->nodeNr : 0;
    for(i = 0; i < size; ++i) {
        cout<<nodes->nodeTab[i]->type;
        if(nodes->nodeTab[i]->type == XML_ELEMENT_NODE) {
            cur = nodes->nodeTab[i];
            //fprintf(stdout, "= element node \"%s\"\n", cur->name);
        } else {
            cur = nodes->nodeTab[i];    
            //fprintf(stdout, "= node \"%s\": value %s\n", cur->name, cur->content);
            result.assign((char*)cur->content);
        }
    }*/
    // First result
    nodes = xpathObj->nodesetval;
    if(nodes && nodes->nodeNr)
    {
        i = 0;
        if(attribut.empty())
        {
            /*if(nodes->nodeTab[i]->type == XML_TEXT_NODE)*/
            
            //Will get all text for the node and chidrens
            temp=xmlNodeGetContent(nodes->nodeTab[i]);
            if(temp)
            {
                result.assign((char *)temp);
                xmlFree(temp);
            }
        }
        else
        {
            if(nodes->nodeTab[i]->type == XML_ELEMENT_NODE)
            {
                temp = xmlGetProp(nodes->nodeTab[i],BAD_CAST attribut.c_str());
                if(temp)
                {
                    result.assign((char*)temp);
                    xmlFree(temp);
                }
            }
        }
    }

    /* Cleanup */
    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx); 

    return result;
}

bool XMLInterface::setXPathValue(string& xpath_expr, string& value, string& attribut)
{
    int i, size;
    xmlNodeSetPtr nodes;

     /* Create xpath evaluation context */
    xmlXPathContextPtr xpathCtx = xmlXPathNewContext(doc);
    if(xpathCtx == NULL) {
        cerr << "Error: unable to create new XPath context"<<endl;
        return -1;
    }

    /* Evaluate xpath expression */
    xmlXPathObjectPtr xpathObj = xmlXPathEvalExpression((const unsigned char*)xpath_expr.c_str(), xpathCtx);
    if(xpathObj == NULL) {
        cerr << "Error: unable to evaluate xpath expression "<<xpath_expr<<endl;
        xmlXPathFreeContext(xpathCtx); 
        return -1;
    }

    
    nodes = xpathObj->nodesetval;
    // size should be at 1
    size = (nodes) ? nodes->nodeNr : 0;
   
    for(i=size-1; i>=0; i--)
    {
        if(attribut.empty()){if(nodes->nodeTab[i]->children == nodes->nodeTab[i]->last)
/* if(nodes->nodeTab[i]->content != NULL)*/ xmlNodeSetContent(nodes->nodeTab[i],BAD_CAST value.c_str());}
        else if(nodes->nodeTab[i]->type == XML_ELEMENT_NODE) xmlSetProp(nodes->nodeTab[i],BAD_CAST attribut.c_str(),BAD_CAST value.c_str());
        // Do it for memory trouble
        if (nodes->nodeTab[i]->type != XML_NAMESPACE_DECL)
            nodes->nodeTab[i] = NULL;
    }

    /* Cleanup */
    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx); 
    return 1;
}

xmlNodePtr XMLInterface::addTagNode(string tag, xmlNodePtr node)
{
  return xmlNewChild(node,NULL,BAD_CAST tag.c_str(),BAD_CAST NULL);
}

xmlNodePtr XMLInterface::find(std::string tag, xmlNodePtr node)
{
  if(node == NULL) node = this->getRootNode();
	xmlNodePtr result = NULL;
	for (xmlNode* child = node->children ; child ; child = child->next)
  	{
    	if (child->type == XML_ELEMENT_NODE && xmlStrEqual(child->name, BAD_CAST tag.c_str()))
	    {
		    return child;
    	}
    	else if(result = find(tag, child)) break;
  	}

	return result;
}

std::list<xmlNodePtr> XMLInterface::findAll(std::string tag, xmlNodePtr node)
{
	std::list<xmlNodePtr> result;

	for (xmlNode* child = node->children ; child ; child = child->next)
  	{
    	if (child->type == XML_ELEMENT_NODE && xmlStrEqual(child->name, BAD_CAST tag.c_str()))
	    {
		    result.push_back(child);
    	}
    	else
		{
			std::list<xmlNodePtr> temp = findAll(tag, child);
			temp.insert(result.end(),temp.begin(),temp.end());
		}
  	}

	if( result.empty() && node->type == XML_ELEMENT_NODE && xmlStrEqual(node->name, BAD_CAST tag.c_str()))
        {
		result.push_back(node);
    	}


	return result;
}

xmlNodePtr XMLInterface::findTagAttribut(string tag, string attribut, string value, xmlNodePtr node)
{
  xmlNodePtr res = NULL;

  if (node->type == XML_ELEMENT_NODE && xmlStrEqual(node->name, BAD_CAST tag.c_str()) && getAttribut(node,attribut) == value) return node;
  else
  {
    for (xmlNodePtr child = node->children ; child ; child = child->next)
    {
      if( res = findTagAttribut(tag,attribut,value,child) ) break;
    }
  }
  return res;
}

xmlDocPtr XMLInterface::loadFile(std::string filename) throw (Myexception)
{
	if(filename.c_str() == NULL || filename == "")
	{
		Myexception(0,"XMLInterface::loadFile: no xml file\n");
        return NULL;
    }
	doc = xmlParseFile(filename.c_str());
	if (doc == NULL)
	{
		Myexception(0,"XMLInterface::loadFile: Error loading the xml file "+filename+"\n");
        return NULL;
    }

	root_node = xmlDocGetRootElement(doc);
    if (root_node == NULL)
	{
        Myexception(0,"Document XML empty\n");
        xmlFreeDoc(doc);
        return NULL;
    }

	return doc;
}

xmlDocPtr XMLInterface::loadString(std::string buffer) throw (Myexception)
{
	doc = xmlParseMemory(buffer.c_str(),(unsigned int)buffer.size());
	if (doc == NULL)
	{
		Myexception(0,"XMLInterface::loadString: Error loading the xml content\n");
        return NULL;
    }

	root_node = xmlDocGetRootElement(doc);
	if (root_node == NULL)
	{
		Myexception(0,"Document XML empty\n");
	    xmlFreeDoc(doc);
	    return NULL;
	}

	return doc;
}

bool XMLInterface::save(std::string& filename)
{
	return xmlSaveFormatFileEnc(filename.c_str(), doc, MY_ENCODING, 1);
}

bool XMLInterface::print(){
	/*std::streambuf * buf;
	buf = std::cout.rdbuf();*/
	/*std::ostream out(buf);*/
//xmlDocDumpFormatMemoryEnc(doc,&(buf->eback()),buf->in_avail(),MY_ENCODING,1);
	return xmlDocFormatDump(stdout, doc,1);
	
}

xmlNodePtr XMLInterface::stampDocument(std::string module, std::string version)
{
	xmlNodePtr stamp = NULL;
	if(this->root_node == NULL){ cerr<<"error in stampDocument root_node is NULL"<<endl; return NULL;}
	xmlNodePtr meta = find("meta", root_node);

	if(meta == NULL) meta = xmlNewChild(root_node, NULL, BAD_CAST "meta", NULL);
	if(meta != NULL)
	{
		xmlNodePtr stamps = find("stamps",meta);
		if(stamps == NULL) stamps = xmlNewChild(meta, NULL, BAD_CAST "stamps", NULL);

		stamp = xmlNewChild(stamps, NULL, BAD_CAST "stamp", NULL);
		xmlSetProp(stamp, BAD_CAST "module", BAD_CAST module.c_str());
		xmlSetProp(stamp, BAD_CAST "version", BAD_CAST version.c_str());
		xmlSetProp(stamp, BAD_CAST "timestamp", BAD_CAST getTimestamp().c_str());
	}

	return stamp;
}

void XMLInterface::deleteNode(xmlNodePtr node)
{
  xmlUnlinkNode(node);
  xmlFreeNode(node);
}


std::string XMLInterface::getTimestamp()
{
  time_t rawtime;
  time(&rawtime);
  std::string timeStr(ctime(&rawtime));
  return timeStr.substr(0,timeStr.find('\n'));
}

xmlChar * XMLInterface::ConvertInput(const char *in, const char *encoding)
{
    xmlChar *out;
    int ret;
    int size;
    int out_size;
    int temp;
    xmlCharEncodingHandlerPtr handler;

    if (in == 0)
        return 0;

    handler = xmlFindCharEncodingHandler(encoding);

    if (!handler) {
        //std::cerr<<"ConvertInput: no encoding handler found for "<<encoding<<std::endl;
        return 0;
    }

    size = (int) strlen(in) + 1;
    out_size = size * 2 - 1;
    out = (unsigned char *) xmlMalloc((size_t) out_size);

    if (out != 0) {
        temp = size - 1;
        ret = handler->input(out, &out_size, (const xmlChar *) in, &temp);
        if ((ret < 0) || (temp - size + 1)) {
            if (ret < 0) {
                //std::cerr<<"ConvertInput: conversion wasn't successful.\n";
            } else {
                //std::cerr<<"ConvertInput: conversion wasn't successful. converted: "<<temp<<" octets.\n";
            }

            xmlFree(out);
            out = 0;
        } else {
            out = (unsigned char *) xmlRealloc(out, out_size + 1);
            out[out_size] = 0;  /*null terminating out */
        }
    } else {
        //std::cerr<<"ConvertInput: no mem\n";
    }

    return out;
}


