/**************************************************************************************************************
 * MEMM is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * Copyright (c) 2009-2016 Etienne Klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * This file is part of MEMM.                                                                                 *
 *                                                                                                            *
 * MEMM is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the GNU General Public License as published by                                       *
 * the Free Software Foundation, either version 3 of the License, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * MEMM is distributed in the hope that it will be useful,                                                    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                                             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                              *
 * GNU General Public License for more details.                                                               *
 *                                                                                                            *
 * You should have received a copy of the GNU General Public License                                          *
 * along with MEMM.  If not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "ConfigTXT.hpp"

ConfigTXT::ConfigTXT() : Configuration(){}

ConfigTXT::ConfigTXT(char * file) : Configuration(file)
{
  cerr << "!!! Configuration File in TXT is DEPRECATED !!!" << endl;
  paramFileStream.open(file, ifstream::in);
}

ConfigTXT::~ConfigTXT()
{
  paramFileStream.close();
}

char * ConfigTXT::getValue(MEMM_parameters_t valueName)
{
  char line[512];
  char * temp = NULL;

  switch(valueName) {
    case MEMM_PARENTS_FILE_NAME:
      goToLine(1);
      paramFileStream.getline(line,512);
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_PARENTS_FILE_NUMBER_OF_LOCUS:
      goToLine(3);
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_PARENTS_FILE_CLASS_COV:
      goToLine(3);
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_PARENTS_FILE_QT_COV:
      goToLine(3);
      paramFileStream.ignore(512,' ');
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_PARENTS_FILE_WEIGHT_VAR:
      goToLine(3);
      paramFileStream.ignore(512,' ');
      paramFileStream.ignore(512,' ');
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512);
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_OFFSPRING_FILE_NAME:
      goToLine(2);
      paramFileStream.getline(line,512);   
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_OFFSPRING_FILE_NUMBERS_OF_LOCUS:
      goToLine(3);
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_AF_FILE_MODE:
      goToLine(4);
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_AF_FILE_NAME:
      goToLine(4);
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512);
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

/*    case MEMM_IND_FEC_DIST:
      goToLine(5);
      paramFileStream.getline(line,512);
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;
*/
    case MEMM_SEED:
      goToLine(6);
      paramFileStream.getline(line,512);
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;
/*
    case MEMM_GAMA_INIT:
      goToLine(7);
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_GAMA_MIN:
      goToLine(7);
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_GAMA_MAX:
      goToLine(7);
      paramFileStream.ignore(512,' ');
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512);
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_DELTA_INIT:
      goToLine(8);
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_DELTA_MIN:
      goToLine(8);
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_DELTA_MAX:
      goToLine(8);
      paramFileStream.ignore(512,' ');
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512);
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;
 
    case MEMM_SHAPE_B_INIT:
      goToLine(9);
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_SHAPE_B_MIN:
      goToLine(9);
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_SHAPE_B_MAX:
      goToLine(9);
      paramFileStream.ignore(512,' ');
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512);
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;
 
    case MEMM_MIG_RATE_M_INIT:
      goToLine(10);
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_MIG_RATE_M_MIN:
      goToLine(10);
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_MIG_RATE_M_MAX:
      goToLine(10);
      paramFileStream.ignore(512,' ');
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512);
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_SELF_RATE_S_INIT:
      goToLine(11);
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_SELF_RATE_S_MIN:
      goToLine(11);
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_SELF_RATE_S_MAX:
      goToLine(11);
      paramFileStream.ignore(512,' ');
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512);
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;
*/
    case MEMM_BURNIN:
      goToLine(12);
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_ITE:
      goToLine(12);
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512,' ');
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_THIN:
      goToLine(12);
      paramFileStream.ignore(512,' ');
      paramFileStream.ignore(512,' ');
      paramFileStream.getline(line,512);
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_GAMA_FILE_NAME:
      goToLine(13);
      paramFileStream.getline(line,512);
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_IND_FEC_FILE_NAME:
      goToLine(14);
      paramFileStream.getline(line,512);
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    case MEMM_DISP_FILE_NAME:
      goToLine(15);
      paramFileStream.getline(line,512);
      temp = new char[strlen(line)+1];
      strcpy(temp,line);
      break;

    default:
      cerr<<"Error reading "<<valueName<<" value"<<endl;
      cerr<<"ERROR : invalid Configuration "<<MEMM_parameters_t{valueName}<<endl;
      temp = new char[1];
      temp[0]='\0';
      break;
  }

  return temp;
}

bool ConfigTXT::setValue(MEMM_parameters_t valueName, char * value)
{
  cerr<<"SetValue not implemented yet"<<endl;

}


/********************* PRIVATE ***************************/
void ConfigTXT::goToLine(int lineNumber)
{
  paramFileStream.seekg(ios::beg);
  for(int i=0; i < lineNumber - 1; ++i)
  {
    paramFileStream.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
  }
}
