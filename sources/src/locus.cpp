/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "locus.h"

/*!
 * \file locus.cpp
 * \brief locus class
 * \author Etienne Klein
 * \author Jean-Francois Rey
 * \version 1.5
 * \date 21 Aril 2015
 */

locus::locus(int all1, int all2)
{
    _allele[0]=all1;
    _allele[1]=all2;
}

locus::~locus() {}

void locus::afficher() const
{
    std::cout << "{" << _allele[0] << " , " << _allele[1] << "}, ";
}

bool locus::operator== (const locus & other) const
{
    bool temp1, temp2, temp3, temp4;
    temp1=((this->_allele[1] == other._allele[1] && this->_allele[2] == other._allele[2]) || (this->_allele[1] == other._allele[2] && this->_allele[2] == other._allele[1]));
    temp2= (this->_allele[1] <= 0);
    //temp3 = (this->_allele[2] == other._allele[1]);
    //temp4 = (this->_allele[2] == other._allele[2]);

    return temp1 || temp2;
}

/*
double locus::mendel1 (const locus & mere , const locus & pere, int na,  std::map<int,int> sizeall, const std::vector<double> & freqall) const {
    std::vector<double> tempm (na);
    std::vector<double> tempp (na);
    double temp;

//    std::cout << "{ " << this->_allele[0] << " , " << this->_allele[1] << " }  vs. { ";
//   std::cout << mere._allele[0] << " , " << mere._allele[1] << " } x { ";
//    std::cout << pere._allele[0] << " , " << pere._allele[1] << " } = ";

    for (int i=0; i<2; i++) {
        if (mere._allele[i]<0) {
            for (int j=0;j<na; j++) {tempm[j]+=freqall[j]/2;}}
        else {tempm[sizeall[mere._allele[i]]]+=0.5;}
        if (pere._allele[i]<0) {
            for (int j=0;j<na; j++) {tempp[j]+=freqall[j]/2;}}
        else {tempp[sizeall[pere._allele[i]]]+=0.5;}}

    if (this->_allele[0] < 0) {
        if (this->_allele[1] < 0) {temp=1;}
        else {temp=tempp[sizeall[this->_allele[1]]]/2+tempm[sizeall[this->_allele[1]]]/2;}}
    else {
        if (this->_allele[1] < 0) {temp=tempp[sizeall[this->_allele[0]]]/2+tempm[sizeall[this->_allele[0]]]/2;}
        else {
            if (this->_allele[0]==this->_allele[1]) {temp=tempm[sizeall[this->_allele[0]]]*tempp[sizeall[this->_allele[0]]];}
            else {temp=tempm[sizeall[this->_allele[0]]]*tempp[sizeall[this->_allele[1]]]+tempm[sizeall[this->_allele[1]]]*tempp[sizeall[this->_allele[0]]];}}}

//    std::cout << temp << std::endl;
    return(temp);

}*/


double locus::mendel1(const locus & mere , const locus & pere, int na,  std::map<int,int> sizeall, const std::vector<double> & freqall, const std::vector< std::vector <double> > & materror) const
{
    std::vector<double> tempm (na);
    std::vector<double> tempp (na);
    std::vector<double> tempm2 (na);    // NEW EK
    std::vector<double> tempp2 (na);    // NEW EK
    double temp;
    
    //    std::cout << "{ " << this->_allele[0] << " , " << this->_allele[1] << " }  vs. { ";
    //   std::cout << mere._allele[0] << " , " << mere._allele[1] << " } x { ";
    //    std::cout << pere._allele[0] << " , " << pere._allele[1] << " } = ";
    
    for (int i=0; i<2; i++) {
        if (mere._allele[i]<0) {
            for (int j=0;j<na; j++) {tempm[j]+=freqall[j]/2;}}
        else {tempm[sizeall[mere._allele[i]]]+=0.5;}
        if (pere._allele[i]<0) {
            for (int j=0;j<na; j++) {tempp[j]+=freqall[j]/2;}}
        else {tempp[sizeall[pere._allele[i]]]+=0.5;}}
    
    if(!materror.empty())
    {
        for(int j=0; j<na; j++) {
            tempm2[j]=0;
            tempp2[j]=0;
            for(int k=0; k<na; k++) {
                tempm2[j]+= materror[j][k]*tempm[k];
                tempp2[j]+= materror[j][k]*tempp[k];
            }
        }
          tempm=tempm2;
          tempp=tempp2;
    }
        
    if (this->_allele[0] < 0) {
        if (this->_allele[1] < 0) {temp=1;}
        else {temp=tempp[sizeall[this->_allele[1]]]/2+tempm[sizeall[this->_allele[1]]]/2;}}
    else {
        if (this->_allele[1] < 0) {temp=tempp[sizeall[this->_allele[0]]]/2+tempm[sizeall[this->_allele[0]]]/2;}
        else {
            if (this->_allele[0]==this->_allele[1]) {temp=tempm[sizeall[this->_allele[0]]]*tempp[sizeall[this->_allele[0]]];}
            else {temp=tempm[sizeall[this->_allele[0]]]*tempp[sizeall[this->_allele[1]]]+tempm[sizeall[this->_allele[1]]]*tempp[sizeall[this->_allele[0]]];}}}
    
    //    std::cout << temp << std::endl;
    return(temp);
    
}

