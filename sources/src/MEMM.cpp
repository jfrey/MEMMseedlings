/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/*!
 * \file MEMM.cpp
 * \brief MEMM abstract class implementation
 * \author Jean-Francois REY
 * \version 1.1
 */


#include "MEMM.hpp"

MEMM::MEMM() : dist_norm(0,1.0), unif(0,1.0), generator(static_cast<unsigned int>(std::time(0))), gauss(generator,dist_norm), accept(generator,unif)
{
  NPar = 0;
  Ns = 0;
  ParamFec = IndivFec = ParamDisp = NULL;
  pLoi = NULL;
  useMatError = false;
  dynamic_ = false;
}

MEMM::~MEMM()
{
  if(ParamFec)
  {
    ParamFec->close();
    delete ParamFec;
  }
  if(IndivFec)
  {
    IndivFec->close();
    delete IndivFec;
  }
  if(ParamDisp)
  {
    ParamDisp->close();
    delete ParamDisp;
  }
  if(pLoi) delete pLoi;

  fatherID.clear();

  allParameters.clear();

}

void MEMM::init(Configuration * p)
{

  if(p == NULL) return;

  if( strcmp(p->getValue(MEMM_TYPE),"dynamic") == 0)
  {
    dynamic_=true;
    cout << "Dynamic ";
  }

  // Load parents from file and fatherID
  loadParentFile(p);

  // Load seeds 
  loadSeeds(p);

  // Calcul distances
  calculDistances(p);

  // load allelic frequencies
  loadAllelicFreq(p);

  // calcul transition matrix
  calculTransitionMatrix(p);  

  calculWeightPollenDonor();

  loadParameters(p);
  loadOptions(p);

  for (int k=0; k<NPar; k++) {delete allParents[k];}
  for (int k=0; k<Ns; k++) {delete allSeeds[k];}
}

void MEMM::burnin()
{
    cout << "Burn in "<<Nburn<<" iterations ..." <<endl;
    if(dynamic_ && Nburn) mcmc_dyn(Nburn,0);
    if(!dynamic_ && Nburn) mcmc(Nburn,0);
    if(Nburn) mcmc(Nburn,0);
    cout << "Burn in iterations : OK" <<endl<<endl;

}

void MEMM::run()
{
    cout << "Run " << Nstep <<" iterations" << endl;
    if(dynamic_ && Nstep) mcmc_dyn(Nstep,Nthin);
    if(!dynamic_ && Nstep) mcmc(Nstep,Nthin);
    cout << "MCMC finished " << Nstep << " iterations"
    << endl << "You can visualize the results from R"<<endl;
}



/***************************** PROTECTED ***********************************/

void MEMM::loadParentFile(Configuration * p)
{
  //////////////////////////////////
  //    LOADING PARENTS           //
  //////////////////////////////////

  string name;
  char * temp;

  if(p == NULL) return;

  if(allParents.size() != 0) allParents.clear();

  cout<<endl<<"# Loading parents..."<<endl;
  temp = p->getValue(MEMM_PARENTS_FILE_NAME);
  ifstream parents(temp);
  if(temp) delete []temp;
  temp = p->getValue(MEMM_PARENTS_FILE_NUMBER_OF_LOCUS);
  Nl = atoi(temp);
  if(temp) delete []temp;
  temp = p->getValue(MEMM_PARENTS_FILE_CLASS_COV);
  Nql = atoi(temp);
  if(temp) delete []temp;
  temp = p->getValue(MEMM_PARENTS_FILE_QT_COV);
  Nqt = atoi(temp);
  if(temp) delete []temp;
  temp = p->getValue(MEMM_PARENTS_FILE_WEIGHT_VAR);
  Nw = atoi(temp);
  if(temp) delete []temp;

  NPar = 0;
  if(parents.good())
  {
    while(parents >> name)
    {
      cout<<name<<"...";
      allParents.push_back(new parent(name,parents,Nql,Nqt,Nl));
      NPar++;
    }
    for(int k=0; k<NPar; k++)
    {
      fatherID[(*allParents[k]).getName()]=k;
    }
    cout<<endl<< NPar << " parents loaded. " << std::endl;
  }
  else
  {
    cerr<<"#ERROR ! Can not read parents file !"<<endl<<"Continue..."<<endl;
  }
  parents.close();

}

void MEMM::loadSeeds(Configuration * p)
{

  int Nsl;
  string name;
  char * temp, *filename;

  if(p == NULL)return;

  cout<<endl<<"# Loading seeds..."<<endl;
  if(allSeeds.size() != 0) allSeeds.clear();

  filename = p->getValue(MEMM_OFFSPRING_FILE_NAME);
  ifstream seeds(filename);
  temp = p->getValue(MEMM_OFFSPRING_FILE_NUMBERS_OF_LOCUS);
  Nsl = atoi(temp);
  if(temp) delete []temp;

  Ns = 0;
  if(seeds.good())
  {
    while(seeds >> name)
    {
      cout<<name<<"...";
      allSeeds.push_back(new graine(name,seeds,Nsl));
      Ns++;
    }
    cout<<endl<<Ns<<" seeds loaded."<<endl;
    seeds.close();
  }
  else
  {
    cerr<<"#ERROR ! Can not read "<<filename<<" seeds file !"<<endl<<"Continue..."<<endl;
  }
  if(filename) delete []filename;

}

void MEMM::calculDistances(Configuration *p)
{
  char * temp;
  string temp1,temp2;
  //cerr<<"# Call of Default function MEMM::CalculDistances()\n  this fuction is not implemented"<<endl;
  if(allParents.size() != NPar || NPar == 0)
  {
    cerr<<"# ERROR : CalculDistance, number of parents incorrect"<<endl;
    return;
  }

  for(int i=DistMP.size()-1; i>=0 ; i--) DistMP[i].clear();
  DistMP.clear();
  DistMP.resize(NPar,vector<double>(NPar,0.));

  temp = p->getValue(MEMM_DIST_FILE_MODE);
  if(strcmp(temp,"file") == 0)
  {
    if(temp) delete []temp;
    temp = p->getValue(MEMM_DIST_FILE_NAME);
    ifstream distances(temp);

    if(distances.good())
    {
      std::cout << "Loading distances from external file "<<temp<<endl;

      while (distances >> temp1)
      {
        distances >> temp2;
        if(fatherID.count(temp1)>0 && fatherID.count(temp2)>0)
        {
          distances >> DistMP[fatherID[temp1]][fatherID[temp2]];
          std::cout << temp1 << " x " << temp2 << " -> " << DistMP[fatherID[temp1]][fatherID[temp2]] << std::endl;
        }
        else
        {
          cerr<<"Unknow "<<temp1<< " and/or "<<temp2<<" individu name"<<endl;
          distances >> temp1;
        }
      }
      std::cout << std::endl << "Loading distances : OK. " << std::endl << std::endl;
    }
    else cerr<<"ERROR: can't open "<<temp<<" distances file"<<endl;
    if(temp) delete []temp;
  }
  else
  {
    if(temp) delete []temp;
    cout << endl << "# Computing distances from x-y coordinates of parents...";
    for (int m=0; m<NPar; m++)
      for (int p=0; p<NPar; p++)
        DistMP[m][p]=(*allParents[m]).dist(*allParents[p]);

    cout<<endl << "Computing distances : OK. " << endl;
  }
}

void MEMM::loadAllelicFreq(Configuration * p)
{

  bool readfile = false;
  char * temp;
  int ntemp;
  double floattemp, stemp;//,a,ab;
  vector< std::vector<int> > AllSize (Nl);
  //vector< std::map<int,double> > SizeFreq (Nl);
    
    
  Na.clear();
  Na.assign(Nl, 0);

  for(int i=AllFreq.size()-1; i>=0 ; i--) AllFreq[i].clear();
  AllFreq.clear();
  AllFreq.resize(Nl);

  for(int i=SizeAll.size()-1; i>=0 ; i--) SizeAll[i].clear();
  SizeAll.clear();
  SizeAll.resize(Nl);

  temp = p->getValue(MEMM_AF_FILE_MODE);
  if( strcmp("file",temp) == 0)
  {
    delete []temp;
    temp = p->getValue(MEMM_AF_FILE_NAME);
    ifstream fall(temp);
    if(fall.good())
    {
      cout << endl<<"# Loading allelic frequencies from file " <<temp<<" ..."<<endl;
      if(temp) delete []temp;
      temp = NULL;
      for (int l=0; l<Nl; l++)
      {
        fall >> Na[l];
        stemp=0;
        for (int a=0; a<Na[l]; a++)
        {
          fall >> ntemp >> floattemp;
          stemp+=floattemp;
          AllSize[l].push_back(ntemp);
          AllFreq[l].push_back(floattemp);
          SizeAll[l][ntemp]=a;
        }
        cout << "Locus " << l << " : " << Na[l] << " alleles" <<endl;
        for (int a=0; a<Na[l]; a++)
        {
          AllFreq[l][a] /= stemp;
          //SizeFreq[l][AllSize[l][a]]=AllFreq[l][a];
          std::cout << AllSize[l][a] << ":" << AllFreq[l][a] << " | ";
        }
        cout << endl;
      }
      cout << "Loading of allelic frequencies : OK. " <<endl;
      fall.close();
      readfile = true;
    }
    else{
      readfile = false;
      cerr<<"ERROR can't read allelic frequencies from "<<temp<<endl;
    }
  }
  
  if(!readfile)
  {
    if(temp) delete []temp;
    temp = NULL;
    int all;
    std::cout << endl << "# Computing allelic frequencies from adult trees..." << std::endl;
    for (int l=0; l<Nl; l++)
    {
      ntemp=0;
      for (int k=0; k<NPar; k++)
      {
        for (int m=0; m<2; m++)
        {
          all = allParents[k]->getGeno(l)->getAllele(m);
          if (all>-1)
          {
            if ( (SizeAll[l].find(all)) == SizeAll[l].end() )
            {
              Na[l]++;
              ntemp++;
              std::cout << Na[l] << "...";
              AllSize[l].push_back(all);
              AllFreq[l].push_back(1.);
              SizeAll[l][all]=Na[l]-1;
            }
            else {
              ntemp++;
              AllFreq[l][SizeAll[l][all]]++;
            }
          }
        }
      }
      std::cout << "Locus " << l << " : " << Na[l] << " alleles (" << ntemp << ")" <<std::endl;

      for (int a=0; a<Na[l]; a++)
      {
        AllFreq[l][a] /= ntemp;
        //SizeFreq[l][AllSize[l][a]]=AllFreq[l][a];
        cout << AllSize[l][a] << ":" << AllFreq[l][a] << " | ";
      }
      cout << endl;
    }
    std::cout << "Estimation of allelic frequencies : OK. " <<std::endl << std::endl;
  }

  if(temp) delete []temp;

  loadMatrixError(p,AllSize);

}

void MEMM::loadMatrixError(Configuration *p, vector< vector<int> > & AllSize)
{
  char * temp;
  vector<double> motif(Nl);
  vector<double> eps0(Nl);
  vector<double> epsInf(Nl);

  temp = p->getValue(MEMM_LOCUS_ERROR);
  if(temp && strcmp("",temp)!=0)
  {
    std::cout<<"# Will use Locus error matrix"<<std::endl<<std::endl;
    delete []temp;
    temp = p->getValue(MEMM_LOCUS_ERROR_FILE);
    ifstream inputLocusError(temp);

    if(inputLocusError.good())
    {
      cout << endl<<"# Loading motif, eps0 and epsInf from file " <<temp<<" ..."<<endl;
      useMatError = true;
      int l;
      for(l=0; l<Nl && inputLocusError.good() ; l++)
      {
        inputLocusError >> motif[l];
        inputLocusError >> eps0[l];
        inputLocusError >> epsInf[l];
      }
      if(l != Nl && l>0)
        for(l=1; l<Nl ; l++)
        {
          motif[l] =motif[0];
          eps0[l] = eps0[0];
          epsInf[l] = epsInf[0];
        }
      inputLocusError.close();

      MatError.resize(Nl);
      for (int l=0; l<Nl; l++)
      {
        MatError[l].resize(Na[l]);
        for (int k=0; k<Na[l]; k++){ MatError[l][k].resize(Na[l]); }
        for (int j=0; j<Na[l]; j++)
        {
          for (int k=0; k<Na[l]; k++)
          {
            if( abs(AllSize[l][j]-AllSize[l][k])==motif[l] ){ MatError[l][k][j]=eps0[l]/2+epsInf[l]/(Na[l]-1); }
              else if( AllSize[l][j]==AllSize[l][k] ){ MatError[l][k][j]=1-eps0[l]-epsInf[l]; }
              else{ MatError[l][k][j]=epsInf[l]/(Na[l]-1); }
          }
        }
      }
      motif.clear();
      eps0.clear();
      epsInf.clear();
    }
    else{
      std::cerr<<"ERROR: can't open File "<<temp<<std::endl;
      useMatError = false;
    }
  }
  else useMatError = false;

  if(temp) delete []temp;
}

void MEMM::calculTransitionMatrix(Configuration *p)
{

  cerr<<endl<<"# Call of Default function MEMM::CalculTransitionMatrix()\n  this fuction is not implemented"<<endl;

}

void MEMM::loadParameters(Configuration *p)
{
  Parameter * param_temp;

  cout << endl << "Reading Parameters ..."<<endl;
  
  // free loadParameters() return after ?
  allParameters = p->loadParameters();

  for(unordered_map<string, Parameter *>::iterator it = allParameters.begin() ; it != allParameters.end(); ++it)
  {
    cout << *it->second;
  }

  // gama
  param_temp = allParameters["gama"];
  if(param_temp)
  {
    GamA = param_temp->INIT;
    mGamA = param_temp->MIN;
    MGamA = param_temp->MAX;
    cout << "GamA : "<< GamA << " [" <<mGamA <<"|"<< MGamA << "]"<<endl;
  }
    
  // pi0
  param_temp = allParameters["Pi0"];
  if(param_temp)
  {
    Pi0 = param_temp->INIT;
    mPi0 = param_temp->MIN;
    MPi0 = param_temp->MAX;
    cout << "Pi0 : "<< Pi0 << " [" << mPi0 <<"|"<< MPi0 << "]"<<endl;

  }
    
  // Scale
  param_temp = allParameters["scale"];
  if(param_temp)
  {
    Scale = param_temp->INIT;
    mScale = param_temp->MIN;
    MScale = param_temp->MAX;
    cout << "Scale : "<< Scale << " [" <<mScale <<"|"<< MScale << "]"<<endl;
  } 
  
  // Shape
  param_temp = allParameters["shape"];
  if(param_temp)
  {
    Shape = param_temp->INIT;
    mShape = param_temp->MIN;
    MShape = param_temp->MAX;
    cout << "Shape : "<< Shape << " [" <<mShape <<"|"<< MShape << "]"<<endl;
  }

  // Mig
  param_temp = allParameters["mig"];
  if(param_temp)
  {
    Mig = param_temp->INIT;
    mMig = param_temp->MIN;
    MMig = param_temp->MAX;
    cout << "Mig : "<< Mig << " [" <<mMig <<"|"<< MMig << "]"<<endl;

  } 
  
  // Self
  param_temp = allParameters["self"];
  if(param_temp)
  {
    Self = param_temp->INIT;
    mSelf = param_temp->MIN;
    MSelf = param_temp->MAX;
    cout << "Self : "<< Self << " [" <<mSelf <<"|"<< MSelf << "]"<<endl;
  }

  pLoi = loadDistribution(p, "individual_fecundities");
  
  cout<<endl<<"...end reading parameters"<<endl;
}


void MEMM::loadOptions(Configuration *p)
{
  char * temp;
  cout << endl << "Load options..."<<endl;

  temp = p->getValue(MEMM_SEED);
  if(atoi(temp) >= 0)
  {
    cout << "New seed : "<<temp<<endl;
    generator.seed(static_cast<long unsigned int>(atoi(temp)));
  }
  else {
      cout << "New seed : generated from time "<<endl;
      // by default
      //generator.seed(static_cast<long unsigned int>(time(NULL)));
  }

  if(temp) delete []temp;

  temp = p->getValue(MEMM_BURNIN);
  Nburn = atoi(temp);
  if(temp) delete []temp;
  temp = p->getValue(MEMM_ITE);
  Nstep = atoi(temp);
  if(temp) delete []temp;
  temp = p->getValue(MEMM_THIN);
  Nthin = atoi(temp);
  if(temp) delete []temp;
  cout<<endl<< "Burnin "<< Nburn << " | Iteration "<<Nstep<<" | Thin "<<Nthin<<endl;
  

  cout<<"Ouput files :"<<endl;
  temp = p->getValue(MEMM_GAMA_FILE_NAME);
  ParamFec = new std::ofstream(temp, ofstream::out);
  if(ParamFec->good()) cout<<"\t-"<<temp<<endl;
  else cerr<<"#ERROR openning "<<temp<<" file"<<endl;
  if(temp) delete []temp;

  temp = p->getValue(MEMM_IND_FEC_FILE_NAME);
  IndivFec = new std::ofstream(temp, ofstream::out);
  if(IndivFec->good()) cout<<"\t-"<<temp<<endl;
  else cerr<<"#ERROR openning "<<temp<<" file"<<endl;
  if(temp) delete []temp;

  temp = p->getValue(MEMM_DISP_FILE_NAME);
  ParamDisp = new std::ofstream(temp, ofstream::out);
  if(ParamDisp->good()) cout<<"\t-"<<temp<<endl;
  else cerr<<"#ERROR openning "<<temp<<" file"<<endl;
  if(temp) delete []temp;

  cout << "... end reading default options"<<endl;
}

void MEMM::calculWeightPollenDonor()
{

  Poids.resize( NPar, 1.0 );

  cout <<endl << "Calculing pollen donor weight..."<<endl;
  for (int p=0; p<NPar; p++)
  {
    for (int k=0; k<Nw; k++)
      Poids[p]*=allParents[p]->getCoVarQuanti(k);
    cout << Poids[p] << " | ";
  }
  cout<<endl;
}

/***************************** PRIVATE ***********************************/

MEMM_loi * MEMM::loadDistribution(Configuration *config, string name)

{

  MEMM_loi * law = NULL;
  string type;
  map<string,string> list_params;

  type = config->getDistributionType(name);
  list_params = config->getDistributionParameters(name);

  if(allParameters.empty() || type == "")
  {
    cerr << "ERROR : need to load Parameters before loading distribution law or distribution "<< name << "not found" << endl;
    return NULL;
  }
  else
  {
    if( type == MEMM_loi::LAWS_NAMES[MEMM_IDLOI_LN]) law = new MEMM_logNormal(gauss);
    else
      if( type == MEMM_loi::LAWS_NAMES[MEMM_IDLOI_GAMMA]) law = new MEMM_gamma( allParameters[list_params[MEMM_loi::LAWS_PARAMETERS[MEMM_IDLOI_GAMMA][0]]]->INIT,accept);
      else
        if( type == MEMM_loi::LAWS_NAMES[MEMM_IDLOI_ZIGAMMA]) law = new MEMM_zigamma(allParameters[list_params[MEMM_loi::LAWS_PARAMETERS[MEMM_IDLOI_ZIGAMMA][0]]]->INIT, allParameters[list_params[MEMM_loi::LAWS_PARAMETERS[MEMM_IDLOI_ZIGAMMA][1]]]->INIT, accept);
        else
          if( type == MEMM_loi::LAWS_NAMES[MEMM_IDLOI_BETA] ) law = new MEMM_beta(allParameters[list_params[MEMM_loi::LAWS_PARAMETERS[MEMM_IDLOI_BETA][0]]]->INIT,allParameters[list_params[MEMM_loi::LAWS_PARAMETERS[MEMM_IDLOI_BETA][1]]]->INIT,accept);
          else law = new MEMM_logNormal(gauss);
  }

  return law;

}
