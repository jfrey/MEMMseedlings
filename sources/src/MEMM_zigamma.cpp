/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include <iostream>
#include <string>
#include "MEMM_util.h"
#include "MEMM_zigamma.h"

MEMM_zigamma::MEMM_zigamma(double& pi0, double& gamA,
		        boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& accept):
  _pi0(pi0),_gamA(gamA),_accept(accept),_gamma(1./(gamA*(1-pi0)-pi0),gamA-pi0/(1-pi0))
{
  _IDLoi=MEMM_IDLOI_ZIGAMMA;
    std::cout<<"Using Zero-Inflated gamma law"<<std::endl;
}



MEMM_zigamma::~MEMM_zigamma()
{
    //dtor
}

void MEMM_zigamma::tirage(double& v){
    if (_accept() < _pi0) v=0;
        else v= quantile(_gamma,_accept());
}
double MEMM_zigamma::logLik(int npar, const std::vector<double> & fec, double& pi, double& A) {

  // Parametrage de la loi gamma en pi0, k, theta
  // Esperance = (1-pi0) k theta = 1;
  // Variance = Esp*(theta + Esp*pi0/(1-pi0) ) = A
    // theta = A - pi0/(1-pi0)
    // k = 1/(A*(1-pi0)-pi0)

  double liktemp=0, a1, a2, a3, a4;
    
    a1=1/(A-pi/(1-pi));
    a2= 1./(A*(1-pi)-pi) - 1.;
    a3=log(A-pi/(1-pi))/(A*(1-pi)-pi);
    a4=gammln(1./(A*(1-pi)-pi));
    
    //std::cout << "clacul de zigamma" << pi << " " << A << " " << a1 << " " << a2 << " " << a3 << " " << a4 << " ";
    
  for (int p=0; p<npar; p++) {
      if (fec[p] <= 0.) liktemp += log(pi);
      else liktemp += log(1-pi) - fec[p]*a1 + log(fec[p])*a2 - a3 - a4;
  }
    //std::cout << liktemp << std::endl;
    //std::cout.flush();
  return liktemp;
}


void MEMM_zigamma::setDParam(unsigned int indiceParam, double & value){
  switch (indiceParam)
    {
    case MEMM_LOI_GAMA:
            _gamA=value;
      _gamma = boost::math::gamma_distribution<>(1./(_gamA*(1-_pi0)-_pi0),_gamA-_pi0/(1-_pi0));
      break;
    case MEMM_LOI_ZIPROB:
        _pi0 = value;
        _gamma = boost::math::gamma_distribution<>(1./(_gamA*(1-_pi0)-_pi0),_gamA-_pi0/(1-_pi0));
        break;
    default:
      printf("Error: MEMM_zigamma indiceParam out of range \n");
    }
}
