/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "MEMM_seedlings_2kernels.hpp"

MEMM_seedlings_2kernels::MEMM_seedlings_2kernels()
{
  cout<< "MEMM : Seedlings_2kernels mode"<<endl;
}

MEMM_seedlings_2kernels::~MEMM_seedlings_2kernels()
{
  if(pLois) delete pLois;
}

void MEMM_seedlings_2kernels::mcmc(int nbIteration, int thinStep)
{

  double lastFecLogLiks, nextFecLogLiks;
  double lastFecLogLikp, nextFecLogLikp;
    double lastFreqLDDLogLik, nextFreqLDDLogLik;
  double previousValue, nextValue;
  double previousValue2;
  double previousLogLik, nextLogLik;

  int xpourCent = nbIteration/20;
  if(!xpourCent) xpourCent=1;

    if ( (pLoi->_IDLoi == MEMM_IDLOI_GAMMA) || (pLoi->_IDLoi == MEMM_IDLOI_LN) ) {
    lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp,GamA);
        lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs,GamAs);}
  else if (pLoi->_IDLoi == MEMM_IDLOI_ZIGAMMA) {
    lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, Pi0, GamA);
    lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs, Pi0s, GamAs);
      cout << " vraisemblance de la zigamma :" << lastFecLogLiks << " " << Pi0s << " " << GamAs << " " << lastFecLogLikp << " " << Pi0 << " " << GamA << endl;
    }
  else std::cerr << " Distribution of individual fecundities not implemented ";
      
   // lastFreqLDDLogLik = pLoiLDD->logLik(NPar,Vec_FreqLDD,AlphaLDD,BetaLDD); //New
    previousLogLik = logLik();  //New

  //output start value
  if(thinStep){

      if ( (pLoi->_IDLoi == MEMM_IDLOI_GAMMA) || (pLoi->_IDLoi == MEMM_IDLOI_LN) )
           *ParamFec << "0 " << lastFecLogLiks << " " << GamAs << " " << lastFecLogLikp << " " << GamA << " " << lastFreqLDDLogLik << " " << AlphaLDD << " " << BetaLDD << endl;
    else if (pLoi->_IDLoi == MEMM_IDLOI_ZIGAMMA)
        *ParamFec << "0 " << lastFecLogLiks << " " << GamAs << " " << Pi0s << " "<< lastFecLogLikp << " " << GamA << " " << Pi0 << " " << " " << lastFreqLDDLogLik << " " << AlphaLDD << " " << BetaLDD << endl;

      ParamFec->flush();
    *IndivFec << "0 " ;
    for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecs[p] << " ";}
    for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecp[p] << " ";}
    for (int p=0; p<NPar; p++) {*IndivFec << Vec_FreqLDD[p] << " ";}
    *IndivFec << endl;
    IndivFec->flush();
    *ParamDisp << "0 " << previousLogLik
      << " " << Scales
      << " " << Shapes
      << " " << ScalesLDD
      << " " << ShapesLDD
      << " " << Migs
      << " " << Scale
      << " " << Shape
      << " " << Mig
      << " " << Self
      <<endl;
    ParamDisp->flush();

  }

  if(nbIteration) cout << "it=1..."<< std::endl;
  for(int ite=1; ite<=nbIteration; ite++)
  {
    if (ite % xpourCent == 0) {cout << "it="<<ite << "..."<< std::endl;}

    //GamAs- fecundities distribution seeds
    nextValue = pow(GamAs, exp(0.2*gauss()));
    if( nextValue>mGamAs && nextValue<MGamAs )
    {
        if ( (pLoi->_IDLoi == MEMM_IDLOI_GAMMA) || (pLoi->_IDLoi == MEMM_IDLOI_LN) )
        {lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs, GamAs);
            nextFecLogLiks = pLois->logLik(NPar, Vec_Fecs, nextValue);}
        else if (pLoi->_IDLoi == MEMM_IDLOI_ZIGAMMA)
        {lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs, Pi0s, GamAs);
        nextFecLogLiks = pLois->logLik(NPar, Vec_Fecs, Pi0s, nextValue);}
        
    if( accept() < exp(nextFecLogLiks-lastFecLogLiks) )
      {
        GamAs = nextValue;
        lastFecLogLiks = nextFecLogLiks;
        pLois->setDParam(MEMM_LOI_GAMA,GamAs);
      }
    }
    if(thinStep)*ParamFec << ite << " " << lastFecLogLiks << " " << GamAs << " ";

      //pi0s - fecundities distribution seeds ZI parameter
      if ( (pLoi->_IDLoi == MEMM_IDLOI_ZIGAMMA) )
      {nextValue = pow(Pi0s, exp(0.2*gauss()));
      if( nextValue>mPi0s && nextValue<MPi0s )
      {
          lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs, Pi0s, GamAs);
          nextFecLogLiks = pLois->logLik(NPar, Vec_Fecs, nextValue, GamAs);
          
          if( accept() < exp(nextFecLogLiks-lastFecLogLiks) )
          {
              Pi0s = nextValue;
              lastFecLogLiks = nextFecLogLiks;
              pLois->setDParam(MEMM_LOI_ZIPROB,Pi0s);
          }
      }
      if(thinStep)*ParamFec << " " << Pi0s << " ";}
      
    //GamA (GamAp)- fecundities distribution pollen
    nextValue = pow(GamA, exp(0.2*gauss()));
    if( nextValue>mGamA && nextValue<MGamA )
    {
        if ( (pLoi->_IDLoi == MEMM_IDLOI_GAMMA) || (pLoi->_IDLoi == MEMM_IDLOI_LN) )
        {lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp,GamA);
            nextFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, nextValue);}
        else if (pLoi->_IDLoi == MEMM_IDLOI_ZIGAMMA)
        {lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, Pi0, GamA);
        nextFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, Pi0, nextValue);}

        if( accept() < exp(nextFecLogLikp-lastFecLogLikp) )
      {
        GamA = nextValue;
        lastFecLogLikp = nextFecLogLikp;
        pLoi->setDParam(MEMM_LOI_GAMA,GamA);
      }
    }
    if(thinStep)*ParamFec << lastFecLogLikp << " " << GamA << " ";

      //pi0 - fecundities distribution pollen ZI parameter
      if ( (pLoi->_IDLoi == MEMM_IDLOI_ZIGAMMA) )
            {nextValue = pow(Pi0, exp(0.2*gauss()));
            if( nextValue>mPi0 && nextValue<MPi0 )
            {
                lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, Pi0, GamA);
                nextFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, nextValue, GamA);
      
                if( accept() < exp(nextFecLogLikp-lastFecLogLikp) )
                {
                    Pi0 = nextValue;
                    lastFecLogLikp = nextFecLogLikp;
                    pLoi->setDParam(MEMM_LOI_ZIPROB,Pi0);
                }
            }
            if(thinStep)*ParamFec << " " << Pi0 << " ";}

      
    //AlphaLDD- FreqLDD distribution parameter#1
    lastFreqLDDLogLik = pLoiLDD->logLik(NPar, Vec_FreqLDD,AlphaLDD,BetaLDD);
    nextValue = pow(AlphaLDD, exp(0.2*gauss()));
      if( nextValue > mAlphaLDD && nextValue<MAlphaLDD )
      {
          nextFreqLDDLogLik = pLoiLDD->logLik(NPar, Vec_FreqLDD,nextValue,BetaLDD);
          if( accept() < exp(nextFreqLDDLogLik-lastFreqLDDLogLik) )
          {
              AlphaLDD = nextValue;
              lastFreqLDDLogLik = nextFreqLDDLogLik;
              pLoiLDD->setDParam(MEMM_LOI_ALPHA,AlphaLDD);
          }
      }

    //BetaLDD- FreqLDD distribution parameter#2
  
    // Début ajout

    nextValue = pow(BetaLDD, exp(0.2*gauss()));
      if( nextValue > mBetaLDD && nextValue<MBetaLDD )
      {
          //lastFreqLDDLogLik = pLoiLDD->logLik(NPar, Vec_FreqLDD,AlphaLDD,BetaLDD);
          nextFreqLDDLogLik = pLoiLDD->logLik(NPar, Vec_FreqLDD,AlphaLDD,nextValue);
          if( accept() < exp(nextFreqLDDLogLik-lastFreqLDDLogLik) )
          {
              BetaLDD = nextValue;
              lastFreqLDDLogLik = nextFreqLDDLogLik;
              pLoiLDD->setDParam(MEMM_LOI_BETA,BetaLDD);
          }
      }
      if(thinStep)*ParamFec << lastFreqLDDLogLik << " " << AlphaLDD << " " << BetaLDD << endl;

    // Fin ajout
      
    //Vec_Fecs & Vec_Fecp - Individual fecundities seeds and pollen simultaneously
    for(int p=0 ; p<NPar ; p++)
    {
      previousValue = Vec_Fecs[p];
      previousValue2 = Vec_Fecp[p];
      pLois->tirage(Vec_Fecs[p]);
      pLoi->tirage(Vec_Fecp[p]);
      nextLogLik = logLik();
      if(accept()<exp(nextLogLik-previousLogLik)) previousLogLik = nextLogLik;
      else
      {
        Vec_Fecs[p] = previousValue;
        Vec_Fecp[p] = previousValue2;
      }
    }

    if(thinStep && (ite%thinStep == 0))
    {
      *IndivFec << ite << " " ;
      for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecs[p] << " ";}
      for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecp[p] << " ";}
      //*IndivFec << endl;
      //IndivFec->flush();
    }

    //Vec_FreqLDD - Individual frequencies of LDD
    for(int p=0 ; p<NPar ; p++)
      {
          previousValue = Vec_FreqLDD[p];
          pLoiLDD->tirage(Vec_FreqLDD[p]);
          nextLogLik = logLik();
          if(accept()<exp(nextLogLik-previousLogLik)) previousLogLik = nextLogLik;
          else Vec_FreqLDD[p] = previousValue;
      }
      
      if(thinStep && (ite%thinStep == 0))
      {
          //*IndivFec << ite << " " ;
          for (int p=0; p<NPar; p++) {*IndivFec << Vec_FreqLDD[p] << " ";}
          *IndivFec << endl;
          IndivFec->flush();
      }
      
    // change Scales
    previousValue = Scales;
    Scales = previousValue*exp(0.2*gauss());
    if( Scales>mScales && Scales<MScales)
    {
      as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
      abs=-pow(as,-Shapes);
        Knorm = Shapes/as/as/exp(gammln(2/Shapes));
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else
      {
        Scales = previousValue;
        as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
        abs=-pow(as,-Shapes);
          Knorm = Shapes/as/as/exp(gammln(2/Shapes));
      }
    }
    else Scales = previousValue;
      
    //change Shapes
    previousValue = Shapes;
    Shapes = previousValue*exp(0.2*gauss());
    if( Shapes>mShapes && Shapes<MShapes)
    {
      as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
      abs=-pow(as,-Shapes);
        Knorm = Shapes/as/as/exp(gammln(2/Shapes));
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else{
        Shapes = previousValue;
        as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
        abs=-pow(as,-Shapes);
          Knorm = Shapes/as/as/exp(gammln(2/Shapes));
      }
    }
    else Shapes = previousValue;

    // change ScalesLDD  // Note -> Changer la fonction exponentielle puissance ???
      previousValue = ScalesLDD;
      ScalesLDD = previousValue*exp(0.2*gauss());
      if( ScalesLDD>mScalesLDD && ScalesLDD<MScalesLDD)
      {
          asLDD=ScalesLDD*exp(gammln(2/ShapesLDD)-gammln(3/ShapesLDD));
          absLDD=-pow(asLDD,-ShapesLDD);
          KnormLDD = ShapesLDD/asLDD/asLDD/exp(gammln(2/ShapesLDD));
          nextLogLik = logLik();
          if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
          else
          {
              ScalesLDD = previousValue;
              asLDD=ScalesLDD*exp(gammln(2/ShapesLDD)-gammln(3/ShapesLDD));
              absLDD=-pow(asLDD,-ShapesLDD);
              KnormLDD = ShapesLDD/asLDD/asLDD/exp(gammln(2/ShapesLDD));
              
          }
      }
      else ScalesLDD = previousValue;

    //change ShapesLDD // Note -> Changer la fonction exponentielle puissance ???
  
    //  Début ajout
    previousValue = ShapesLDD;
    ShapesLDD = previousValue*exp(0.2*gauss());
    if( ShapesLDD>mShapesLDD && ShapesLDD<MShapesLDD)
    {
      asLDD=ScalesLDD*exp(gammln(2/ShapesLDD)-gammln(3/ShapesLDD));
      absLDD=-pow(asLDD,-ShapesLDD);
        KnormLDD = ShapesLDD/asLDD/asLDD/exp(gammln(2/ShapesLDD));
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else{
        ShapesLDD = previousValue;
        asLDD=ScalesLDD*exp(gammln(2/ShapesLDD)-gammln(3/ShapesLDD));
        absLDD=-pow(asLDD,-ShapesLDD);
          KnormLDD = ShapesLDD/asLDD/asLDD/exp(gammln(2/ShapesLDD));
      }
    }
    else ShapesLDD = previousValue;
    // Fin ajout
      
      
    // change Migs
    previousValue = Migs;
    Migs = previousValue*exp(0.2*gauss());
    if( Migs>mMigs && Migs<MMigs)
    {
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else Migs = previousValue;
    }
    else Migs = previousValue;

    // change Scale
    previousValue = Scale;
    Scale = previousValue*exp(0.2*gauss());
    if( Scale>mScale && Scale<MScale)
    {
      ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
      abp=-pow(ap,-Shape);
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else
      {
        Scale = previousValue;
        ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        abp=-pow(ap,-Shape);
      }
    }
    else Scale = previousValue;

    // change Shape
    previousValue = Shape;
    Shape = previousValue*exp(0.2*gauss());
    if( Shape>mShape && Shape<MShape)
    {
      ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
      abp=-pow(ap,-Shape);
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else{
        Shape = previousValue;
        ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        abp=-pow(ap,-Shape);
      }
    }   
    else Shape = previousValue;

    // change Mig
    previousValue = Mig;
    Mig = previousValue*exp(0.2*gauss());
    if( Mig>mMig && Mig<MMig)
    {
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else Mig = previousValue;
    }
    else Mig = previousValue;

    // change Self
    previousValue = Self;
    Self = previousValue*exp(0.2*gauss());
    if( Self>mSelf && Self<MSelf)
    {
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else Self = previousValue;
    }
    else Self = previousValue;

    if(thinStep && (ite%thinStep == 0))
    {
      *ParamDisp << ite<<" " << previousLogLik
      << " " << Scales
      << " " << Shapes
      << " " << ScalesLDD
      << " " << ShapesLDD
      << " " << Migs
      << " " << Scale
      << " " << Shape
      << " " << Mig
      << " " << Self
      <<endl;
    ParamDisp->flush();
    }

  }
}

void MEMM_seedlings_2kernels::mcmc_dyn(int nbIteration, int thinStep)
{
  cerr<<"seedlings 2kernels dynamic mcmc not implemented yet"<<endl;
  exit(0);
}


/***************************** PROTECTED ********************************/

long double MEMM_seedlings_2kernels::logLik()
{

  long double liktemp = 0;
  double pip = 0;
  double pip2 = 0;
  int pbm = 0;

  vector < vector<long double> > matp (NPar, vector <long double> (NPar, 0.) );
  vector < vector<long double> > mats (Ns, vector <long double> (NPar, 0.) );
  vector <long double> totp (NPar);
  vector <long double> tots (Ns);

  for (int m=0; m<NPar; m++)
  {
    totp[m]=0;
    for (int p=0; p<m; p++)
    {
      matp[m][p]=exp(abp*pow(DistMP[m][p],Shape))*Vec_Fecp[p];
      if (isnan(matp[m][p])){matp[m][p]=0; pbm=1;}
      totp[m]+=matp[m][p];
    }
    for (int p=m+1; p<NPar; p++)
    {
      matp[m][p]=exp(abp*pow(DistMP[m][p],Shape))*Vec_Fecp[p];
      if (isnan(matp[m][p])){matp[m][p]=0; pbm=1;}
      totp[m]+=matp[m][p];
    }
    matp[m][m]=Self*totp[m]/(1-Mig-Self);
  }

  for (int s=0; s<Ns; s++)
  {
    tots[s]=0;
    for (int m=0; m<NPar; m++)
    {
      //mats[s][m]=((1-Vec_FreqLDD[m])*exp(abs*pow(DistSM[s][m],Shapes))/Knorm+(Vec_FreqLDD[m])*exp(absLDD*pow(DistSM[s][m],ShapesLDD))/KnormLDD)*Vec_Fecs[m];
        
        mats[s][m]=((1-Vec_FreqLDD[m])*exp(abs*pow(DistSM[s][m],Shapes))/Knorm+(Vec_FreqLDD[m])*exp(-pow((log(DistSM[s][m])-log(ScalesLDD)),2)/2/ShapesLDD)/pow(2*3.14*ShapesLDD,0.5))*Vec_Fecs[m];
        // To Use a mixture of exponential power with bivariate log-normal
        
      if (isnan(mats[s][m])){mats[s][m]=0; pbm=1;}
      tots[s]+=mats[s][m];
    }
  }

  if (pbm==1) { cerr << " Warning: Overflow. One component of the dispersal matrix was not defined. " << endl;}

  for (int s=0; s<Ns; s++)
  {
    if( ProbMig[s]>0 && tots[s]>0)
    {
      pip=0;pip2=0;
      for (int m=0; m<NbMeres[s]; m++)
      {
        pip += (ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot];
      }
      for (int m=0; m<NbCouples[s]; m++)
      {
        pip2 += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[ProbCouples[s][m].mere_pot][ProbCouples[s][m].pere_pot]/totp[ProbCouples[s][m].mere_pot];
      }
      liktemp += log(ProbMig[s]*Migs+(1-Migs)/tots[s]*(pip*Mig+(1-Mig-Self)*pip2));
    }
  }

return liktemp;

}


/*************** INIT ******************/

void MEMM_seedlings_2kernels::loadParameters(Configuration *p)
{

  MEMM_seedlings::loadParameters(p);

  Parameter * param_temp;

  cout<< "Load more and more Parameters..."<<endl;

  // alphaLDD
  param_temp = allParameters["alphaLDD"];
  if(param_temp)
  {
    AlphaLDD = param_temp->INIT;
    mAlphaLDD = param_temp->MIN;
    MAlphaLDD = param_temp->MAX;
    cout << "AlphaLDD : "<< AlphaLDD << " [" <<mAlphaLDD <<"|"<< MAlphaLDD << "]"<<endl;
  }

  // betaLDD
  param_temp = allParameters["betaLDD"];
  if(param_temp)
  {
    BetaLDD = param_temp->INIT;
    mBetaLDD = param_temp->MIN;
    MBetaLDD = param_temp->MAX;
    cout << "BetaLDD : "<< BetaLDD << " [" <<mBetaLDD <<"|"<< MBetaLDD << "]"<<endl;
  }
    
   
  // ScalesLDD 
  param_temp = allParameters["scalesLDD"];
  if(param_temp)
  {
    ScalesLDD = param_temp->INIT;
    mScalesLDD = param_temp->MIN;
    MScalesLDD = param_temp->MAX;
    cout << "ScalesLDD : "<< ScalesLDD << " [" <<mScalesLDD <<"|"<< MScalesLDD << "]"<<endl;
  }
  
  // shapesLDD
  param_temp = allParameters["shapesLDD"];
  if(param_temp)
  {
    ShapesLDD = param_temp->INIT;
    mShapesLDD = param_temp->MIN;
    MShapesLDD = param_temp->MAX;
    cout << "ShapesLDD : "<< ShapesLDD << " [" <<mShapesLDD <<"|"<< MShapesLDD << "]"<<endl;
  }

  asLDD=ScalesLDD*exp(gammln(2/ShapesLDD)-gammln(3/ShapesLDD));
  absLDD=-pow(asLDD,-ShapesLDD);
  KnormLDD = ShapesLDD/asLDD/asLDD/exp(gammln(2/ShapesLDD));
  Knorm = Shapes/as/as/exp(gammln(2/Shapes));

  Vec_FreqLDD.resize(NPar, 0.1 );

  pLoiLDD = loadDistribution(p,"individual_fecundities_LDD");
    
  cout<<"end loading Parameters"<<endl;

}


