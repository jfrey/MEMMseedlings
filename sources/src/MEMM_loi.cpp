/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "MEMM_loi.h"
#include <iostream>
#include <string>

// Add here law name
string MEMM_loi::LAWS_NAMES[] = {"LN","gamma","beta","zigamma"};

// Add here Laws parameters
map<int,vector<string>> MEMM_loi::LAWS_PARAMETERS = {
    {MEMM_IDLOI_LN , {"gama"}},
    {MEMM_IDLOI_GAMMA , {"gama"}},
    {MEMM_IDLOI_BETA , {"alpha","beta"}},
  {MEMM_IDLOI_ZIGAMMA , {"pi0","gama"}}
};

#define LAWS_NAME_AND_PARAMETERS { \
    { MEMM_loi::LAWS_NAMES[MEMM_IDLOI_LN] , MEMM_loi::LAWS_PARAMETERS[MEMM_IDLOI_LN]}, \
    { MEMM_loi::LAWS_NAMES[MEMM_IDLOI_GAMMA] , MEMM_loi::LAWS_PARAMETERS[MEMM_IDLOI_GAMMA]}, \
    { MEMM_loi::LAWS_NAMES[MEMM_IDLOI_BETA] , MEMM_loi::LAWS_PARAMETERS[MEMM_IDLOI_BETA]}, \
    { MEMM_loi::LAWS_NAMES[MEMM_IDLOI_ZIGAMMA] , MEMM_loi::LAWS_PARAMETERS[MEMM_IDLOI_ZIGAMMA]} \
}

const map<string,vector<string>> MEMM_loi::laws_name_and_parameters = LAWS_NAME_AND_PARAMETERS;

MEMM_loi::MEMM_loi()
{
}




MEMM_loi::~MEMM_loi()
{
    //dtor
}
