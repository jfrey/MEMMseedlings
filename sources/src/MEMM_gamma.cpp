/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include <iostream>
#include <string>
#include "MEMM_util.h"
#include "MEMM_gamma.h"

MEMM_gamma::MEMM_gamma(double& gamA,
		        boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& accept):
  _accept(accept),_gamma(1./gamA,gamA)
{
  _IDLoi=MEMM_IDLOI_GAMMA;
    std::cout<<"Using gamma law with gama value : "<<gamA<<std::endl;
}



MEMM_gamma::~MEMM_gamma()
{
    //dtor
}

void MEMM_gamma::tirage(double& v){
  v= quantile(_gamma,_accept());
}
double MEMM_gamma::logLik(int npar, const std::vector<double> & fec, double& A) {

  // Parametrage de la loi gamma en A, B.
  // A=1/alpha; B=alpha x beta = 1. fixe
  // k=shape=1/A; theta=scale=A
  // Esperance = B; dobs/de = Var/E/E = A

  double liktemp=0, a1, a2, a3, a4;
  a1=1/A; a2=(1-A)/A; a3=log(A)/A; a4=gammln(1/A);
  for (int p=0; p<npar; p++) {
    liktemp += -fec[p]*a1 + log(fec[p])*a2 - a3 - a4;
  }
  return liktemp;
}
void MEMM_gamma::setDParam(unsigned int indiceParam, double & value){
  switch (indiceParam)
    {
    case MEMM_LOI_GAMA:
      _gamma = boost::math::gamma_distribution<>(1./value,value);
      break;
    default:
      printf("Error: MEMM_gamma indiceParam out of range \n");
    }
}
