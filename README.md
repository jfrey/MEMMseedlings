MEMMSeedlings v2.0
===================

## MEMM (Mixed Effects Mating Model) implemtents a Bayesian statistic method to estimate the pollen dispersal kernel and the variance in male fecundity from spatial and genetic data (microsatellites) concerning adult plants and sampled seeds (see Klein et al. 2008 Molecular Ecology).

## MEMMSeedlings implements a Bayesian method to estimate individual female and male fecundities, based on genotypes and positions of established seedlings and of their candidate parent plants, and accounting for genotyping error.

[Binaries](binaries/)  
[Source codes](sources/)
